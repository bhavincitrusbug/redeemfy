import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import PushNotificationIOS from '@react-native-community/push-notification-ios';

import React, {PureComponent} from 'react';
import {Platform, View, SafeAreaView, StatusBar, YellowBox} from 'react-native';
import styles from './app/resources/styles';
import constants from './app/resources/constants';
import colors from './app/resources/colors';
import {doSetNotificationData} from './app/redux/actions/AppActions';
import SwitchStack from './app/screens/switchIndex';
import * as Sentry from '@sentry/react-native';
import FlashMessage from 'react-native-flash-message';

YellowBox.ignoreWarnings(['Remote debugger']);
Sentry.init({
  dsn: 'https://d49bf57771254294889b817dddf81724@sentry.io/2817024',
});
class App extends PureComponent {
  componentDidMount() {
    this.checkPermission();
    this.createNotificationListeners();

    if (Platform.OS === 'ios') {
      PushNotificationIOS.addEventListener('register', token => {});
      PushNotificationIOS.requestPermissions();
    }
    this.onTokenRefreshListener = firebase
      .messaging()
      .onTokenRefresh(fcmToken => {
        AsyncStorage.setItem(constants.DEVICE_TOKEN, fcmToken);
      });
  }
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();

    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();

      this.getToken();
    } catch (error) {
      console.log(error.message);
    }
  }
  async getToken() {
    let fcmToken = await AsyncStorage.getItem(constants.DEVICE_TOKEN);
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        await AsyncStorage.setItem(constants.DEVICE_TOKEN, fcmToken);
      }
    }
  }
  componentWillUnmount() {
    this.unsubscribeFromNotificationListener();
    this.notificationOpenedListener();
  }
  async createNotificationListeners() {
    const channel = new firebase.notifications.Android.Channel(
      'com.redeemfy',
      'notification',
      firebase.notifications.Android.Importance.Max,
    ).setDescription('A natural description of the channel');
    firebase.notifications().android.createChannel(channel);

    const channelGroup = new firebase.notifications.Android.ChannelGroup(
      'com.redeemfy',
      'notification',
    );
    firebase.notifications().android.createChannelGroup(channelGroup);

    this.unsubscribeFromNotificationListener = firebase
      .notifications()
      .onNotification(notification => {
        const {count} = notification.data;
        if (Platform.OS === 'android') {
          const localNotification = new firebase.notifications.Notification({
            sound: 'default',
            show_in_foreground: true,
          })
            .setNotificationId(notification.notificationId)
            .setTitle(notification.title)
            .setSubtitle(notification.subtitle)
            .setBody(notification.body)
            .setData(notification.data)
            .setSound('default')
            .android.setChannelId('com.redeemfy')
            .android.setSmallIcon('ic_notification')
            .android.setColor(colors.colorNotification)
            .android.setAutoCancel(true)
            .android.setGroupSummary(true)
            .android.setGroup('com.redeemfy')
            .android.setBadgeIconType(
              firebase.notifications.Android.BadgeIconType.Small,
            )
            .android.setPriority(firebase.notifications.Android.Priority.High);

          let notificationShow = firebase.notifications();
          notificationShow.setBadge(parseInt(count));
          notificationShow
            .displayNotification(localNotification)
            .catch(err => console.error(err));
        } else if (Platform.OS === 'ios') {
          const localNotification = new firebase.notifications.Notification()
            .setNotificationId(notification.notificationId)
            .setTitle(notification.title)
            .setSubtitle(notification.subtitle)
            .setBody(notification.body)
            .setData(notification.data)
            .setSound('default')
            .ios.setBadge(count);
          firebase
            .notifications()
            .displayNotification(localNotification)
            .catch(err => console.error(err));
        }
      });

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        const {title, body} = notificationOpen.notification;

        if (notificationOpen) {
          const action = notificationOpen.action;
          const notification = notificationOpen.notification;

          // const user = this.getUser();
          this.props.doSetNotificationData(notification.data);

          firebase
            .notifications()
            .removeDeliveredNotification(notification.notificationId);
        }
      });

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */

    firebase
      .notifications()
      .getInitialNotification()
      .then(notificationOpen => {
        if (notificationOpen) {
          const action = notificationOpen.action;
          const notification = notificationOpen.notification;
          this.props.doSetNotificationData(notification.data);
        }
      });
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        {Platform.OS === 'android' ? (
          undefined
        ) : (
          <StatusBar translucent={true} barStyle="light-content" />
        )}
        <View style={styles.container}>
          <SwitchStack />
        </View>
        <FlashMessage position="top" />
      </SafeAreaView>
    );
  }
}
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({doSetNotificationData}, dispatch),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
