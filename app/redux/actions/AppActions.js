import * as Request from '../../networks/ApiRequest';
import * as ActionType from './ActionTypes';
import ApiUrls from '../../networks/ApiUrls';

export const doSetServerTime = text => {
  return dispatch => {
    dispatch({type: ActionType.SET_SERVER_TIME, payload: text});
  };
};
export const doSetCurrentTimeTimer = text => {
  return dispatch => {
    dispatch({type: ActionType.UPDATE_CURRENT_TIME_TIMER, payload: text});
  };
};
export const doSetLastTimeTimer = text => {
  return dispatch => {
    dispatch({type: ActionType.UPDATE_LAST_TIME_TIMER, payload: text});
  };
};
export const doSetUser = text => {
  return dispatch => {
    dispatch({type: ActionType.SET_CURRENT_USER, payload: text});
  };
};
export const doUpdateCouponList = text => {
  return dispatch => {
    dispatch({type: ActionType.UPDATE_COUPON_LIST, payload: text});
  };
};
export const doSetSearchOptions = text => {
  return dispatch => {
    dispatch({type: ActionType.SET_SEARCH_OPTIONS, payload: text});
  };
};
export const doUpdateMarketList = text => {
  return dispatch => {
    dispatch({type: ActionType.UPDATE_MARKET_LIST, payload: text});
  };
};
export const doUpdateBagList = text => {
  return dispatch => {
    dispatch({type: ActionType.UPDATE_BAG_LIST, payload: text});
  };
};
export const doSetNotificationData = text => {
  return dispatch => {
    dispatch({type: ActionType.SET_NOTIFICATION_DATA, payload: text});
  };
};

export const doGetOnGoingRewards = body => {
  return async dispatch => {
    try {
      let urlParameters = Object.entries(body)
        .map(e => e.join('='))
        .join('&');
      dispatch({type: ActionType.GET_ONGOING_REWARDS_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_REWARDS + '?' + urlParameters,
        body,
      );
      dispatch({type: ActionType.GET_ONGOING_REWARDS_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_ONGOING_REWARDS_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doGetComingRewards = body => {
  return async dispatch => {
    try {
      let urlParameters = Object.entries(body)
        .map(e => e.join('='))
        .join('&');
      dispatch({type: ActionType.GET_COMING_REWARDS_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_REWARDS + '?' + urlParameters,
        body,
      );
      dispatch({type: ActionType.GET_COMING_REWARDS_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_COMING_REWARDS_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doGetMarketList = body => {
  return async dispatch => {
    try {
      let urlParameters = Object.entries(body)
        .map(e => e.join('='))
        .join('&');
      dispatch({type: ActionType.GET_MARKET_LIST_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_MARKET_LIST + '?' + urlParameters,
        body,
      );
      dispatch({type: ActionType.GET_MARKET_LIST_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_MARKET_LIST_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doGetBagInventoryList = body => {
  return async dispatch => {
    try {
      let urlParameters = Object.entries(body)
        .map(e => e.join('='))
        .join('&');
      dispatch({type: ActionType.GET_BAG_INVENTORY_LIST_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_BAG_LIST + '?' + urlParameters,
        body,
      );
      dispatch({
        type: ActionType.GET_BAG_INVENTORY_LIST_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_BAG_INVENTORY_LIST_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doGetBagRedeemedList = body => {
  return async dispatch => {
    try {
      let urlParameters = Object.entries(body)
        .map(e => e.join('='))
        .join('&');
      dispatch({type: ActionType.GET_BAG_REDEEMED_LIST_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_BAG_LIST + '?' + urlParameters,
        body,
      );
      dispatch({
        type: ActionType.GET_BAG_REDEEMED_LIST_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_BAG_REDEEMED_LIST_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doGetBagDetail = body => {
  return async dispatch => {
    try {
      let urlParameters = Object.entries(body)
        .map(e => e.join('='))
        .join('&');
      dispatch({type: ActionType.GET_BAG_DETAIL_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_BAG_DETAIL + '/' + body.bagId + '?' + urlParameters,
        body,
      );
      dispatch({type: ActionType.GET_BAG_DETAIL_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_BAG_DETAIL_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doGetNotificationList = body => {
  return async dispatch => {
    try {
      let urlParameters = Object.entries(body)
        .map(e => e.join('='))
        .join('&');
      dispatch({type: ActionType.GET_NOTIFICATION_LIST_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_NOTIFICATION_LIST + '?' + urlParameters,
        body,
      );
      dispatch({
        type: ActionType.GET_NOTIFICATION_LIST_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_NOTIFICATION_LIST_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doGetRewardDetail = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.GET_REWARD_DETAIL_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_REWARD_DETAIL + '/' + body.rewardId,
        body,
      );
      dispatch({type: ActionType.GET_REWARD_DETAIL_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_REWARD_DETAIL_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doGetMarketDetail = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.GET_MARKET_DETAIL_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_MARKET_DETAIL + '/' + body.marketId,
        body,
      );
      dispatch({type: ActionType.GET_MARKET_DETAIL_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_MARKET_DETAIL_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doPostRewardDetail = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.GET_REWARD_CLAIM_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.GET_REWARD_DETAIL + '/' + body.rewardId,
        body,
      );
      dispatch({type: ActionType.GET_REWARD_CLAIM_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_REWARD_CLAIM_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doGetBankDetail = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.GET_BANK_DETAIL_REQUEST});
      const result = await Request.getRequest(ApiUrls.GET_BANK_DETAIL, body);
      dispatch({type: ActionType.GET_BANK_DETAIL_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_BANK_DETAIL_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doPostBankDetail = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_BANK_DETAIL_REQUEST});
      const result = await Request.postRequest(ApiUrls.POST_BANK_DETAIL, body);
      dispatch({type: ActionType.POST_BANK_DETAIL_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_BANK_DETAIL_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doPostUpdateBankDetail = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_UPDATE_BANK_DETAIL_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.POST_BANK_DETAIL_UPDATE + '/' + body.bankId,
        body,
      );
      dispatch({
        type: ActionType.POST_UPDATE_BANK_DETAIL_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_UPDATE_BANK_DETAIL_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doPostSellReward = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_REWARD_SELL_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.POST_SELL_COUPAN + '/' + body.rewardId,
        body,
      );
      dispatch({type: ActionType.POST_REWARD_SELL_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_REWARD_SELL_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doPostRedeemReward = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_REWARD_REDEEM_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.POST_REDEEM_COUPAN + '/' + body.rewardId,
        body,
      );
      dispatch({type: ActionType.POST_REWARD_REDEEM_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_REWARD_REDEEM_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doPostEditProfile = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_EDIT_PROFILE_REQUEST});
      const result = await Request.postRequest(ApiUrls.POST_EDIT_PROFILE, body);
      dispatch({type: ActionType.POST_EDIT_PROFILE_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_EDIT_PROFILE_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doGetNotificationSwitch = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.GET_NOTIFICATION_SWITCH_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_POST_NOTIFICATION_SWITCH,
        body,
      );
      dispatch({
        type: ActionType.GET_NOTIFICATION_SWITCH_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_NOTIFICATION_SWITCH_FAILUER,
        payload: error,
      });
      throw error;
    }
  };
};
export const doPostNotificationSwitch = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_NOTIFICATION_SWITCH_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.GET_POST_NOTIFICATION_SWITCH,
        body,
      );
      dispatch({
        type: ActionType.POST_NOTIFICATION_SWITCH_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_NOTIFICATION_SWITCH_FAILUER,
        payload: error,
      });
      throw error;
    }
  };
};
export const doPostNotificationRead = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_NOTIFICATION_READ_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.POST_NOTIFICATION_READ,
        body,
      );
      dispatch({
        type: ActionType.POST_NOTIFICATION_READ_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_NOTIFICATION_READ_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doPostCashoutRequest = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_CASHOUT_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.POST_CASHOUT_REQUEST,
        body,
      );
      dispatch({
        type: ActionType.POST_CASHOUT_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_CASHOUT_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doPostEditOtpSend = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_EDIT_SEND_OTP_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.POST_EDIT_OTP_SEND,
        body,
      );
      dispatch({
        type: ActionType.POST_EDIT_SEND_OTP_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_EDIT_SEND_OTP_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doPostEditOtpVerify = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_EDIT_OTP_VERIFY_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.POST_EDIT_OTP_VERIFY,
        body,
      );
      dispatch({
        type: ActionType.POST_EDIT_OTP_VERIFY_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_EDIT_OTP_VERIFY_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doPostEditOtpResend = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_EDIT_RESEND_OTP_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.POST_EDIT_OTP_RESEND,
        body,
      );
      dispatch({
        type: ActionType.POST_EDIT_RESEND_OTP_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_EDIT_RESEND_OTP_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doGetTransacations = body => {
  return async dispatch => {
    try {
      let urlParameters = Object.entries(body)
        .map(e => e.join('='))
        .join('&');
      dispatch({type: ActionType.GET_TRANSACTIONS_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_TRANSACATIONS + '?' + urlParameters,
        body,
      );
      dispatch({
        type: ActionType.GET_TRANSACTIONS_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_TRANSACTIONS_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doGetTransacationsViewMore = body => {
  return async dispatch => {
    try {
      let urlParameters = Object.entries(body)
        .map(e => e.join('='))
        .join('&');
      dispatch({type: ActionType.GET_TRANSACTIONS_VIEWMORE_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_TRANSACATIONS + '?' + urlParameters,
        body,
      );
      dispatch({
        type: ActionType.GET_TRANSACTIONS_VIEWMORE_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_TRANSACTIONS_VIEWMORE_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doGetTransacationDetail = body => {
  return async dispatch => {
    try {
      let urlParameters = Object.entries(body)
        .map(e => e.join('='))
        .join('&');
      dispatch({type: ActionType.GET_TRANSACTION_DETAIL_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_TRANSACATION_DETAIL +
          '/' +
          body.transactionId +
          '?' +
          urlParameters,
        body,
      );
      dispatch({
        type: ActionType.GET_TRANSACTION_DETAIL_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_TRANSACTION_DETAIL_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doGetCashoutRequests = body => {
  return async dispatch => {
    try {
      let urlParameters = Object.entries(body)
        .map(e => e.join('='))
        .join('&');
      dispatch({type: ActionType.GET_CASHOUT_REQUESTS_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_CASHOUT_REQUESTS + '?' + urlParameters,
        body,
      );
      dispatch({
        type: ActionType.GET_CASHOUT_REQUESTS_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_CASHOUT_REQUESTS_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doPostBuyCoupon = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_BUY_COUPON_REQUEST});
      const result = await Request.postRequest(ApiUrls.POST_BUY_COUPON, body);
      dispatch({
        type: ActionType.POST_BUY_COUPON_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_BUY_COUPON_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doGetSearchHistory = (body, user) => {
  return async dispatch => {
    try {
      let urlParameters = Object.entries(body)
        .map(e => e.join('='))
        .join('&');
      dispatch({type: ActionType.GET_SEARCH_HISTORY_REQUEST});
      const result = await Request.getRequest(
        ApiUrls.GET_SEARCH_HISTORY + '?' + urlParameters,
        user,
      );
      dispatch({
        type: ActionType.GET_SEARCH_HISTORY_SUCCESS,
        payload: result,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_SEARCH_HISTORY_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doGetServerTime = (body, user) => {
  return async dispatch => {
    try {
      let urlParameters = Object.entries(body)
        .map(e => e.join('='))
        .join('&');
      dispatch({type: ActionType.GET_SERVER_TIME_REQUEST});
      const result = await Request.getTimeRequest(
        ApiUrls.GET_REWARDS + '?' + urlParameters,
        body,
      );
      const {data, links, server, expired} = result.result;
      dispatch({
        type: ActionType.GET_SERVER_TIME_SUCCESS,
        payload: server,
      });
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.GET_SERVER_TIME_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

// export const doPostUpdateProfile = (field, imageUrl, body) => {
//   return async dispatch => {
//     try {
//       dispatch({type: ActionType.UPDATE_PROFILE_REQUEST});
//       const result = await postImageRequest(
//         ApiUrls.UPDATE_PROFILE,
//         field,
//         imageUrl,
//         body,
//       );
//       dispatch({type: ActionType.UPDATE_PROFILE_SUCCESS, payload: result});
//       return result;
//     } catch (error) {
//       dispatch({
//         type: ActionType.UPDATE_PROFILE_FAILURE,
//         payload: error,
//       });
//       throw error;
//     }
//   };
// };
