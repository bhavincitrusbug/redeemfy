import * as Request from '../../networks/ApiRequest';
import * as ActionType from './ActionTypes';

import ApiUrls from '../../networks/ApiUrls';

export const doLogin = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_LOGIN_REQUEST});
      const result = await Request.postRequest(ApiUrls.POST_LOGIN, body);
      dispatch({type: ActionType.POST_LOGIN_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_LOGIN_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doRegister = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_REGISTER_REQUEST});
      const result = await Request.postRequest(ApiUrls.POST_REGISTER, body);
      return dispatch({
        type: ActionType.POST_REGISTER_SUCCESS,
        payload: result,
      });
    } catch (error) {
      dispatch({
        type: ActionType.POST_REGISTER_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doForgotPassword = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_FORGOT_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.POST_FORGOT_PASSWORD,
        body,
      );
      return dispatch({type: ActionType.POST_FORGOT_SUCCESS, payload: result});
    } catch (error) {
      dispatch({
        type: ActionType.POST_FORGOT_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doLogout = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_LOGOUT_REQUEST});
      const result = await Request.postRequest(ApiUrls.POST_LOGOUT, body);
      return dispatch({type: ActionType.POST_LOGOUT_SUCCESS, payload: result});
    } catch (error) {
      dispatch({
        type: ActionType.POST_LOGOUT_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};

export const doPostVerifyNumber = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_VERIFY_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.POST_VERIFY + '/' + body.userId,
        body,
      );
      dispatch({type: ActionType.POST_VERIFY_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_VERIFY_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doPostResendCode = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_RESEND_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.POST_RESEND + '/' + body.userId,
        body,
      );
      dispatch({type: ActionType.POST_RESEND_SUCCESS, payload: result});
      return result;
    } catch (error) {
      dispatch({
        type: ActionType.POST_RESEND_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doPostResetPassword = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_RESET_PASSWORD_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.POST_RESET_PASSWORD + '/' + body.userId,
        body,
      );
      return dispatch({
        type: ActionType.POST_RESET_PASSWORD_SUCCESS,
        payload: result,
      });
    } catch (error) {
      dispatch({
        type: ActionType.POST_RESET_PASSWORD_FAILURE,
        payload: error,
      });
      throw error;
    }
  };
};
export const doPostChangePassword = body => {
  return async dispatch => {
    try {
      dispatch({type: ActionType.POST_CHANGE_PASSWORD_REQUEST});
      const result = await Request.postRequest(
        ApiUrls.POST_CHANGE_PASSWORD,
        body,
      );
      return dispatch({
        type: ActionType.POST_CHANGE_PASSWORD_SUCCESS,
        payload: result,
      });
    } catch (error) {
      dispatch({
        type: ActionType.POST_CHANGE_PASSWORD_F,
        payload: error,
      });
      throw error;
    }
  };
};

// export const doLogout = body => {
//   return async dispatch => {
//     try {
//       dispatch({ type: ActionType.POST_LOGOUT_REQUEST });
//       const result = await Request.postRequest(ApiUrls.POST_LOGOUT, body);
//       return dispatch({ type: ActionType.POST_LOGOUT_SUCCESS, payload: result });
//     } catch (error) {
//       dispatch({
//         type: ActionType.POST_LOGOUT_FAILURE,
//         payload: error
//       });
//       throw error;
//     }
//   };
// };
