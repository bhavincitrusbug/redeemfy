import * as ActionType from '../actions/ActionTypes';
import strings from '../../resources/strings';

const initialState = {
  currentLang: strings.LAG_ENG,
  serverTime: 0,
  currentTimeTimer: undefined,
  lastTimeTimer: undefined,
  currentUser: undefined,
  notificationData: undefined,
  searchOptions: {
    enable: false,
    fromDate: '',
    toDate: '',
    keyword: '',
    transacation_type: '',
    sort_by: '',
    minValue: '',
    maxValue: '',
    key: '',
  },
  isBusyOnGoingRewards: false,
  responseOnGoingRewards: undefined,
  errorOnGoingRewards: undefined,
  isBusyComingRewards: false,
  responseComingRewards: undefined,
  errorComingRewards: undefined,
  isBusyMarketList: false,
  responseMarketList: undefined,
  errorMarketList: undefined,
  isBusyBagInventoryList: false,
  responseBagInventoryList: undefined,
  errorBagInventoryList: undefined,
  isBusyBagRedeemedList: false,
  responseBagRedeemedList: undefined,
  errorBagRedeemedList: undefined,
  isBusyNotificationList: false,
  responseNotificationList: undefined,
  errorNotificationList: undefined,
  isBusyRewardDetail: false,
  responseRewardDetail: undefined,
  errorRewardDetail: undefined,
  isBusyMarketDetail: false,
  responseMarketDetail: undefined,
  errorMarketDetail: undefined,
  isBusyRewardClaim: false,
  responseRewardClaim: undefined,
  errorRewardClaim: undefined,
  isBusyRewardBuy: false,
  responseRewardBuy: undefined,
  errorRewardBuy: undefined,
  isBusyBankDetail: false,
  responseBankDetail: undefined,
  errorBankDetail: undefined,
  isBusyAddBankDetail: false,
  responseAddBankDetail: undefined,
  errorAddBankDetail: undefined,
  isBusyUpdateBankDetail: false,
  responseUpdateBankDetail: undefined,
  errorUpdateBankDetail: undefined,
  isBusyBagDetail: false,
  responseBagDetail: undefined,
  errorBagDetail: undefined,
  isBusyRewardSell: false,
  responseRewardSell: undefined,
  errorRewardSell: undefined,
  isBusyRewardRedeem: false,
  responseRewardRedeem: undefined,
  errorRewardRedeem: undefined,
  bagListUpdate: {
    enable: false,
    key: '',
  },
  couponListUpdate: {
    enable: false,
    key: '',
  },
  marketListUpdate: {
    enable: false,
    key: '',
  },
  isBusyEditProfile: false,
  responseEditProfile: undefined,
  errorEditProfile: undefined,
  isBusyGetNotificationSwitch: false,
  responseGetNotificationSwitch: undefined,
  errorGetNotificationSwitch: undefined,
  isBusySetNotificationSwitch: false,
  responseSetNotificationSwitch: undefined,
  errorSetNotificationSwitch: undefined,
  isBusyNotificationRead: false,
  responseNotificationRead: undefined,
  errorNotificationRead: undefined,
  isBusyCashout: false,
  responseCashout: undefined,
  errorCashout: undefined,
  isBusyEditSendOtp: false,
  responseEditSendOtp: undefined,
  errorEditSendOtp: undefined,
  isBusyEditOtpVerify: false,
  responseEditOtpVerify: undefined,
  errorEditOtpVerify: undefined,
  isBusyEditReSendOtp: false,
  responseEditReSendOtp: undefined,
  errorEditReSendOtp: undefined,
  isBusyTransactions: false,
  responseTransactions: undefined,
  errorTransactions: undefined,
  isBusyTransactionsViewMore: false,
  responseTransactionsViewMore: undefined,
  errorTransactionsViewMore: undefined,
  isBusyTransactionDetail: false,
  responseTransactionDetail: undefined,
  errorTransactionDetail: undefined,
  isBusyCashoutRequest: false,
  responseCashoutRequest: undefined,
  errorCashoutRequest: undefined,
  isBusySearchHistory: false,
  responseSearchHistory: undefined,
  errorSearchHistory: undefined,
  isBusyServerTime: false,
  responseServerTime: undefined,
  errorServerTime: undefined,
};

export default function AppReducer(state = initialState, action) {
  switch (action.type) {
    case ActionType.SET_SERVER_TIME:
      return {
        ...state,
        serverTime: action.payload,
      };
    case ActionType.UPDATE_LAST_TIME_TIMER:
      return {
        ...state,
        lastTimeTimer: action.payload,
      };
    case ActionType.UPDATE_CURRENT_TIME_TIMER:
      return {
        ...state,
        currentTimeTimer: action.payload,
      };
    case ActionType.SET_SEARCH_OPTIONS:
      return {
        ...state,
        searchOptions: action.payload,
      };
    case ActionType.UPDATE_COUPON_LIST:
      return {
        ...state,
        couponListUpdate: action.payload,
      };
    case ActionType.UPDATE_MARKET_LIST:
      return {
        ...state,
        marketListUpdate: action.payload,
      };
    case ActionType.UPDATE_BAG_LIST:
      return {
        ...state,
        bagListUpdate: action.payload,
      };
    case ActionType.SET_CURRENT_USER:
      return {
        ...state,
        currentUser: action.payload,
      };
    case ActionType.SET_NOTIFICATION_DATA:
      return {
        ...state,
        notificationData: action.payload,
      };
    case ActionType.GET_ONGOING_REWARDS_REQUEST:
      return {
        ...state,
        isBusyOnGoingRewards: true,
      };
    case ActionType.GET_ONGOING_REWARDS_SUCCESS:
      return {
        ...state,
        isBusyOnGoingRewards: false,
        responseOnGoingRewards: action.payload,
      };
    case ActionType.GET_ONGOING_REWARDS_FAILURE:
      return {
        ...state,
        isBusyOnGoingRewards: false,
        errorOnGoingRewards: action.payload,
      };
    case ActionType.GET_COMING_REWARDS_REQUEST:
      return {
        ...state,
        isBusyComingRewards: true,
      };
    case ActionType.GET_COMING_REWARDS_SUCCESS:
      return {
        ...state,
        isBusyComingRewards: false,
        responseComingRewards: action.payload,
      };
    case ActionType.GET_COMING_REWARDS_FAILURE:
      return {
        ...state,
        isBusyComingRewards: false,
        errorComingRewards: action.payload,
      };
    case ActionType.GET_MARKET_LIST_REQUEST:
      return {
        ...state,
        isBusyMarketList: true,
      };
    case ActionType.GET_MARKET_LIST_SUCCESS:
      return {
        ...state,
        isBusyMarketList: false,
        responseMarketList: action.payload,
      };
    case ActionType.GET_MARKET_LIST_FAILURE:
      return {
        ...state,
        isBusyMarketList: false,
        errorMarketList: action.payload,
      };
    case ActionType.GET_BAG_INVENTORY_LIST_REQUEST:
      return {
        ...state,
        isBusyBagInventoryList: true,
      };
    case ActionType.GET_BAG_INVENTORY_LIST_SUCCESS:
      return {
        ...state,
        isBusyBagInventoryList: false,
        responseBagInventoryList: action.payload,
      };
    case ActionType.GET_BAG_INVENTORY_LIST_FAILURE:
      return {
        ...state,
        isBusyBagInventoryList: false,
        errorBagInventoryList: action.payload,
      };
    case ActionType.GET_BAG_REDEEMED_LIST_REQUEST:
      return {
        ...state,
        isBusyBagRedeemedList: true,
      };
    case ActionType.GET_BAG_REDEEMED_LIST_SUCCESS:
      return {
        ...state,
        isBusyBagRedeemedList: false,
        responseBagRedeemedList: action.payload,
      };
    case ActionType.GET_BAG_REDEEMED_LIST_FAILURE:
      return {
        ...state,
        isBusyBagRedeemedList: false,
        errorBagRedeemedList: action.payload,
      };
    case ActionType.GET_NOTIFICATION_LIST_REQUEST:
      return {
        ...state,
        isBusyNotificationList: true,
      };
    case ActionType.GET_NOTIFICATION_LIST_SUCCESS:
      return {
        ...state,
        isBusyNotificationList: false,
        responseNotificationList: action.payload,
      };
    case ActionType.GET_NOTIFICATION_LIST_FAILURE:
      return {
        ...state,
        isBusyNotificationList: false,
        errorNotificationList: action.payload,
      };
    case ActionType.GET_REWARD_DETAIL_REQUEST:
      return {
        ...state,
        isBusyRewardDetail: true,
      };
    case ActionType.GET_REWARD_DETAIL_SUCCESS:
      return {
        ...state,
        isBusyRewardDetail: false,
        responseRewardDetail: action.payload,
      };
    case ActionType.GET_REWARD_DETAIL_FAILURE:
      return {
        ...state,
        isBusyRewardDetail: false,
        errorRewardDetail: action.payload,
      };
    case ActionType.GET_MARKET_DETAIL_REQUEST:
      return {
        ...state,
        isBusyMarketDetail: true,
      };
    case ActionType.GET_MARKET_DETAIL_SUCCESS:
      return {
        ...state,
        isBusyMarketDetail: false,
        responseMarketDetail: action.payload,
      };
    case ActionType.GET_MARKET_DETAIL_FAILURE:
      return {
        ...state,
        isBusyMarketDetail: false,
        errorMarketDetail: action.payload,
      };
    case ActionType.GET_REWARD_CLAIM_REQUEST:
      return {
        ...state,
        isBusyRewardClaim: true,
      };
    case ActionType.GET_REWARD_CLAIM_SUCCESS:
      return {
        ...state,
        isBusyRewardClaim: false,
        responseRewardClaim: action.payload,
      };
    case ActionType.GET_REWARD_CLAIM_FAILURE:
      return {
        ...state,
        isBusyRewardClaim: false,
        errorRewardClaim: action.payload,
      };

    case ActionType.GET_BANK_DETAIL_REQUEST:
      return {
        ...state,
        isBusyBankDetail: true,
      };
    case ActionType.GET_BANK_DETAIL_SUCCESS:
      return {
        ...state,
        isBusyBankDetail: false,
        responseBankDetail: action.payload,
      };
    case ActionType.GET_BANK_DETAIL_FAILURE:
      return {
        ...state,
        isBusyBankDetail: false,
        errorBankDetail: action.payload,
      };
    case ActionType.POST_BANK_DETAIL_REQUEST:
      return {
        ...state,
        isBusyAddBankDetail: true,
      };
    case ActionType.POST_BANK_DETAIL_SUCCESS:
      return {
        ...state,
        isBusyAddBankDetail: false,
        responseAddBankDetail: action.payload,
      };
    case ActionType.POST_BANK_DETAIL_FAILURE:
      return {
        ...state,
        isBusyAddBankDetail: false,
        errorAddBankDetail: action.payload,
      };
    case ActionType.POST_UPDATE_BANK_DETAIL_REQUEST:
      return {
        ...state,
        isBusyUpdateBankDetail: true,
      };
    case ActionType.POST_UPDATE_BANK_DETAIL_SUCCESS:
      return {
        ...state,
        isBusyUpdateBankDetail: false,
        responseUpdateBankDetail: action.payload,
      };
    case ActionType.POST_UPDATE_BANK_DETAIL_FAILURE:
      return {
        ...state,
        isBusyUpdateBankDetail: false,
        errorUpdateBankDetail: action.payload,
      };
    case ActionType.GET_BAG_DETAIL_REQUEST:
      return {
        ...state,
        isBusyBagDetail: true,
      };
    case ActionType.GET_BAG_DETAIL_SUCCESS:
      return {
        ...state,
        isBusyBagDetail: false,
        responseBagDetail: action.payload,
      };
    case ActionType.GET_BAG_DETAIL_FAILURE:
      return {
        ...state,
        isBusyBagDetail: false,
        errorBagDetail: action.payload,
      };
    case ActionType.POST_REWARD_SELL_REQUEST:
      return {
        ...state,
        isBusyRewardSell: true,
      };
    case ActionType.POST_REWARD_SELL_SUCCESS:
      return {
        ...state,
        isBusyRewardSell: false,
        responseRewardSell: action.payload,
      };
    case ActionType.POST_REWARD_SELL_FAILURE:
      return {
        ...state,
        isBusyRewardSell: false,
        errorRewardSell: action.payload,
      };
    case ActionType.POST_REWARD_REDEEM_REQUEST:
      return {
        ...state,
        isBusyRewardRedeem: true,
      };
    case ActionType.POST_REWARD_REDEEM_SUCCESS:
      return {
        ...state,
        isBusyRewardRedeem: false,
        responseRewardRedeem: action.payload,
      };
    case ActionType.POST_REWARD_REDEEM_FAILURE:
      return {
        ...state,
        isBusyRewardRedeem: false,
        errorRewardRedeem: action.payload,
      };
    case ActionType.POST_EDIT_PROFILE_REQUEST:
      return {
        ...state,
        isBusyEditProfile: true,
      };
    case ActionType.POST_EDIT_PROFILE_SUCCESS:
      return {
        ...state,
        isBusyEditProfile: false,
        responseEditProfile: action.payload,
      };
    case ActionType.POST_EDIT_PROFILE_FAILURE:
      return {
        ...state,
        isBusyEditProfile: false,
        errorEditProfile: action.payload,
      };
    case ActionType.GET_NOTIFICATION_SWITCH_REQUEST:
      return {
        ...state,
        isBusyGetNotificationSwitch: true,
      };
    case ActionType.GET_NOTIFICATION_SWITCH_SUCCESS:
      return {
        ...state,
        isBusyGetNotificationSwitch: false,
        responseGetNotificationSwitch: action.payload,
      };
    case ActionType.GET_NOTIFICATION_SWITCH_FAILUER:
      return {
        ...state,
        isBusyGetNotificationSwitch: false,
        errorGetNotificationSwitch: action.payload,
      };
    case ActionType.POST_NOTIFICATION_SWITCH_REQUEST:
      return {
        ...state,
        isBusySetNotificationSwitch: true,
      };
    case ActionType.POST_NOTIFICATION_SWITCH_SUCCESS:
      return {
        ...state,
        isBusySetNotificationSwitch: false,
        responseSetNotificationSwitch: action.payload,
      };
    case ActionType.POST_NOTIFICATION_SWITCH_FAILUER:
      return {
        ...state,
        isBusySetNotificationSwitch: false,
        errorSetNotificationSwitch: action.payload,
      };
    case ActionType.POST_NOTIFICATION_READ_REQUEST:
      return {
        ...state,
        isBusyNotificationRead: true,
      };
    case ActionType.POST_NOTIFICATION_READ_SUCCESS:
      return {
        ...state,
        isBusyNotificationRead: false,
        responseNotificationRead: action.payload,
      };
    case ActionType.POST_NOTIFICATION_READ_FAILURE:
      return {
        ...state,
        isBusyNotificationRead: false,
        errorNotificationRead: action.payload,
      };
    case ActionType.POST_CASHOUT_REQUEST:
      return {
        ...state,
        isBusyCashout: true,
      };
    case ActionType.POST_CASHOUT_SUCCESS:
      return {
        ...state,
        isBusyCashout: false,
        responseCashout: action.payload,
      };
    case ActionType.POST_CASHOUT_FAILURE:
      return {
        ...state,
        isBusyCashout: false,
        errorCashout: action.payload,
      };
    case ActionType.POST_EDIT_SEND_OTP_REQUEST:
      return {
        ...state,
        isBusyEditSendOtp: true,
      };
    case ActionType.POST_EDIT_SEND_OTP_SUCCESS:
      return {
        ...state,
        isBusyEditSendOtp: false,
        responseEditSendOtp: action.payload,
      };
    case ActionType.POST_EDIT_SEND_OTP_FAILURE:
      return {
        ...state,
        isBusyEditSendOtp: false,
        errorEditSendOtp: action.payload,
      };
    case ActionType.POST_EDIT_OTP_VERIFY_REQUEST:
      return {
        ...state,
        isBusyEditOtpVerify: true,
      };
    case ActionType.POST_EDIT_OTP_VERIFY_SUCCESS:
      return {
        ...state,
        isBusyEditOtpVerify: false,
        responseEditOtpVerify: action.payload,
      };
    case ActionType.POST_EDIT_OTP_VERIFY_FAILURE:
      return {
        ...state,
        isBusyEditOtpVerify: false,
        errorEditOtpVerify: action.payload,
      };
    case ActionType.POST_EDIT_RESEND_OTP_REQUEST:
      return {
        ...state,
        isBusyEditReSendOtp: true,
      };
    case ActionType.POST_EDIT_RESEND_OTP_SUCCESS:
      return {
        ...state,
        isBusyEditReSendOtp: false,
        responseEditReSendOtp: action.payload,
      };
    case ActionType.POST_EDIT_RESEND_OTP_FAILURE:
      return {
        ...state,
        isBusyEditReSendOtp: false,
        errorEditReSendOtp: action.payload,
      };
    case ActionType.GET_TRANSACTIONS_REQUEST:
      return {
        ...state,
        isBusyTransactions: true,
      };
    case ActionType.GET_TRANSACTIONS_SUCCESS:
      return {
        ...state,
        isBusyTransactions: false,
        responseTransactions: action.payload,
      };
    case ActionType.GET_TRANSACTIONS_FAILURE:
      return {
        ...state,
        isBusyTransactions: false,
        errorTransactions: action.payload,
      };
    case ActionType.GET_TRANSACTIONS_VIEWMORE_REQUEST:
      return {
        ...state,
        isBusyTransactionsViewMore: true,
      };
    case ActionType.GET_TRANSACTIONS_VIEWMORE_SUCCESS:
      return {
        ...state,
        isBusyTransactionsViewMore: false,
        responseTransactionsViewMore: action.payload,
      };
    case ActionType.GET_TRANSACTIONS_VIEWMORE_FAILURE:
      return {
        ...state,
        isBusyTransactionsViewMore: false,
        errorTransactionsViewMore: action.payload,
      };
    case ActionType.GET_TRANSACTION_DETAIL_REQUEST:
      return {
        ...state,
        isBusyTransactionDetail: true,
      };
    case ActionType.GET_TRANSACTION_DETAIL_SUCCESS:
      return {
        ...state,
        isBusyTransactionDetail: false,
        responseTransactionDetail: action.payload,
      };
    case ActionType.GET_TRANSACTION_DETAIL_FAILURE:
      return {
        ...state,
        isBusyTransactionDetail: false,
        errorTransactionDetail: action.payload,
      };
    case ActionType.GET_CASHOUT_REQUESTS_REQUEST:
      return {
        ...state,
        isBusyCashoutRequest: true,
      };
    case ActionType.GET_CASHOUT_REQUESTS_SUCCESS:
      return {
        ...state,
        isBusyCashoutRequest: false,
        responseCashoutRequest: action.payload,
      };
    case ActionType.GET_CASHOUT_REQUESTS_FAILURE:
      return {
        ...state,
        isBusyCashoutRequest: false,
        errorCashoutRequest: action.payload,
      };
    case ActionType.POST_BUY_COUPON_REQUEST:
      return {
        ...state,
        isBusyRewardBuy: true,
      };
    case ActionType.POST_BUY_COUPON_SUCCESS:
      return {
        ...state,
        isBusyRewardBuy: false,
        responseRewardBuy: action.payload,
      };
    case ActionType.POST_BUY_COUPON_FAILURE:
      return {
        ...state,
        isBusyRewardBuy: false,
        errorRewardBuy: action.payload,
      };
    case ActionType.GET_SEARCH_HISTORY_REQUEST:
      return {
        ...state,
        isBusySearchHistory: true,
      };
    case ActionType.GET_SEARCH_HISTORY_SUCCESS:
      return {
        ...state,
        isBusySearchHistory: false,
        responseSearchHistory: action.payload,
      };
    case ActionType.GET_SEARCH_HISTORY_FAILURE:
      return {
        ...state,
        isBusySearchHistory: false,
        errorSearchHistory: action.payload,
      };
    case ActionType.GET_SERVER_TIME_REQUEST:
      return {
        ...state,
        isBusyServerTime: true,
      };
    case ActionType.GET_SERVER_TIME_SUCCESS:
      return {
        ...state,
        isBusyServerTime: false,
        responseServerTime: action.payload,
      };
    case ActionType.GET_SERVER_TIME_FAILURE:
      return {
        ...state,
        isBusyServerTime: false,
        errorServerTime: action.payload,
      };
    default:
      return state;
  }
}
