import {combineReducers} from 'redux';
import AuthReducer from './AuthReducer';
import AppReducer from './AppReducer';

const index = combineReducers({
  auth: AuthReducer,
  app: AppReducer,
});

export default index;
