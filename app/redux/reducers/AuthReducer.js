import * as ActionType from '../actions/ActionTypes';

const initialState = {
  isBusyLogin: false,
  responseLogin: undefined,
  errorLogin: undefined,
  isBusyRegister: false,
  responseRegister: undefined,
  errorRegister: undefined,
  isBusyForgot: false,
  responseForgot: undefined,
  errorForgot: undefined,
  isBusyLogout: false,
  responseLogout: undefined,
  errorLogout: undefined,
  isBusyVerify: false,
  responseVerify: undefined,
  errorVerify: undefined,
  isBusyResend: false,
  responseResend: undefined,
  errorResend: undefined,
  isBusyResetPassword: false,
  responseResetPassword: undefined,
  errorResetPassword: undefined,
  isBusyChangePassword: false,
  responseChangePassword: undefined,
  errorChangePassword: undefined,
};

export default function AuthReducer(state = initialState, action) {
  switch (action.type) {
    case ActionType.POST_LOGIN_REQUEST:
      return {
        ...state,
        isBusyLogin: true,
      };
    case ActionType.POST_LOGIN_SUCCESS:
      return {
        ...state,
        isBusyLogin: false,
        responseLogin: action.payload,
      };
    case ActionType.POST_LOGIN_FAILURE:
      return {
        ...state,
        isBusyLogin: false,
        errorLogin: action.payload,
      };
    case ActionType.POST_REGISTER_REQUEST:
      return {
        ...state,
        isBusyRegister: true,
      };
    case ActionType.POST_REGISTER_SUCCESS:
      return {
        ...state,
        isBusyRegister: false,
        responseRegister: action.payload,
      };
    case ActionType.POST_REGISTER_FAILURE:
      return {
        ...state,
        isBusyRegister: false,
        errorRegister: action.payload,
      };
    case ActionType.POST_FORGOT_REQUEST:
      return {
        ...state,
        isBusyForgot: true,
      };
    case ActionType.POST_FORGOT_SUCCESS:
      return {
        ...state,
        isBusyForgot: false,
        responseForgot: action.payload,
      };
    case ActionType.POST_FORGOT_FAILURE:
      return {
        ...state,
        isBusyForgot: false,
        errorForgot: action.payload,
      };
    case ActionType.POST_LOGOUT_REQUEST:
      return {
        ...state,
        isBusyLogout: true,
      };
    case ActionType.POST_LOGOUT_SUCCESS:
      return {
        ...state,
        isBusyLogout: false,
        responseLogout: action.payload,
      };
    case ActionType.POST_LOGOUT_FAILURE:
      return {
        ...state,
        isBusyLogout: false,
        errorLogout: action.payload,
      };
    case ActionType.POST_VERIFY_REQUEST:
      return {
        ...state,
        isBusyVerify: true,
      };
    case ActionType.POST_VERIFY_SUCCESS:
      return {
        ...state,
        isBusyVerify: false,
        responseVerify: action.payload,
      };
    case ActionType.POST_VERIFY_FAILURE:
      return {
        ...state,
        isBusyVerify: false,
        errorVerify: action.payload,
      };
    case ActionType.POST_RESEND_REQUEST:
      return {
        ...state,
        isBusyResend: true,
      };
    case ActionType.POST_RESEND_SUCCESS:
      return {
        ...state,
        isBusyResend: false,
        responseResend: action.payload,
      };
    case ActionType.POST_RESEND_FAILURE:
      return {
        ...state,
        isBusyResend: false,
        errorResend: action.payload,
      };
    case ActionType.POST_RESET_PASSWORD_REQUEST:
      return {
        ...state,
        isBusyResetPassword: true,
      };
    case ActionType.POST_RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        isBusyResetPassword: false,
        responseResetPassword: action.payload,
      };
    case ActionType.POST_RESET_PASSWORD_FAILURE:
      return {
        ...state,
        isBusyResetPassword: false,
        errorResetPassword: action.payload,
      };
    case ActionType.POST_CHANGE_PASSWORD_REQUEST:
      return {
        ...state,
        isBusyChangePassword: true,
      };
    case ActionType.POST_CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        isBusyChangePassword: false,
        responseChangePassword: action.payload,
      };
    case ActionType.POST_CHANGE_PASSWORD_FAILURE:
      return {
        ...state,
        isBusyChangePassword: false,
        errorChangePassword: action.payload,
      };

    default:
      return state;
  }
}
