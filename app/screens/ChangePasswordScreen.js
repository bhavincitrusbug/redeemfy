import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {View, Text, Image, TouchableOpacity, Platform} from 'react-native';
import colors from '../resources/colors';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../resources/fonts';
import Reinput from 'reinput';
import {getErrorValue, getLangValue} from '../resources/languages/language';
import {showErrorMessage} from '../resources/validation';
import errors from '../resources/errors';
import strings from '../resources/strings';
import ButtonGradient from '../components/buttons/ButtonGradient';
import ChangePasswordStyle from '../resources/styles/ChangePasswordStyle';
import {doPostChangePassword} from '../redux/actions/AuthActions';
import Loader from '../components/progress/Loader';
import NetInfo from '@react-native-community/netinfo';

class ChangePasswordScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentUser: props.currentUser,
      currentLang: props.currentLang,
      oldPassword: '',
      errOldPassword: undefined,
      password: '',
      errPassword: undefined,
      confirmPassword: '',
      errConfirmPassword: undefined,
      isOldPasswordVisible: false,
      isPasswordVisible: false,
      isPasswordVisibleConfirm: false,
    };
  }
  doSubmit = () => {
    const {
      currentUser,
      isConnected,
      userId,
      oldPassword,
      password,
      confirmPassword,
    } = this.state;

    if (oldPassword === '') {
      this.setState(
        {
          errOldPassword: getErrorValue(
            errors.ENTER_OLD_PASSWORD,
            this.state.currentLang,
          ),
        },
        () => {
          this.verifyInput.focus();
        },
      );
      return;
    }
    this.setState({errOldPassword: undefined});

    if (password === '') {
      this.setState(
        {
          errPassword: getErrorValue(
            errors.ENTER_NEW_PASSWORD,
            this.state.currentLang,
          ),
        },
        () => {
          this.passwordInput.focus();
        },
      );
      return;
    }
    this.setState({errPassword: undefined});
    if (confirmPassword === '') {
      this.setState(
        {
          errConfirmPassword: getErrorValue(
            errors.ENTER_CONFIRM_PASSWORD,
            this.state.currentLang,
          ),
        },
        () => {
          this.passwordConfirmInput.focus();
        },
      );
      return;
    }
    this.setState({errConfirmPassword: undefined});
    if (password !== confirmPassword) {
      this.setState(
        {
          errPassword: getErrorValue(
            errors.VALID_CONFIRM_PASSWORD,
            this.state.currentLang,
          ),
          errConfirmPassword: getErrorValue(
            errors.VALID_CONFIRM_PASSWORD,
            this.state.currentLang,
          ),
        },
        () => {
          this.passwordConfirmInput.focus();
        },
      );
      return;
    }
    this.setState({errPassword: undefined, errConfirmPassword: undefined});

    if (isConnected) {
      const params = {
        apiToken: currentUser !== undefined ? currentUser.token : '',
        old_password: oldPassword,
        new_password: password,
        userId: userId,
      };
      this.props.doPostChangePassword(params).catch(error => {
        showErrorMessage(error.message);
      });
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  };
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected,
    });
  };
  componentDidMount() {
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
  }

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (
      this.props.responseChangePassword !== prevProps.responseChangePassword
    ) {
      if (this.props.responseChangePassword !== undefined) {
        const {code, status, message} = this.props.responseChangePassword;
        switch (code) {
          case 200:
            this.setState({oldPassword: '', password: '', confirmPassword: ''});
            showErrorMessage(message);
            this.doBackClick();
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  componentWillUnmount() {
    this._subscription();
  }
  render() {
    return (
      <View style={ChangePasswordStyle.container}>
        <View style={ChangePasswordStyle.viewHeader}>
          <Text style={ChangePasswordStyle.textHeaderForgot}>
            {getLangValue(
              strings.HEADER_CHANGE_PASSWORD,
              this.state.currentLang,
            )}
          </Text>
          <View style={ChangePasswordStyle.viewBack}>
            <TouchableOpacity onPress={() => this.doBackClick()}>
              <Image
                source={icons.BACK}
                style={ChangePasswordStyle.imgBack}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={ChangePasswordStyle.viewMain}>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <View style={{flex: 1}}>
              <Reinput
                ref={input => (this.verifyInput = input)}
                label={getLangValue(
                  strings.OLD_PASSWORD,
                  this.state.currentLang,
                )}
                fontFamily={fonts.Dosis_Regular}
                labelColor={colors.colorLabelInActive}
                labelActiveColor={colors.colorLabelActive}
                color={colors.white}
                activeColor={colors.white}
                value={this.state.oldPassword}
                onChangeText={text => {
                  this.setState({oldPassword: text}, () => {
                    this.state.oldPassword === ''
                      ? this.setState({
                          errOldPassword: getErrorValue(
                            errors.ENTER_OLD_PASSWORD,
                            this.state.currentLang,
                          ),
                        })
                      : this.setState({errOldPassword: undefined});
                  });
                }}
                error={this.state.errOldPassword}
                fontSize={RFPercentage(3)}
                underlineColor={colors.colorLine}
                secureTextEntry={!this.state.isOldPasswordVisible}
                returnKeyType="next"
                onSubmitEditing={() => {
                  this.state.oldPassword.length > 0
                    ? this.setState({errOldPassword: undefined})
                    : this.passwordConfirmInput.focus();
                }}
              />
            </View>
            <View
              style={{
                position: 'absolute',
                end: 0,
                top: hp(2),
                marginBottom: 5,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    isOldPasswordVisible: !this.state.isOldPasswordVisible,
                  });
                }}>
                <Image
                  resizeMode="contain"
                  source={icons.EYE}
                  style={{height: hp(5), width: wp(5)}}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <View style={{flex: 1}}>
              <Reinput
                ref={input => (this.passwordInput = input)}
                label={getLangValue(
                  strings.NEW_PASSWORD,
                  this.state.currentLang,
                )}
                fontFamily={fonts.Dosis_Regular}
                labelColor={colors.colorLabelInActive}
                labelActiveColor={colors.colorLabelActive}
                color={colors.white}
                activeColor={colors.white}
                value={this.state.password}
                onChangeText={text => {
                  this.setState({password: text}, () => {
                    this.state.password === ''
                      ? this.setState({
                          errPasswordn: getErrorValue(
                            errors.ENTER_PASSWORD,
                            this.state.currentLang,
                          ),
                        })
                      : this.setState({errPassword: undefined});
                  });
                }}
                error={this.state.errPassword}
                fontSize={RFPercentage(3)}
                underlineColor={colors.colorLine}
                secureTextEntry={!this.state.isPasswordVisible}
                returnKeyType="next"
                onSubmitEditing={() => {
                  this.state.password.length > 0
                    ? this.setState({errPassword: undefined})
                    : this.passwordConfirmInput.focus();
                }}
              />
            </View>
            <View
              style={{
                position: 'absolute',
                end: 0,
                top: hp(2),
                marginBottom: 5,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    isPasswordVisible: !this.state.isPasswordVisible,
                  });
                }}>
                <Image
                  resizeMode="contain"
                  source={icons.EYE}
                  style={{height: hp(5), width: wp(5)}}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <View style={{flex: 1}}>
              <Reinput
                ref={input => (this.passwordConfirmInput = input)}
                label={getLangValue(
                  strings.CONFIRM_PASSWORD,
                  this.state.currentLang,
                )}
                fontFamily={fonts.Dosis_Regular}
                labelColor={colors.colorLabelInActive}
                labelActiveColor={colors.colorLabelActive}
                color={colors.white}
                activeColor={colors.white}
                value={this.state.confirmPassword}
                onChangeText={text => {
                  this.setState({confirmPassword: text}, () => {
                    this.state.confirmPassword === ''
                      ? this.setState({
                          errConfirmPassword: getErrorValue(
                            errors.ENTER_PASSWORD,
                            this.state.currentLang,
                          ),
                        })
                      : this.setState({errConfirmPassword: undefined});
                  });
                }}
                error={this.state.errConfirmPassword}
                fontSize={RFPercentage(3)}
                underlineColor={colors.colorLine}
                secureTextEntry={!this.state.isPasswordVisibleConfirm}
                autoCapitalize="none"
                returnKeyType="done"
                onSubmitEditing={() => {
                  this.state.confirmPassword.length > 0
                    ? this.setState({errConfirmPassword: undefined})
                    : null;
                }}
              />
            </View>
            <View
              style={{
                position: 'absolute',
                end: 0,
                top: hp(2),
                marginBottom: 5,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    isPasswordVisibleConfirm: !this.state
                      .isPasswordVisibleConfirm,
                  });
                }}>
                <Image
                  resizeMode="contain"
                  source={icons.EYE}
                  style={{height: hp(5), width: wp(5)}}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View style={ChangePasswordStyle.viewBtnSubmit}>
            <ButtonGradient
              title={getLangValue(strings.BTN_SUBMIT, this.state.currentLang)}
              backgroundColor={colors.orange}
              fontFamily={fonts.Dosis_Bold}
              fontSize={RFPercentage(3)}
              onPress={() => this.doSubmit()}
              width={wp(80)}
              height={hp(6)}
              currentLang={this.state.currentLang}
              borderRadius={23}
            />
          </View>
        </View>
        {this.props.isBusyChangePassword ? <Loader /> : undefined}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    isBusyChangePassword: state.auth.isBusyChangePassword,
    responseChangePassword: state.auth.responseChangePassword,
    errorChangePassword: state.auth.errorChangePassword,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({doPostChangePassword}, dispatch),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChangePasswordScreen);
