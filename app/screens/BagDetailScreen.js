import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView,
  Modal,
} from 'react-native';
import colors from '../resources/colors';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import fonts from '../resources/fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ButtonGradientIcon from '../components/buttons/ButtonGradientIcon';
import {getLangValue, getErrorValue} from '../resources/languages/language';
import strings from '../resources/strings';
import BagDetailStyle from '../resources/styles/BagDetailStyle';
import ButtonGradient from '../components/buttons/ButtonGradient';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../resources/constants';
import Loader from '../components/progress/Loader';
import NetInfo from '@react-native-community/netinfo';
import {
  showErrorMessage,
  generateKey,
  showSuccessMessage,
} from '../resources/validation';
import {
  doGetBagDetail,
  doPostSellReward,
  doPostRedeemReward,
  doUpdateBagList,
} from '../redux/actions/AppActions';
import errors from '../resources/errors';
import LoginAlert from '../components/alert/LoginAlert';
_isMounted = false;
class BagDetailScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentUser: props.currentUser,
      currentLang: props.currentLang,
      imageUrl: '',
      bagId: 0,
      rewardId: 0,
      title: '--',
      description: '--',
      terms: '--',
      isSellModelVisible: false,
      isLoginAlert: false,
      quantity: 1,
      user_quantity: undefined,
      price: '--',
      type: 0,
      code_number: '',
      expiry_date: '',
      isRedeemModalVisible: false,
    };
  }
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  showSellModal() {
    this.setState({isSellModelVisible: true});
  }
  hideSellModal() {
    this.setState({isSellModelVisible: false});
  }
  doSellClick = () => {
    this.showSellModal();
  };
  showRedeemModal = () => {
    this.setState({ isRedeemModalVisible: !this.state.isRedeemModalVisible})
  }

  doRedeemRewardConfirm = () => {
    this.showRedeemModal();
    AsyncStorage.getItem(constants.IS_LOGIN).then(value => {
    if (value && value === 'true') {
      this.setState({isLoginAlert: false});
      if (this.state.isConnected) {
        const params = {
          apiToken:
            this.state.currentUser !== undefined
              ? this.state.currentUser.token
              : '',
          bagId: this.state.bagId,
          rewardId: this.state.rewardId,
          userId:
            this.state.currentUser !== undefined
              ? this.state.currentUser.id
              : 0,
          status: constants.REDEEMED,
        };
        this.props.doPostRedeemReward(params).catch(error => {
          showErrorMessage(error.message);
        });
      } else {
        showErrorMessage(
          getErrorValue(
            errors.NO_INTERNET_CONNECTION,
            this.state.currentLang,
          ),
        );
      }
    } else {
      this.setState({isLoginAlert: true});
    }
  });
  }

  doRedeemReward = () => {
    this.showRedeemModal();
  };

  doConfirmSell = () => {
    this.hideSellModal();
    AsyncStorage.getItem(constants.IS_LOGIN).then(value => {
      if (value && value === 'true') {
        this.setState({isLoginAlert: false});
        if (this.state.isConnected) {
          const params = {
            apiToken:
              this.state.currentUser !== undefined
                ? this.state.currentUser.token
                : '',
            bagId: this.state.bagId,
            rewardId: this.state.rewardId,
            userId:
              this.state.currentUser !== undefined
                ? this.state.currentUser.id
                : 0,
            status: constants.SALE,
            quantity: this.state.quantity,
          };
          this.props.doPostSellReward(params).catch(error => {
            showErrorMessage(error.message);
          });
        } else {
          showErrorMessage(
            getErrorValue(
              errors.NO_INTERNET_CONNECTION,
              this.state.currentLang,
            ),
          );
        }
      } else {
        this.setState({isLoginAlert: true});
      }
    });
  };
  doQtyMinus = () => {
    if (this.state.quantity === 1) {
      return;
    }
    this.setState({quantity: this.state.quantity - 1});
  };
  doQtyPlus = () => {
    if (
      this.state.user_quantity !== undefined &&
      this.state.quantity < this.state.user_quantity
    ) {
      this.setState({
        quantity: this.state.quantity + 1,
      });
    }
  };
  renderRedeemModal() {
    return (
      <Modal
        animationType="slide"
        presentationStyle="overFullScreen"
        transparent={true}
        onRequestClose={() => this.showRedeemModal()}
        visible={this.state.isRedeemModalVisible}>
        <TouchableHighlight
          style={BagDetailStyle.touchModalMain}
          underlayColor={colors.colorTransparent}
          onPress={() => this.showRedeemModal()}>
          <View style={BagDetailStyle.viewModalMain}>
            <View style={BagDetailStyle.viewModalBackground}>
              <Text style={BagDetailStyle.textEnterQty}>
                Confirm redemption of voucher?
              </Text>
              <View style={BagDetailStyle.viewButtonConfirm}>
                <ButtonGradient
                  title={getLangValue(
                    strings.BTN_CONFIRM,
                    this.state.currentLang,
                  )}
                  backgroundColor={colors.orange}
                  fontFamily={fonts.Dosis_Bold}
                  fontSize={RFPercentage(2.5)}
                  onPress={() => this.doRedeemRewardConfirm()}
                  width={wp(70)}
                  height={hp(6)}
                  currentLang={this.state.currentLang}
                  borderRadius={23}
                />
              </View>
            </View>
          </View>
        </TouchableHighlight>
      </Modal>
    );
  }
  renderSellModel() {
    return (
      <Modal
        animationType="slide"
        presentationStyle="overFullScreen"
        transparent={true}
        onRequestClose={() => this.hideSellModal()}
        visible={this.state.isSellModelVisible}>
        <TouchableHighlight
          style={BagDetailStyle.touchModalMain}
          underlayColor={colors.colorTransparent}
          onPress={() => this.hideSellModal()}>
          <View style={BagDetailStyle.viewModalMain}>
            <View style={BagDetailStyle.viewModalBackground}>
              <Text style={BagDetailStyle.textEnterQty}>
                {getLangValue(
                  strings.ENTER_QUANTITY_TO_SELL,
                  this.state.currentLang,
                )}
              </Text>
              <View style={BagDetailStyle.viewQtyItem}>
                <View style={BagDetailStyle.touchImg}>
                  <TouchableOpacity
                    style={{
                      display:
                        this.state.user_quantity !== undefined &&
                        this.state.user_quantity === 1
                          ? 'none'
                          : 'flex',
                    }}
                    onPress={() => this.doQtyMinus()}>
                    <Image
                      source={icons.LEFT}
                      style={BagDetailStyle.imgArrow}
                    />
                  </TouchableOpacity>
                </View>
                <View style={BagDetailStyle.viewQtyText}>
                  <Text style={BagDetailStyle.textQty}>
                    {this.state.quantity}
                  </Text>
                </View>
                <View style={BagDetailStyle.touchImg}>
                  <TouchableOpacity
                    style={{
                      display:
                        this.state.user_quantity !== undefined &&
                        this.state.user_quantity === this.state.quantity
                          ? 'none'
                          : this.state.user_quantity !== undefined &&
                            this.state.user_quantity === 1
                          ? 'none'
                          : 'flex',
                    }}
                    onPress={() => this.doQtyPlus()}>
                    <Image
                      source={icons.RIGHT}
                      style={BagDetailStyle.imgArrow}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={BagDetailStyle.viewButtonConfirm}>
                <ButtonGradient
                  title={getLangValue(
                    strings.BTN_CONFIRM,
                    this.state.currentLang,
                  )}
                  backgroundColor={colors.orange}
                  fontFamily={fonts.Dosis_Bold}
                  fontSize={RFPercentage(2.5)}
                  onPress={() => this.doConfirmSell()}
                  width={wp(70)}
                  height={hp(6)}
                  currentLang={this.state.currentLang}
                  borderRadius={23}
                />
              </View>
            </View>
          </View>
        </TouchableHighlight>
      </Modal>
    );
  }
  renderButtons() {
    if (this.state.type === 0) {
      return (
        <View style={BagDetailStyle.viewButtonRow}>
          <View style={BagDetailStyle.viewButtonSell}>
            <TouchableOpacity onPress={() => this.doSellClick()}>
              <View style={BagDetailStyle.viewSell}>
                <Text style={BagDetailStyle.textSell}>
                  {getLangValue(strings.BTN_SELL, this.state.currentLang)}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={BagDetailStyle.viewButtonRedeem}>
            <ButtonGradientIcon
              title={getLangValue(strings.BTN_REDEEM, this.state.currentLang)}
              backgroundColor={colors.orange}
              fontFamily={fonts.Dosis_Bold}
              fontSize={RFPercentage(3)}
              onPress={() => this.doRedeemReward()}
              width={wp(50)}
              height={hp(10)}
              currentLang={this.state.currentLang}
              borderRadius={0}
              icon={icons.QRCODE}
              iconStyle={BagDetailStyle.imgIcon}
            />
          </View>
        </View>
      );
    } else {
      return undefined;
    }
  }
  getBagDetail() {
    const params = {
      apiToken:
        this.state.currentUser !== undefined
          ? this.state.currentUser.token
          : '',
      bagId: this.state.bagId,
      status: this.state.type,
    };
    this.props.doGetBagDetail(params).catch(error => {
      showErrorMessage(error.message);
    });
  }
  _handleConnectivityChange = state => {
    if (this._isMounted) {
      this.setState({
        isConnected: state.isConnected,
      });
    }
  };
  componentDidMount() {
    this._isMounted = true;
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    const {item, type} = this.props.navigation.state.params;

    if (item) {
      this.setState(
        {
          rewardId: item.coupon_id === undefined ? 0 : item.coupon_id.id,
          bagId: item.coupon_id === undefined ? 0 : item.coupon_id.id,
          code_number: item.code_number,
          type: type,
          expiry_date:
            item.coupon_id === undefined ? '' : item.coupon_id.expiry_date,
        },
        () => {
          this.getBagDetail();
        },
      );
    }
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.currentUser !== prevProps.currentUser) {
      this.setState({currentUser: this.props.currentUser}, () => {
        this.getBagDetail();
      });
    }
    if (this.props.responseBagDetail !== prevProps.responseBagDetail) {
      if (this.props.responseBagDetail !== undefined) {
        const {code, status, message} = this.props.responseBagDetail;
        switch (code) {
          case 200:
            const {result} = this.props.responseBagDetail;
            const {data} = result;
            this.setState({
              imageUrl: data.image,
              title: data.name,
              description: data.description,
              terms: data.terms,
              rewardStatus: data.status,
              price: data.price,
              user_quantity: data.user_quantity,
              expiry_date: data.expiry_date,
            });
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
    if (this.props.responseRewardSell !== prevProps.responseRewardSell) {
      if (this.props.responseBagDetail !== undefined) {
        const {code, status, message} = this.props.responseRewardSell;
        switch (code) {
          case 200:
            if (status) {
              this.props.doUpdateBagList({enable: true, key: generateKey(5)});
              this.props.navigation.navigate('SellSuccess');
            }
            showSuccessMessage(message);

            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
    if (this.props.responseRewardRedeem !== prevProps.responseRewardRedeem) {
      if (this.props.responseRewardRedeem !== undefined) {
        const {code, status, message} = this.props.responseRewardRedeem;
        switch (code) {
          case 200:
            if (status) {
              this.props.doUpdateBagList({enable: true, key: generateKey(5)});
              this.props.navigation.navigate('RedeemSuccess', {
                code_number: this.state.code_number,
                expiry_date: this.state.expiry_date,
              });
            }
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
    this._subscription();
  }
  render() {
    return (
      <View style={BagDetailStyle.container}>
        <View style={BagDetailStyle.viewHeader}>
          <TouchableOpacity onPress={() => this.doBackClick()}>
            <Image source={icons.BACK} style={BagDetailStyle.imgBack} />
          </TouchableOpacity>
        </View>
        <ScrollView
          style={{flex: 1, marginBottom: this.state.type === 0 ? hp(10) : 0}}>
          <View style={BagDetailStyle.viewMain}>
            <View style={BagDetailStyle.viewRewardImg}>
              <Image
                source={{uri: this.state.imageUrl}}
                style={BagDetailStyle.imgReward}
                resizeMode="cover"
              />
            </View>
            <Text style={BagDetailStyle.textTitle}>{this.state.title}</Text>

            <Text style={BagDetailStyle.textDescription}>
              {`$${this.state.price}`}
            </Text>
            {this.state.type === 1 ? (
              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'row',
                  marginTop: hp(1),
                }}>
                <Text style={BagDetailStyle.textUniqueIdLabel}>
                  {getLangValue(strings.UNIQUE_ID, this.state.currentLang)}
                </Text>
                <View
                  style={{
                    backgroundColor: colors.white,
                    padding: 5,
                    marginStart: wp(2),
                    borderRadius: 4,
                  }}>
                  <Text style={BagDetailStyle.textUniqueIdValue}>
                    {this.state.code_number}
                  </Text>
                </View>
              </View>
            ) : null}

            <Text style={BagDetailStyle.textTerms}>
              {this.state.description}
            </Text>
            <Text style={BagDetailStyle.textTerms}>{this.state.terms}</Text>
          </View>
        </ScrollView>
        {this.renderButtons()}
        {this.state.isRedeemModalVisible ? this.renderRedeemModal() : undefined}
        {this.state.isSellModelVisible ? this.renderSellModel() : undefined}
        {this.props.isBusyBagDetail || this.props.isBusyRewardSell ? (
          <Loader />
        ) : (
          undefined
        )}
        {this.state.isLoginAlert ? (
          <LoginAlert
            isLoginAlert={this.state.isLoginAlert}
            navigation={this.props.navigation}
            doNoClick={value => {
              this.setState({isLoginAlert: value});
            }}
          />
        ) : (
          undefined
        )}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    isBusyBagDetail: state.app.isBusyBagDetail,
    responseBagDetail: state.app.responseBagDetail,
    errorBagDetail: state.app.errorBagDetail,
    isBusyRewardSell: state.app.isBusyRewardSell,
    responseRewardSell: state.app.responseRewardSell,
    errorRewardSell: state.app.errorRewardSell,
    isBusyRewardRedeem: state.app.isBusyRewardRedeem,
    responseRewardRedeem: state.app.responseRewardRedeem,
    errorRewardRedeem: state.app.errorRewardRedeem,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {doGetBagDetail, doPostSellReward, doPostRedeemReward, doUpdateBagList},
      dispatch,
    ),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BagDetailScreen);
