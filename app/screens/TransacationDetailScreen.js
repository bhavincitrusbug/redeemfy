import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {View, Text, Image, TouchableOpacity, ScrollView} from 'react-native';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {getLangValue} from '../resources/languages/language';
import strings from '../resources/strings';
import TransacationDetailStyle from '../resources/styles/TransacationDetailStyle';
import {showErrorMessage, generateKey} from '../resources/validation';
import {
  doGetTransacationDetail,
  doPostRewardDetail,
  doUpdateCouponList,
} from '../redux/actions/AppActions';
import Loader from '../components/progress/Loader';
import NetInfo from '@react-native-community/netinfo';

class TransacationDetailScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentUser: props.currentUser,
      currentLang: props.currentLang,
      type: props.navigation.state.params.type,
      transactionId: props.navigation.state.params.item.id,
      transactionIdLabel: '',
      transactionType: '',
      discountedPrice: 0,
      serverFeeDiscount: 0,
      serverFeeDiscountedPrice: 0,
      totalPrice: '',
      imageUrl: '',
      title: '--',
      description: '--',
      discount: 0,
      price: 0,
      terms: '--',
      rewardStatus: 0,
    };
    this._isMounted = false;
  }
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };

  getTrancationDetail() {
    const params = {
      apiToken:
        this.state.currentUser !== undefined
          ? this.state.currentUser.token
          : '',
      transactionId: this.state.transactionId,
    };
    this.props.doGetTransacationDetail(params).catch(error => {
      showErrorMessage(error.message);
    });
  }
  _handleConnectivityChange = state => {
    if (this._isMounted) {
      this.setState({
        isConnected: state.isConnected,
      });
    }
  };
  componentDidMount() {
    this._isMounted = true;
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    const {item} = this.props.navigation.state.params;
    if (item) {
      this.getTrancationDetail();
    }
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.currentUser !== prevProps.currentUser) {
      this.setState({currentUser: this.props.currentUser}, () => {
        this.getTrancationDetail();
      });
    }
    if (
      this.props.responseTransactionDetail !==
      prevProps.responseTransactionDetail
    ) {
      if (this.props.responseTransactionDetail !== undefined) {
        const {code, status, message} = this.props.responseTransactionDetail;
        switch (code) {
          case 200:
            const {result} = this.props.responseTransactionDetail;
            const {data} = result;
            this.setState({
              imageUrl: data.coupons_id.image,
              title: data.coupons_id.name,
              description: data.coupons_id.description,
              terms: data.coupons_id.terms,
              rewardStatus: data.coupons_id.status,
              price: data.coupons_id.price,
              discount: data.coupons_id.discount,
              transactionIdLabel: data.transaction_id,
              transactionType:
                data.payment_types === 'sale'
                  ? 'Sell'
                  : data.payment_types === 'buy'
                  ? 'Buy'
                  : '--',
              discountedPrice: data.amount * data.coupons_id.discount,
              totalPrice: data.amount - data.amount * data.coupons_id.discount,
              serverFeeDiscount: data.admin_commission,
              serverFeeDiscountedPrice: data.admin_commission_amount,
            });
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
    if (this.props.responseRewardClaim !== prevProps.responseRewardClaim) {
      if (this.props.responseTransactionDetail !== undefined) {
        const {code, status, message} = this.props.responseRewardClaim;
        switch (code) {
          case 200:
            if (status) {
              this.props.doUpdateCouponList({
                enable: true,
                key: generateKey(5),
              });
              this.props.navigation.navigate('ClaimSuccess');
            }
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
    this._subscription();
  }

  render() {
    return (
      <View style={TransacationDetailStyle.container}>
        <View style={TransacationDetailStyle.viewHeader}>
          <TouchableOpacity onPress={() => this.doBackClick()}>
            <Image
              source={icons.BACK}
              style={TransacationDetailStyle.imgBack}
            />
          </TouchableOpacity>
        </View>
        <ScrollView
          style={{flex: 1, marginBottom: this.state.type === 0 ? hp(10) : 0}}>
          <View style={TransacationDetailStyle.viewMain}>
            <View style={TransacationDetailStyle.viewRewardImg}>
              {this.state.imageUrl !== '' ? (
                <Image
                  source={{uri: this.state.imageUrl}}
                  style={TransacationDetailStyle.imgReward}
                  resizeMode="cover"
                />
              ) : (
                undefined
              )}
            </View>
            <Text style={TransacationDetailStyle.textTitle}>
              {this.state.title}
            </Text>
            {/* <Text style={TransacationDetailStyle.textDescription}>
              {`$${this.state.price}`}
            </Text> */}
            <Text style={TransacationDetailStyle.textDescription}>
              {this.state.description}
            </Text>
            <Text style={TransacationDetailStyle.textTerms}>
              {this.state.terms}
            </Text>
            <View style={TransacationDetailStyle.rowItem}>
              <Text style={TransacationDetailStyle.textRowValue}>
                {getLangValue(strings.TRANSCATION_ID, this.state.currentLang)} :{' '}
                {this.state.transactionIdLabel}
              </Text>
            </View>
            <View style={TransacationDetailStyle.rowItem}>
              <Text style={TransacationDetailStyle.textRowItemOpacity}>
                {getLangValue(strings.TRANSCATION_TYPE, this.state.currentLang)}
              </Text>
              <Text style={TransacationDetailStyle.textRowValueOpacity}>
                {this.state.transactionType}
              </Text>
            </View>
            <View style={TransacationDetailStyle.rowItem}>
              <Text style={TransacationDetailStyle.textRowItemOpacity}>
                {getLangValue(strings.AMOUNT, this.state.currentLang)}
              </Text>
              <Text style={TransacationDetailStyle.textRowValueOpacity}>
                ${this.state.price}
              </Text>
            </View>
            <View style={TransacationDetailStyle.rowItem}>
              <Text style={TransacationDetailStyle.textRowItemOpacity}>
                {getLangValue(strings.DISCOUNT, this.state.currentLang)}(
                {this.state.discount}%)
              </Text>
              <Text style={TransacationDetailStyle.textRowValueOpacity}>
                ${this.state.discountedPrice}
              </Text>
            </View>
            {this.state.transactionType === 'Sell' ? (
              <View style={TransacationDetailStyle.rowItem}>
                <Text style={TransacationDetailStyle.textRowItemOpacity}>
                  {getLangValue(
                    strings.REDEEMFY_SERVICE_FEE,
                    this.state.currentLang,
                  )}
                  ({this.state.serverFeeDiscount}%)
                </Text>
                <Text style={TransacationDetailStyle.textRowValueOpacity}>
                  ${this.state.serverFeeDiscountedPrice}
                </Text>
              </View>
            ) : null}

            <View style={TransacationDetailStyle.viewLine} />
            <View style={TransacationDetailStyle.rowCenter}>
              <Text style={TransacationDetailStyle.textRowItem}>
                {getLangValue(strings.TOTAL, this.state.currentLang)}
              </Text>
              <Text style={TransacationDetailStyle.textRowValue}>
                ${this.state.totalPrice}
              </Text>
            </View>
          </View>
        </ScrollView>

        {this.props.isBusyTransactionDetail || this.props.isBusyRewardClaim ? (
          <Loader />
        ) : (
          undefined
        )}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    isBusyTransactionDetail: state.app.isBusyTransactionDetail,
    responseTransactionDetail: state.app.responseTransactionDetail,
    errorTransactionDetail: state.app.errorTransactionDetail,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {doGetTransacationDetail, doPostRewardDetail, doUpdateCouponList},
      dispatch,
    ),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TransacationDetailScreen);
