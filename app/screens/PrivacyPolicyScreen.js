import React, {PureComponent} from 'react';
import {View, Image, TouchableOpacity, Platform, Text} from 'react-native';
import icons from '../resources/icons';
import TermsPrivacyStyle from '../resources/styles/TermsPrivacyStyle';
import {WebView} from 'react-native-webview';
import colors from '../resources/colors';
import Loader from '../components/progress/Loader';
import {getLangValue} from '../resources/languages/language';
import strings from '../resources/strings';
import constants from '../resources/constants';
const PolicyHTML = require('../resources/privacy_policy.html');

class PrivacyPolicyScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentLang: props.currentLang,
      title: 'TITLE',
      message: 'Condition to Cash Out',
      terms: 'More terms and condition',
      loading: true,
    };
  }
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  render() {
    const INJECTEDJAVASCRIPT = `const meta = document.createElement('meta'); meta.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta); `;

    return (
      <View style={TermsPrivacyStyle.container}>
        <View style={TermsPrivacyStyle.viewHeader}>
          <Text style={TermsPrivacyStyle.textHeader}>
            {getLangValue(
              strings.HEADER_PRIVACY_POLICY,
              this.state.currentLang,
            )}
          </Text>
          <View style={{position: 'absolute'}}>
            <TouchableOpacity onPress={() => this.doBackClick()}>
              <Image source={icons.BACK} style={TermsPrivacyStyle.imgBack} />
            </TouchableOpacity>
          </View>
        </View>
        <WebView
          allowFileAccess={true}
          domStorageEnabled={true}
          allowUniversalAccessFromFileURLs={true}
          allowFileAccessFromFileURLs={true}
          javaScriptEnabled={true}
          mixedContentMode="always"
          originWhitelist={['*']}
          source={{uri: constants.PRIVACY_POLICY}}
          style={{backgroundColor: colors.colorBackground}}
          onLoadStart={() => this.setState({loading: true})}
          onLoadEnd={() => this.setState({loading: false})}
          scalesPageToFit={true}
          injectedJavaScript={INJECTEDJAVASCRIPT}
          scrollEnabled={true}
        />
        {/* <ScrollView>
          <View style={TermsPrivacyStyle.viewMain}>
            <Text style={TermsPrivacyStyle.textTitle}>{this.state.title}</Text>
            <Text style={TermsPrivacyStyle.textMessage}>
              {this.state.message}
            </Text>
            <Text style={TermsPrivacyStyle.textTerms}>{this.state.terms}</Text>
          </View>
        </ScrollView> */}
        {this.state.loading ? <Loader /> : undefined}
      </View>
    );
  }
}
export default PrivacyPolicyScreen;
