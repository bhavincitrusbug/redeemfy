import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {View, Text, TouchableOpacity, Image, FlatList} from 'react-native';
import {getLangValue} from '../resources/languages/language';
import strings from '../resources/strings';
import BagTabStyle from '../resources/styles/BagTabStyle';
import icons from '../resources/icons';
import BagTab from '../components/tabs/BagTab';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import BagListItem from '../components/list/BagListItem';
import {
  doGetBagInventoryList,
  doGetBagRedeemedList,
  doUpdateBagList,
} from '../redux/actions/AppActions';
import {showErrorMessage} from '../resources/validation';
import Loader from '../components/progress/Loader';
class BagTabScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentLang: strings.LAG_ENG,
      currentUser: props.currentUser,
      viewEmptyInventory: false,
      viewEmptyComing: false,
      refreshing_inventory: false,
      refreshing_redeemed: false,
      isActiveInventory: true,
      isActiveRedeemed: false,
      inventory_page: 1,
      inventory_last_page: 0,
      redeemed_page: 1,
      redeemed__last_page: 0,
      bagsInventory: [],
      bagsRedeemed: [],
      rewards: [
        {
          rewardId: 1,
          title: 'VR Glass 3.9',
          description: '$20 Cash Voucher',
          image:
            'https://www.jabra.in/-/media/Images/Products/Jabra-Elite-85h/Product/elite_85h_navy_01.png?w=555&la=en-IN&hash=1E80CF674AF05BA1BFCDC7B0C9B25A37966FDCF7',
          total: 120,
          used: 20,
          quantity: 10,
        },
        {
          rewardId: 1,
          title: 'VR Glass 3.9',
          description: '$20 Cash Voucher',
          image:
            'https://www.jabra.in/-/media/Images/Products/Jabra-Elite-85h/Product/elite_85h_navy_01.png?w=555&la=en-IN&hash=1E80CF674AF05BA1BFCDC7B0C9B25A37966FDCF7',
          total: 120,
          used: 30,
          quantity: 10,
        },
        {
          rewardId: 1,
          title: 'VR Glass 3.9',
          description: '$20 Cash Voucher',
          image:
            'https://www.jabra.in/-/media/Images/Products/Jabra-Elite-85h/Product/elite_85h_navy_01.png?w=555&la=en-IN&hash=1E80CF674AF05BA1BFCDC7B0C9B25A37966FDCF7',
          total: 120,
          used: 50,
          quantity: 10,
        },
        {
          rewardId: 1,
          title: 'VR Glass 3.9',
          description: '$20 Cash Voucher',
          image:
            'https://www.jabra.in/-/media/Images/Products/Jabra-Elite-85h/Product/elite_85h_navy_01.png?w=555&la=en-IN&hash=1E80CF674AF05BA1BFCDC7B0C9B25A37966FDCF7',
          total: 120,
          used: 80,
          quantity: 10,
        },
        {
          rewardId: 1,
          title: 'VR Glass 3.9',
          description: '$20 Cash Voucher',
          image:
            'https://www.jabra.in/-/media/Images/Products/Jabra-Elite-85h/Product/elite_85h_navy_01.png?w=555&la=en-IN&hash=1E80CF674AF05BA1BFCDC7B0C9B25A37966FDCF7',
          total: 120,
          used: 90,
          quantity: 10,
        },
        {
          rewardId: 1,
          title: 'VR Glass 3.9',
          description: '$20 Cash Voucher',
          image:
            'https://www.jabra.in/-/media/Images/Products/Jabra-Elite-85h/Product/elite_85h_navy_01.png?w=555&la=en-IN&hash=1E80CF674AF05BA1BFCDC7B0C9B25A37966FDCF7',
          total: 120,
          used: 100,
          quantity: 10,
        },
        {
          rewardId: 1,
          title: 'VR Glass 3.9',
          description: '$20 Cash Voucher',
          image:
            'https://www.jabra.in/-/media/Images/Products/Jabra-Elite-85h/Product/elite_85h_navy_01.png?w=555&la=en-IN&hash=1E80CF674AF05BA1BFCDC7B0C9B25A37966FDCF7',
          total: 120,
          used: 80,
          quantity: 10,
        },
      ],
      status: 0,
    };
  }
  doBackClick() {
    this.props.navigation.goBack(null);
  }
  doOpenRewardDetail(item, type) {
    this.props.navigation.navigate('BagDetail', {item: item, type: type});
  }
  onRefresh(title) {
    if (title === getLangValue(strings.INVENTORY, this.state.currentLang)) {
      this.setState({inventory_page: 1, refreshing_inventory: true}, () => {
        this.getBagList(title);
      });
    } else {
      this.setState({redeemed_page: 1, refreshing_redeemed: true}, () => {
        this.getBagList(title);
      });
    }
  }
  handleLoadMore(title) {
    if (title === getLangValue(strings.INVENTORY, this.state.currentLang)) {
      if (this.state.inventory_last_page !== 0) {
        if (this.state.inventory_last_page !== this.state.inventory_page) {
          this.setState(
            (prevState, nextProps) => ({
              inventory_page: prevState.inventory_page + 1,
            }),
            () => {
              this.getBagList(title);
            },
          );
        }
      }
    } else {
      if (this.state.redeemed__last_page !== 0) {
        if (this.state.redeemed__last_page !== this.state.redeemed_page) {
          this.setState(
            (prevState, nextProps) => ({
              redeemed_page: prevState.redeemed_page + 1,
            }),
            () => {
              this.getBagList(title);
            },
          );
        }
      }
    }
  }
  renderEmpty = () => {
    return (
      <View style={BagTabStyle.viewEmptyMain}>
        <Image
          resizeMode="contain"
          source={icons.NO_BAG}
          style={BagTabStyle.imgNoReward}
        />
        <Text style={BagTabStyle.textNoRewards}>
          {getLangValue(strings.NO_BAGS, this.state.currentLang)}
        </Text>
      </View>
    );
  };
  renderTabOne() {
    // Do your operations

    return (
      <View>
        <View
          style={{
            marginStart: wp(2),
            marginEnd: wp(2),
            marginTop: hp(2),
            marginBottom: hp(5),
          }}>
          <FlatList
            key={4112128}
            data={this.state.bagsInventory}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => this.doOpenRewardDetail(item, 0)}>
                <BagListItem
                  image_url={item.coupon_id.image}
                  title={item.coupon_id.name}
                  description={`$${item.coupon_id.price}`}
                  total={item.coupon_id.quantity}
                  used={item.coupon_id.quantity - item.coupon_id.quantity_left}
                  quantity={item.coupon_id.user_quantity}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index.toString()}
            onRefresh={() =>
              this.onRefresh(
                getLangValue(strings.INVENTORY, this.state.currentLang),
              )
            }
            refreshing={this.state.refreshing_inventory}
            onEndReached={() =>
              this.handleLoadMore(
                getLangValue(strings.INVENTORY, this.state.currentLang),
              )
            }
            onEndReachedThreshold={15}
            contentContainerStyle={
              this.state.viewEmptyInventory && BagTabStyle.listContent
            }
            ListEmptyComponent={
              this.state.viewEmptyInventory ? this.renderEmpty : null
            }
          />
        </View>
      </View>
    );
  }

  renderTabTwo() {
    // Do your operations

    return (
      <View>
        <View
          style={{
            marginStart: wp(2),
            marginEnd: wp(2),
            marginTop: hp(2),
            marginBottom: hp(5),
          }}>
          <FlatList
            key={4112129}
            data={this.state.bagsRedeemed}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => this.doOpenRewardDetail(item, 1)}>
                <BagListItem
                  image_url={item.coupon_id.image}
                  title={item.coupon_id.name}
                  description={`$${item.coupon_id.price}`}
                  total={item.coupon_id.quantity}
                  used={item.coupon_id.quantity - item.coupon_id.quantity_left}
                  quantity={item.coupon_id.user_quantity}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index.toString()}
            onRefresh={() =>
              this.onRefresh(
                getLangValue(strings.REDEEMED, this.state.currentLang),
              )
            }
            refreshing={this.state.refreshing_redeemed}
            onEndReached={() =>
              this.handleLoadMore(
                getLangValue(strings.REDEEMED, this.state.currentLang),
              )
            }
            onEndReachedThreshold={15}
            contentContainerStyle={
              this.state.viewEmptyComing && BagTabStyle.listContent
            }
            ListEmptyComponent={
              this.state.viewEmptyComing ? this.renderEmpty : null
            }
          />
        </View>
      </View>
    );
  }
  getBagList(title) {
    if (title === getLangValue(strings.INVENTORY, this.state.currentLang)) {
      this.setState(
        {status: 0, isActiveInventory: true, isActiveRedeemed: false},
        () => {
          const params = {
            apiToken:
              this.state.currentUser !== undefined
                ? this.state.currentUser.token
                : '',
            status: this.state.status,
          };
          this.props.doGetBagInventoryList(params).catch(error => {
            showErrorMessage(error.message);
          });
        },
      );
    } else {
      this.setState(
        {status: 1, isActiveInventory: false, isActiveRedeemed: true},
        () => {
          const params = {
            apiToken:
              this.state.currentUser !== undefined
                ? this.state.currentUser.token
                : '',
            status: this.state.status,
          };
          this.props.doGetBagRedeemedList(params).catch(error => {
            showErrorMessage(error.message);
          });
        },
      );
    }
  }
  componentDidMount() {
    this.getBagList(getLangValue(strings.INVENTORY, this.state.currentLang));
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (
      this.props.responseBagInventoryList !== prevProps.responseBagInventoryList
    ) {
      if (this.props.responseBagInventoryList !== undefined) {
        const {code, status, message} = this.props.responseBagInventoryList;
        switch (code) {
          case 200:
            const {data, links} = this.props.responseBagInventoryList.result;
            const {last, current} = links;
            if (current === 1) {
              this.setState({
                bagsInventory: data,
                refreshing_inventory: false,
                inventory_last_page: last,
                viewEmptyInventory: data.length <= 0,
              });
            } else {
              this.setState({
                bagsInventory: [...this.state.bagsInventory, ...data],
                refreshing_inventory: false,
                inventory_last_page: last,
                viewEmptyInventory: false,
              });
            }
            break;
          case 400:
            this.setState({
              bagsInventory: [],
              refreshing_inventory: false,
              inventory_last_page: 0,
              viewEmptyInventory: true,
            });
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
    if (
      this.props.responseBagRedeemedList !== prevProps.responseBagRedeemedList
    ) {
      if (this.props.responseBagRedeemedList !== undefined) {
        const {code, status, message} = this.props.responseBagRedeemedList;
        switch (code) {
          case 200:
            const {result} = this.props.responseBagRedeemedList;
            const {data, links} = result;
            const {last, current} = links;
            if (current === 1) {
              this.setState({
                bagsRedeemed: data,
                refreshing_redeemed: false,
                redeemed__last_page: last,
                viewEmptyComing: data.length <= 0,
              });
            } else {
              this.setState({
                bagsRedeemed: [...this.state.bagsRedeemed, ...data],
                refreshing_redeemed: false,
                redeemed__last_page: last,
                viewEmptyComing: false,
              });
            }
            break;
          case 400:
            this.setState({
              bagsInventory: [],
              refreshing_redeemed: false,
              redeemed__last_page: 0,
              viewEmptyComing: true,
            });
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
    if (this.props.bagListUpdate !== prevProps.bagListUpdate) {
      const {enable} = this.props.bagListUpdate;
      if (enable) {
        if (this.state.isActiveInventory) {
          this.getBagList(
            getLangValue(strings.INVENTORY, this.state.currentLang),
          );
        } else {
          this.getBagList(
            getLangValue(strings.REDEEMED, this.state.currentLang),
          );
        }

        this.props.doUpdateBagList({enable: false, key: ''});
      }
    }
  }
  render() {
    return (
      <View style={BagTabStyle.container}>
        <View style={BagTabStyle.viewHeader}>
          <Text style={BagTabStyle.textHeaderHome}>
            {getLangValue(strings.HEADER_BAG, this.state.currentLang)}
          </Text>
          <View style={BagTabStyle.viewBack}>
            <TouchableOpacity onPress={() => this.doBackClick()}>
              <Image
                source={icons.BACK}
                style={BagTabStyle.imgBack}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{flex: 1}}>
          <BagTab
            tabOneTitle={getLangValue(strings.INVENTORY, strings.LAG_ENG)}
            tabTwoTitle={getLangValue(strings.REDEEMED, strings.LAG_ENG)}
            renderTabOne={() => this.renderTabOne()}
            renderTabTwo={() => this.renderTabTwo()}
            getBagList={title => this.getBagList(title)}
          />
        </View>
        {this.props.isBusyBagInventoryList ||
        this.props.isBusyBagRedeemedList ? (
          <Loader />
        ) : (
          undefined
        )}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    bagListUpdate: state.app.bagListUpdate,
    isBusyBagInventoryList: state.app.isBusyBagInventoryList,
    responseBagInventoryList: state.app.responseBagInventoryList,
    errorBagInventoryList: state.app.errorBagInventoryList,
    isBusyBagRedeemedList: state.app.isBusyBagRedeemedList,
    responseBagRedeemedList: state.app.responseBagRedeemedList,
    errorBagRedeemedList: state.app.errorBagRedeemedList,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {doGetBagInventoryList, doGetBagRedeemedList, doUpdateBagList},
      dispatch,
    ),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(BagTabScreen);
