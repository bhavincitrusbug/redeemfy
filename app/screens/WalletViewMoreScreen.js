import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {View, TouchableOpacity, Image, Text, FlatList} from 'react-native';
import strings from '../resources/strings';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {getLangValue} from '../resources/languages/language';
import WalletStyle from '../resources/styles/WalletStyle';
import TrasacationListItem from '../components/list/TrasacationListItem';
import {showErrorMessage} from '../resources/validation';
import NetInfo from '@react-native-community/netinfo';
import Loader from '../components/progress/Loader';
import {doGetTransacationsViewMore} from '../redux/actions/AppActions';
import moment from 'moment';
import TransactionTab from '../components/tabs/TransactionTab';
_isMounted = false;
class WalletViewMoreScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentLang: props.currentLang,
      currentUser: props.currentUser,
      refreshing: false,
      viewEmpty: false,
      page: 1,
      last_page: 1,
      transacationListData: [],
      transacationList: [
        {
          id: 1,
          title: 'Arctis Pro Wireless',
          date: '12th Jan, 2018 12:00 PM',
          price: 2000,
          image:
            'https://media.steelseriescdn.com/thumbs/filer_public/aa/1b/aa1b5f10-ed38-4553-a1c0-7adace4100db/purchase-gallery-apro-wireless-hero.png__1850x800_q100_crop-scale_optimize_subsampling-2.png',
          transacationType: 54,
        },
        {
          id: 1,
          title: 'Arctis Pro Wireless',
          date: '12th Jan, 2018 12:00 PM',
          price: 2000,
          image:
            'https://media.steelseriescdn.com/thumbs/filer_public/aa/1b/aa1b5f10-ed38-4553-a1c0-7adace4100db/purchase-gallery-apro-wireless-hero.png__1850x800_q100_crop-scale_optimize_subsampling-2.png',
          transacationType: 0,
        },
        {
          id: 1,
          title: 'Arctis Pro Wireless',
          date: '12th Jan, 2018 12:00 PM',
          price: 3000,
          image:
            'https://media.steelseriescdn.com/thumbs/filer_public/aa/1b/aa1b5f10-ed38-4553-a1c0-7adace4100db/purchase-gallery-apro-wireless-hero.png__1850x800_q100_crop-scale_optimize_subsampling-2.png',
          transacationType: 1,
        },
        {
          id: 1,
          title: 'Arctis Pro Wireless',
          date: '12th Jan, 2018 12:00 PM',
          price: 3500,
          image:
            'https://media.steelseriescdn.com/thumbs/filer_public/aa/1b/aa1b5f10-ed38-4553-a1c0-7adace4100db/purchase-gallery-apro-wireless-hero.png__1850x800_q100_crop-scale_optimize_subsampling-2.png',
          transacationType: 0,
        },
        {
          id: 1,
          title: 'Arctis Pro Wireless',
          date: '12th Jan, 2018 12:00 PM',
          price: 3000,
          image:
            'https://media.steelseriescdn.com/thumbs/filer_public/aa/1b/aa1b5f10-ed38-4553-a1c0-7adace4100db/purchase-gallery-apro-wireless-hero.png__1850x800_q100_crop-scale_optimize_subsampling-2.png',
          transacationType: 1,
        },
        {
          id: 1,
          title: 'Arctis Pro Wireless',
          date: '12th Jan, 2018 12:00 PM',
          price: 3000,
          image:
            'https://media.steelseriescdn.com/thumbs/filer_public/aa/1b/aa1b5f10-ed38-4553-a1c0-7adace4100db/purchase-gallery-apro-wireless-hero.png__1850x800_q100_crop-scale_optimize_subsampling-2.png',
          transacationType: 2,
        },
        {
          id: 1,
          title: 'Arctis Pro Wireless',
          date: '12th Jan, 2018 12:00 PM',
          price: 4000,
          image:
            'https://media.steelseriescdn.com/thumbs/filer_public/aa/1b/aa1b5f10-ed38-4553-a1c0-7adace4100db/purchase-gallery-apro-wireless-hero.png__1850x800_q100_crop-scale_optimize_subsampling-2.png',
          transacationType: 0,
        },
        {
          id: 1,
          title: 'Arctis Pro Wireless',
          date: '12th Jan, 2018 12:00 PM',
          price: 3000,
          image:
            'https://media.steelseriescdn.com/thumbs/filer_public/aa/1b/aa1b5f10-ed38-4553-a1c0-7adace4100db/purchase-gallery-apro-wireless-hero.png__1850x800_q100_crop-scale_optimize_subsampling-2.png',
          transacationType: 1,
        },
      ],
    };
  }
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  doSearchClick = () => {
    this.props.navigation.navigate('TransactionSearch');
  };
  doOpenTransactionDetail = item => {
    this.props.navigation.navigate('TransacationDetail', {item: item});
  };

  onRefresh() {
    this.setState({page: 1, refreshing: true}, () => {
      this.getTransacationList(
        getLangValue(strings.SELL, this.state.currentLang),
      );
    });
  }
  handleLoadMore() {
    if (this.state.page < this.state.last_page) {
      this.setState(
        (prevState, nextProps) => ({
          page: prevState.page + 1,
        }),
        () => {
          this.getTransacationList(
            getLangValue(strings.SELL, this.state.currentLang),
          );
        },
      );
    }
  }

  renderEmpty = () => {
    return (
      <View style={WalletStyle.viewEmptyMain}>
        <Image
          resizeMode="contain"
          source={icons.NO_NOTIFICATIONS}
          style={WalletStyle.imgNoReward}
        />
        <Text style={WalletStyle.textNoRewards}>
          {getLangValue(strings.NO_NOTIFICATIONS, this.state.currentLang)}
        </Text>
      </View>
    );
  };
  getTransacationList(title) {
    const params = {
      apiToken:
        this.state.currentUser !== undefined
          ? this.state.currentUser.token
          : '',
      userId:
        this.state.currentUser !== undefined ? this.state.currentUser.id : '0',
      page: this.state.page,
      status:
        title === getLangValue(strings.SELL, this.state.currentLang)
          ? 1
          : title === getLangValue(strings.BUY, this.state.currentLang)
          ? 0
          : 1,
    };
    this.props.doGetTransacationsViewMore(params);
  }
  renderTabOne() {
    return (
      <View>
        <View style={{marginTop: hp(2)}}>
          <FlatList
            key={4112130}
            data={this.state.transacationListData}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => this.doOpenTransactionDetail(item)}>
                <TrasacationListItem
                  image={item.code_id.image}
                  title={item.code_id.name}
                  date={moment(item.created_date).format(
                    'Do MMM, YYYY HH:mm A',
                  )}
                  price={item.amount}
                  type={''}
                  payment_types={item.payment_types}
                  currentLang={this.state.currentLang}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index.toString()}
            refreshing={this.state.refreshing}
            onRefresh={() => this.onRefresh()}
            onEndReached={() => this.handleLoadMore()}
            onEndReachedThreshold={15}
            ListEmptyComponent={this.state.viewEmpty ? this.renderEmpty : null}
            contentContainerStyle={
              this.state.viewEmpty && WalletStyle.listContent
            }
          />
        </View>
      </View>
    );
  }
  renderTabTwo() {
    return (
      <View>
        <View style={{marginTop: hp(2)}}>
          <FlatList
            key={4112130}
            data={this.state.transacationListData}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => this.doOpenTransactionDetail(item)}>
                <TrasacationListItem
                  image={item.code_id.image}
                  title={item.code_id.name}
                  date={moment(item.created_date).format(
                    'Do MMM, YYYY HH:mm A',
                  )}
                  price={item.amount}
                  type={''}
                  payment_types={item.payment_types}
                  currentLang={this.state.currentLang}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index.toString()}
            refreshing={this.state.refreshing}
            onRefresh={() => this.onRefresh()}
            onEndReached={() => this.handleLoadMore()}
            onEndReachedThreshold={15}
            ListEmptyComponent={this.state.viewEmpty ? this.renderEmpty : null}
            contentContainerStyle={
              this.state.viewEmpty && WalletStyle.listContent
            }
          />
        </View>
      </View>
    );
  }
  _handleConnectivityChange = state => {
    if (this._isMounted) {
      this.setState({
        isConnected: state.isConnected,
      });
    }
  };
  componentDidMount() {
    this._isMounted = true;
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    this.getTransacationList(
      getLangValue(strings.SELL, this.state.currentLang),
    );
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (
      this.props.responseTransactionsViewMore !==
      prevProps.responseTransactionsViewMore
    ) {
      if (this.props.responseTransactionsViewMore !== undefined) {
        const {code, status, message} = this.props.responseTransactionsViewMore;
        switch (code) {
          case 200:
            const {
              data,
              links,
            } = this.props.responseTransactionsViewMore.result;
            const {last, current} = links;
            if (current === 1) {
              this.setState({
                transacationListData: data,
                refreshing: false,
                last_page: last,
                viewEmpty: data.length <= 0,
              });
            } else {
              this.setState({
                transacationListData: [
                  ...this.state.transacationListData,
                  data,
                ],
                refreshing: false,
                last_page: last,
                viewEmpty: false,
              });
            }
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
    this._subscription();
  }
  render() {
    return (
      <View style={WalletStyle.container}>
        <View style={WalletStyle.viewHeader}>
          <Text style={WalletStyle.textHeader}>
            {getLangValue(
              strings.HEADER_TRANSACATION_HISTORY,
              this.state.currentLang,
            )}
          </Text>

          <View style={WalletStyle.viewBack}>
            <TouchableOpacity onPress={() => this.doBackClick()}>
              <Image source={icons.BACK} style={WalletStyle.imgBack} />
            </TouchableOpacity>
          </View>
          <View style={WalletStyle.viewSearch}>
            <TouchableOpacity onPress={() => this.doSearchClick()}>
              <Image source={icons.SEARCH} style={WalletStyle.imgSearch} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{flex: 1, marginTop: hp(2)}}>
          <TransactionTab
            tabOneTitle={getLangValue(strings.SELL, strings.LAG_ENG)}
            tabTwoTitle={getLangValue(strings.BUY, strings.LAG_ENG)}
            renderTabOne={() => this.renderTabOne()}
            renderTabTwo={() => this.renderTabTwo()}
            getTransacationList={title => this.getTransacationList(title)}
          />
        </View>

        {this.props.isBusyTransactionsViewMore ? <Loader /> : undefined}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    isBusyTransactionsViewMore: state.app.isBusyTransactionsViewMore,
    responseTransactionsViewMore: state.app.responseTransactionsViewMore,
    errorTransactionsViewMore: state.app.errorTransactionsViewMore,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({doGetTransacationsViewMore}, dispatch),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(WalletViewMoreScreen);
