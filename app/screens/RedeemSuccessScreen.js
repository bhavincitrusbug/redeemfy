import React, {PureComponent} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  BackHandler,
} from 'react-native';
import colors from '../resources/colors';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import fonts from '../resources/fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ButtonGradient from '../components/buttons/ButtonGradient';
import {getLangValue} from '../resources/languages/language';
import strings from '../resources/strings';
import RedeemSuccessStyle from '../resources/styles/RedeemSuccessStyle';
import moment from 'moment';
class RedeemSuccessScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      message: 'Successfully Redeemed!',
      description: '',
      coupon_id: '',
      expiry_date: '',
    };
  }
  doBackClick = () => {
    this.props.navigation.replace('Home');
  };
  handleBackPress = () => {
    this.doBackClick(); // works best when the goBack is async
    return true;
  };
  componentDidMount() {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackPress,
    );

    const {code_number, expiry_date} = this.props.navigation.state.params;

    if (code_number) {
      this.setState({
        coupon_id: code_number,
      });
    }
    if (expiry_date) {
      this.setState({
        expiry_date: moment(expiry_date, 'YYYY-MM-DD hh:mm:ss').format(
          'Do MMM YYYY',
        ),
      });
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }
  render() {
    return (
      <View style={RedeemSuccessStyle.container}>
        <View style={RedeemSuccessStyle.viewHeader}>
          <TouchableOpacity onPress={() => this.doBackClick()}>
            <Image source={icons.BACK} style={RedeemSuccessStyle.imgBack} />
          </TouchableOpacity>
        </View>
        <ScrollView>
          <View style={RedeemSuccessStyle.viewMain}>
            <Text style={RedeemSuccessStyle.textTitle}>
              {this.state.message}
            </Text>
            <Text style={RedeemSuccessStyle.textDescription}>
              {this.state.description}
            </Text>
            <Text style={RedeemSuccessStyle.textDescription}>
              Your coupon ID is : " {this.state.coupon_id} "
            </Text>
            <Text style={RedeemSuccessStyle.textDescription}>
              *Please note that the coupon validity is till (
              {this.state.expiry_date}).
            </Text>
          </View>
        </ScrollView>
        <View style={RedeemSuccessStyle.viewButtonClaim}>
          <ButtonGradient
            title={getLangValue(
              strings.BTN_BACK_TO_HOME,
              this.state.currentLang,
            )}
            backgroundColor={colors.orange}
            fontFamily={fonts.Dosis_Bold}
            fontSize={RFPercentage(3)}
            onPress={() => this.doBackClick()}
            width={wp(100)}
            height={hp(10)}
            currentLang={this.state.currentLang}
            borderRadius={0}
          />
        </View>
      </View>
    );
  }
}
export default RedeemSuccessScreen;
