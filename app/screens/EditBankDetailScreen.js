import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import colors from '../resources/colors';
import EditBankDetailStyle from '../resources/styles/EditBankDetailStyle';
import icons from '../resources/icons';
import {getLangValue, getErrorValue} from '../resources/languages/language';
import strings from '../resources/strings';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../resources/fonts';
import Reinput from 'reinput';
import errors from '../resources/errors';
import ButtonGradient from '../components/buttons/ButtonGradient';
import {
  doGetBankDetail,
  doPostUpdateBankDetail,
  doPostBankDetail,
} from '../redux/actions/AppActions';
import NetInfo from '@react-native-community/netinfo';
import {showErrorMessage, showSuccessMessage} from '../resources/validation';
import Loader from '../components/progress/Loader';
import {Dropdown} from 'react-native-material-dropdown';
import * as banks from '../resources/banks.json';

_isMounted = false;
class EditBankDetailScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentUser: props.currentUser,
      currentLang: strings.LAG_ENG,
      isCurrentAccount: true,
      isSavingAccount: false,
      transacation_types: [],
      accountType: 0,
      bankId: undefined,
      accountNumber: '',
      errAccountNumber: undefined,
      branchCode: '',
      errBranchCode: undefined,
      terms: `Make sure your name and bank information match your bank account. Otherwise, your transactions won’t process and you’ll be charged a fee. \n\nBy continuing, you agree to let Redeemfy send deposits to
your bank account.`,
    };
  }
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  doSubmit = () => {
    const {
      isConnected,
      currentUser,
      bankId,
      accountType,
      accountNumber,
      branchCode,
    } = this.state;
    if (accountNumber === '') {
      this.setState(
        {
          errAccountNumber: getErrorValue(
            errors.ENTER_ACCOUNT_NUMBER,
            this.state.currentLang,
          ),
        },
        () => this.accountNumberInput.focus(),
      );
      return;
    }
    this.setState({errAccountNumber: undefined});
    if (branchCode === '') {
      this.setState({
        errBranchCode: getErrorValue(
          errors.ENTER_BRANCH_CODE,
          this.state.currentLang,
        ),
      });
      return;
    }
    this.setState({errAccountNumber: undefined, errBranchCode: undefined});
    if (isConnected) {
      let params = undefined;
      if (bankId !== undefined) {
        params = {
          apiToken: currentUser !== undefined ? currentUser.token : '',
          bankId: bankId,
          user: currentUser !== undefined ? currentUser.id : '',
          account_number: accountNumber,
          branch_number: branchCode,
          account_type: accountType,
        };
        this.props.doPostUpdateBankDetail(params).catch(error => {
          showErrorMessage(error.message);
        });
      } else {
        params = {
          apiToken: currentUser !== undefined ? currentUser.token : '',
          user: currentUser !== undefined ? currentUser.id : '',
          account_number: accountNumber,
          branch_number: branchCode,
          account_type: accountType,
        };

        this.props.doPostBankDetail(params).catch(error => {
          showErrorMessage(error.message);
        });
      }
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  };
  doRadioEnable = (enable, type) => {
    if (type === 'current') {
      this.setState({
        isCurrentAccount: true,
        isSavingAccount: false,
        accountType: 0,
      });
    } else {
      this.setState({
        isCurrentAccount: false,
        isSavingAccount: true,
        accountType: 1,
      });
    }
  };
  getBankDetail() {
    const params = {
      apiToken:
        this.state.currentUser !== undefined
          ? this.state.currentUser.token
          : '',
    };
    this.props.doGetBankDetail(params).catch(error => {
      showErrorMessage(error.message);
    });
  }
  _handleConnectivityChange = state => {
    if (this._isMounted) {
      this.setState({
        isConnected: state.isConnected,
      });
    }
  };
  componentDidMount() {
    this._isMounted = true;
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    this.setState({transacation_types: banks.banks});
    this.getBankDetail();
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.responseBankDetail !== prevProps.responseBankDetail) {
      if (this.props.responseBankDetail !== undefined) {
        const {code, status, message} = this.props.responseBankDetail;
        switch (code) {
          case 200:
            if (status) {
              const {data} = this.props.responseBankDetail.result;
              this.setState({
                isCurrentAccount: data.account_type === '0',
                isSavingAccount: data.account_type === '1',
                accountType: data.account_type,
                accountNumber: data.account_number,
                branchCode: data.branch_number,
                bankId: data.id,
              });
            }
            break;
          case 400:
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
    if (this.props.responseAddBankDetail !== prevProps.responseAddBankDetail) {
      if (this.props.responseAddBankDetail !== undefined) {
        const {code, status, message} = this.props.responseAddBankDetail;
        switch (code) {
          case 200:
            if (status) {
              const {data} = this.props.responseAddBankDetail.result;
              this.setState({
                isCurrentAccount: data.account_type === '0',
                isSavingAccount: data.account_type === '1',
                accountType: data.account_type,
                accountNumber: data.account_number,
                branchCode: data.branch_number,
                bankId: data.id,
              });
              showSuccessMessage(message);
              this.doBackClick();
            }
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
    if (
      this.props.responseUpdateBankDetail !== prevProps.responseUpdateBankDetail
    ) {
      if (this.props.responseUpdateBankDetail !== undefined) {
        const {code, status, message} = this.props.responseUpdateBankDetail;
        switch (code) {
          case 200:
            showSuccessMessage(message);
            clearTimeout(this.clearTimeout);
            this.clearTimeout = setTimeout(() => {
              this.props.navigation.goBack(null);
            }, 500);
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
    this._subscription();
  }
  renderRadio(title, enable, type) {
    return (
      <TouchableOpacity onPress={() => this.doRadioEnable(enable, type)}>
        <View style={EditBankDetailStyle.viewRowCenter}>
          <Image
            source={enable ? icons.RADIO_ON : icons.RADIO_OFF}
            style={{width: wp(5), height: wp(5)}}
          />
          <Text style={EditBankDetailStyle.textTitle}>{title}</Text>
        </View>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <View style={EditBankDetailStyle.container}>
        <View style={EditBankDetailStyle.viewHeader}>
          <Text style={EditBankDetailStyle.textHeaderBankDetail}>
            {getLangValue(strings.HEADER_BANK_DETAIL, this.state.currentLang)}
          </Text>
          <View style={EditBankDetailStyle.viewBack}>
            <TouchableOpacity onPress={() => this.doBackClick()}>
              <Image
                source={icons.BACK}
                style={EditBankDetailStyle.imgBack}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={EditBankDetailStyle.viewMain}>
          <View style={EditBankDetailStyle.viewRowCenter}>
            {this.renderRadio(
              getLangValue(strings.CURRENT, this.state.currentLang),
              this.state.isCurrentAccount,
              'current',
            )}
            <View style={{width: wp(10)}} />
            {this.renderRadio(
              getLangValue(strings.SAVINGS, this.state.currentLang),
              this.state.isSavingAccount,
              'savings',
            )}
          </View>

          <Reinput
            ref={input => (this.accountNumberInput = input)}
            label={getLangValue(strings.ACCOUNT_NUMBER, this.state.currentLang)}
            labelColor={colors.colorLabelInActive}
            labelActiveColor={colors.colorLabelActive}
            color={colors.white}
            activeColor={colors.white}
            fontFamily={fonts.Dosis_Regular}
            value={this.state.accountNumber}
            onChangeText={text => {
              this.setState({accountNumber: text.replace(/\D/g, '')}, () => {
                this.state.accountNumber === ''
                  ? this.setState({
                      errAccountNumber: getErrorValue(
                        errors.ENTER_ACCOUNT_NUMBER,
                        this.state.currentLang,
                      ),
                    })
                  : this.setState({errAccountNumber: undefined});
              });
            }}
            error={this.state.errAccountNumber}
            fontSize={RFPercentage(3)}
            underlineColor={colors.colorLine}
            keyboardType="numeric"
            returnKeyType="done"
            onSubmitEditing={() => {
              this.state.accountNumber.length > 0
                ? this.setState({errAccountNumber: undefined})
                : null;
            }}
          />
          <View style={{marginBottom: hp(1)}}>
            <Dropdown
              label={getLangValue(strings.WHICH_BANK, this.state.currentLang)}
              data={this.state.transacation_types}
              inputContainerStyle={{
                color: colors.colorLabelInActive,
              }}
              labelTextStyle={{fontFamily: fonts.Dosis_Regular}}
              fontSize={RFPercentage(2.5)}
              labelFontSize={RFPercentage(2)}
              itemTextStyle={EditBankDetailStyle.textDropDown}
              baseColor={colors.colorLabelInActive}
              textColor={colors.white}
              selectedItemColor={colors.colorBackground}
              itemColor={colors.colorBackground}
              value={this.state.branchCode}
              onChangeText={(value, index, data) => {
                this.setState({branchCode: value});
              }}
              error={this.state.errPreference}
              dropdownOffset={{top: hp(3), left: 0}}
              dropdownMargins={{min: wp(5), max: 16}}
              itemCount={6}
              dropdownPosition={0}
            />
          </View>
          {/* <Reinput
            ref={input => (this.branchCodeInput = input)}
            label={getLangValue(strings.BRANCH_NUMBER, this.state.currentLang)}
            labelColor={colors.colorLabelInActive}
            labelActiveColor={colors.colorLabelActive}
            color={colors.white}
            activeColor={colors.white}
            fontFamily={fonts.Dosis_Regular}
            value={this.state.branchCode}
            onChangeText={text => {
              this.setState({branchCode: text}, () => {
                this.state.branchCode === ''
                  ? this.setState({
                      errBranchCode: getErrorValue(
                        errors.ENTER_BRANCH_CODE,
                        this.state.currentLang,
                      ),
                    })
                  : this.setState({errBranchCode: undefined});
              });
            }}
            error={this.state.errBranchCode}
            fontSize={RFPercentage(3)}
            underlineColor={colors.colorLine}
            keyboardType="default"
            returnKeyType="done"
          /> */}
          <Text style={EditBankDetailStyle.textTerms}>{this.state.terms}</Text>
          <View style={EditBankDetailStyle.viewBtnSubmit}>
            <ButtonGradient
              title={getLangValue(strings.BTN_SUBMIT, this.state.currentLang)}
              backgroundColor={colors.orange}
              fontFamily={fonts.Dosis_Bold}
              fontSize={RFPercentage(3)}
              onPress={() => this.doSubmit()}
              width={wp(90)}
              height={hp(6)}
              currentLang={this.state.currentLang}
              borderRadius={23}
            />
          </View>
        </View>
        {this.props.isBusyBankDetail ||
        this.props.isBusyUpdateBankDetail ||
        this.props.isBusyAddBankDetail ? (
          <Loader />
        ) : (
          undefined
        )}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    isBusyBankDetail: state.app.isBusyBankDetail,
    responseBankDetail: state.app.responseBankDetail,
    errorBankDetail: state.app.errorBankDetail,
    isBusyAddBankDetail: state.app.isBusyAddBankDetail,
    responseAddBankDetail: state.app.responseAddBankDetail,
    errorAddBankDetail: state.app.errorAddBankDetail,
    isBusyUpdateBankDetail: state.app.isBusyUpdateBankDetail,
    responseUpdateBankDetail: state.app.responseUpdateBankDetail,
    errorUpdateBankDetail: state.app.errorUpdateBankDetail,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {doGetBankDetail, doPostUpdateBankDetail, doPostBankDetail},
      dispatch,
    ),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditBankDetailScreen);
