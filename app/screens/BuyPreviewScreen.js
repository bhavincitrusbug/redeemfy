import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {Text, View, TouchableOpacity, Image, ScrollView} from 'react-native';
import colors from '../resources/colors';
import fonts from '../resources/fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import icons from '../resources/icons';
import {getErrorValue, getLangValue} from '../resources/languages/language';
import errors from '../resources/errors';
import strings from '../resources/strings';
import Reinput from 'reinput';
import ButtonGradient from '../components/buttons/ButtonGradient';
import {doUpdateMarketList, doPostBuyCoupon} from '../redux/actions/AppActions';
import NetInfo from '@react-native-community/netinfo';
import stripe from 'tipsi-stripe';
import constants from '../resources/constants';
import {showErrorMessage, generateKey} from '../resources/validation';
import Loader from '../components/progress/Loader';
import BuyPreviewStyle from '../resources/styles/BuyPreviewStyle';

stripe.setOptions({
  publishableKey: constants.STRIPE_KEY,
});
class BuyPreviewScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentUser: props.currentUser,
      currentLang: props.currentLang,
      isSelectWallet: false,
      coupon_amount: '0',
      discount: '0',
      wallet_amount: '0',
      discount_amount: '0',
      payable_amount: '0',
      user_amount: '0',
      errUserAmount: undefined,
      buyParams: undefined,
      walletData: undefined,
    };
  }
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  doCalPayableAmount() {
    this.setState({
      discount_amount: this.doGetDiscountAmount(),
      payable_amount: this.doGetPayableAmount(),
      user_amount: this.doGetUserAMount(),
    });
  }
  doGetDiscountAmount() {
    return this.state.coupon_amount * (this.state.discount / 100);
  }
  doGetUserAMount() {
    let value = 0;
    if (this.state.isSelectWallet) {
      value = this.doGetPayableAmount() - this.state.wallet_amount;
    } else {
      value = this.doGetPayableAmount();
    }
    if (value < 0) {
      return 0;
    } else {
      return value;
    }
  }
  doGetPayableAmount() {
    if (this.state.isSelectWallet) {
      let payable_amount_with_wallet =
        this.state.coupon_amount - this.doGetDiscountAmount();
      if (payable_amount_with_wallet < 0) {
        this.setState({
          user_amount: 0,
        });
      }
      return payable_amount_with_wallet > 0 ? payable_amount_with_wallet : 0;
    } else {
      let payable_amount_without_wallet =
        this.state.coupon_amount - this.doGetDiscountAmount();
      return payable_amount_without_wallet;
    }
  }
  toggleNotifications() {
    this.setState({isSelectWallet: !this.state.isSelectWallet}, () => {
      this.doCalPayableAmount();
    });
  }
  handleCardPayPress = async () => {
    try {
      this.setState({loading: true, token: null});
      const options = {
        theme: {
          primaryBackgroundColor: colors.white,
          secondaryBackgroundColor: colors.white,
          primaryForegroundColor: colors.colorBackground,
          secondaryForegroundColor: colors.colorBackground,
          accentColor: colors.colorTabBackground,
        },
      };
      const token = await stripe.paymentRequestWithCardForm(options);
      if (this.state.isConnected) {
        this.doApiCall(token);
      } else {
        showErrorMessage(
          getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
        );
      }
      this.setState({loading: false, token});
    } catch (error) {
      this.setState({loading: false});
    }
  };
  doApiCall(token) {
    if (token === '') {
      let params = Object.assign({}, this.state.buyParams);
      params.customer_token = '';
      params.wallet_amount = 'true';
      params.amount = this.state.user_amount;
      this.props.doPostBuyCoupon(params).catch(error => {
        showErrorMessage(error.message);
      });
    } else {
      let params = Object.assign({}, this.state.buyParams);
      params.customer_token = token.tokenId;
      params.wallet_amount = 'false';
      params.amount = this.state.user_amount;
      this.props.doPostBuyCoupon(params).catch(error => {
        showErrorMessage(error.message);
      });
    }
  }
  doPayNow = () => {
    if (this.state.isConnected) {
      if (this.state.user_amount === 0 || this.state.user_amount === '0') {
        this.doApiCall('');
      } else {
        this.handleCardPayPress();
      }
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  };
  renderSwitch() {
    return (
      <TouchableOpacity onPress={() => this.toggleNotifications()}>
        <Image
          source={this.state.isSelectWallet ? icons.ON : icons.OFF}
          style={{width: wp(10), height: hp(4)}}
          resizeMode="contain"
        />
      </TouchableOpacity>
    );
  }
  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected,
    });
  };
  componentDidMount() {
    const {
      buyParams,
      couponData,
      walletData,
    } = this.props.navigation.state.params;

    this.setState(
      {
        walletData: walletData,
        buyParams: buyParams,
        coupon_amount: couponData.price,
        discount: couponData.discount,
        wallet_amount: walletData.amount,
      },
      () => {
        this.doCalPayableAmount();
      },
    );
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
  }
  componentDidUpdate(prevProps) {
    if (this.props.responseRewardBuy !== prevProps.responseRewardBuy) {
      if (this.props.responseRewardBuy !== undefined) {
        const {code, status, message} = this.props.responseRewardBuy;
        switch (code) {
          case 200:
            if (status) {
              this.props.doUpdateMarketList({
                enable: true,
                key: generateKey(5),
              });
              this.props.navigation.navigate('BuySuccess');
            }

            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  render() {
    return (
      <View style={BuyPreviewStyle.container}>
        <View style={BuyPreviewStyle.viewHeader}>
          <Text style={BuyPreviewStyle.textMainHeader}>
            {getLangValue(
              strings.HEADER_PREVIEW_DETAIL,
              this.state.currentLang,
            )}
          </Text>
          <View style={BuyPreviewStyle.viewBack}>
            <TouchableOpacity onPress={() => this.doBackClick()}>
              <Image
                source={icons.BACK}
                style={{width: wp(6), height: wp(6)}}
              />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView>
          <View style={BuyPreviewStyle.viewMain}>
            <Text style={BuyPreviewStyle.textHeader}>
              {getLangValue(strings.WALLET_DETAIL, this.state.currentLang)}
            </Text>
            <View style={BuyPreviewStyle.viewWalletDetail}>
              <View style={BuyPreviewStyle.rowCenter}>
                <Text style={BuyPreviewStyle.textItemFlex}>
                  {getLangValue(strings.BALALNCE, this.state.currentLang)}
                </Text>
                <Text style={BuyPreviewStyle.textItem}>
                  ${this.state.wallet_amount}
                </Text>
              </View>
            </View>
            <Text style={BuyPreviewStyle.textHeader}>
              {getLangValue(strings.PAYMENT_DETAIL, this.state.currentLang)}
            </Text>
            <View style={BuyPreviewStyle.viewPaymentDetail}>
              <View style={BuyPreviewStyle.viewRowItem}>
                <Text style={BuyPreviewStyle.textItemFlex}>
                  {getLangValue(strings.COUPON_AMOUNT, this.state.currentLang)}
                </Text>
                <Text style={BuyPreviewStyle.textItem}>
                  ${this.state.coupon_amount}
                </Text>
              </View>

              <View style={BuyPreviewStyle.viewRowDiscount}>
                <Text style={BuyPreviewStyle.textItemFlex}>
                  {getLangValue(strings.DISCOUNT, this.state.currentLang)} (
                  {this.state.discount}%)
                </Text>
                <Text style={BuyPreviewStyle.textItem}>
                  ${this.state.discount_amount}
                </Text>
              </View>
              <View style={BuyPreviewStyle.viewRowPayable}>
                <Text style={BuyPreviewStyle.textItemFlex}>
                  {getLangValue(strings.PAYABLE_AMOUNT, this.state.currentLang)}
                </Text>
                <Text style={BuyPreviewStyle.textItem}>
                  ${this.state.payable_amount}
                </Text>
              </View>
            </View>
            <View style={BuyPreviewStyle.viewWalletAmount}>
              <View style={BuyPreviewStyle.viewRowWalletAmount}>
                <Text style={BuyPreviewStyle.textItemFlex}>
                  {getLangValue(strings.PAY_BY_WALLET, this.state.currentLang)}
                </Text>
                {this.renderSwitch()}
              </View>
              <Reinput
                ref={input => (this.fullNameInput = input)}
                label={getLangValue(
                  strings.TOTAL_AMOUNT_PAYABLE,
                  this.state.currentLang,
                )}
                labelColor={colors.colorBackground}
                labelActiveColor={colors.colorBackground}
                color={colors.colorBackground}
                activeColor={colors.colorBackground}
                fontFamily={fonts.Dosis_Regular}
                value={'$' + this.state.user_amount}
                editable={false}
                onChangeText={text => {
                  this.setState(
                    {user_amount: text.replace(/[^0-9]/g, '')},
                    () => {
                      this.state.user_amount === ''
                        ? this.setState({
                            errUserAmount: getErrorValue(
                              errors.ENTER_AMOUNT,
                              this.state.currentLang,
                            ),
                          })
                        : this.setState({errUserAmount: undefined});
                    },
                  );
                }}
                error={this.state.errUserAmount}
                fontSize={RFPercentage(3)}
                underlineColor={colors.colorLine1}
                returnKeyType="done"
              />
            </View>
          </View>
        </ScrollView>
        <View style={BuyPreviewStyle.viewButton}>
          <ButtonGradient
            title={getLangValue(strings.BTN_PAY_NOW, this.state.currentLang)}
            backgroundColor={colors.orange}
            fontFamily={fonts.Dosis_Bold}
            fontSize={RFPercentage(3)}
            onPress={() => this.doPayNow()}
            width={wp(100)}
            height={hp(10)}
            currentLang={this.state.currentLang}
            borderRadius={0}
          />
        </View>
        {this.props.isBusyRewardBuy ? <Loader /> : undefined}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    currentLang: state.app.currentLang,
    isBusyRewardBuy: state.app.isBusyRewardBuy,
    responseRewardBuy: state.app.responseRewardBuy,
    errorRewardBuy: state.app.errorRewardBuy,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {
        doUpdateMarketList,
        doPostBuyCoupon,
      },
      dispatch,
    ),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(BuyPreviewScreen);
