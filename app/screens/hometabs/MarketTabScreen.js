import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  Dimensions,
  RefreshControl,
  Linking,
} from 'react-native';
import {getLangValue} from '../../resources/languages/language';
import strings from '../../resources/strings';
import MarketTabStyle from '../../resources/styles/MarketTabStyle';
import icons from '../../resources/icons';
import colors from '../../resources/colors';
import MarketListItem from '../../components/list/MarketListItem';
import constants from '../../resources/constants';
import AsyncStorage from '@react-native-community/async-storage';
import {
  doGetMarketList,
  doUpdateMarketList,
  doSetUser,
} from '../../redux/actions/AppActions';
import Loader from '../../components/progress/Loader';
import {showErrorMessage} from '../../resources/validation';
import LoginAlert from '../../components/alert/LoginAlert';
import FlatGrid from '../../components/grids/FlatGrid';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {BallIndicator} from 'react-native-indicators';

class MarketTabScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: props.currentUser,
      currentLang: strings.LAG_ENG,
      isLoginAlert: false,
      marketItems: [],
      advertisement_image: '',
      advertisement_link: '',
      rewards: [
        {
          rewardId: 1,
          title: 'NOW ONLY $40',
          description: '$20 Cash Voucher',
          image:
            'https://www.jabra.in/-/media/Images/Products/Jabra-Elite-85h/Product/elite_85h_navy_01.png?w=555&la=en-IN&hash=1E80CF674AF05BA1BFCDC7B0C9B25A37966FDCF7',
          total: 120,
          used: 20,
          discount: 5,
        },
        {
          rewardId: 1,
          title: 'NOW ONLY $40',
          description: '$20 Cash Voucher',
          image:
            'https://www.jabra.in/-/media/Images/Products/Jabra-Elite-85h/Product/elite_85h_navy_01.png?w=555&la=en-IN&hash=1E80CF674AF05BA1BFCDC7B0C9B25A37966FDCF7',
          total: 120,
          used: 30,
          discount: 8,
        },
        {
          rewardId: 1,
          title: 'NOW ONLY $40',
          description: '$20 Cash Voucher',
          image:
            'https://www.jabra.in/-/media/Images/Products/Jabra-Elite-85h/Product/elite_85h_navy_01.png?w=555&la=en-IN&hash=1E80CF674AF05BA1BFCDC7B0C9B25A37966FDCF7',
          total: 120,
          used: 50,
          discount: 2,
        },
        {
          rewardId: 1,
          title: 'NOW ONLY $40',
          description: '$20 Cash Voucher',
          image:
            'https://www.jabra.in/-/media/Images/Products/Jabra-Elite-85h/Product/elite_85h_navy_01.png?w=555&la=en-IN&hash=1E80CF674AF05BA1BFCDC7B0C9B25A37966FDCF7',
          total: 120,
          used: 80,
          discount: 6,
        },
        {
          rewardId: 1,
          title: 'NOW ONLY $40',
          description: '$20 Cash Voucher',
          image:
            'https://www.jabra.in/-/media/Images/Products/Jabra-Elite-85h/Product/elite_85h_navy_01.png?w=555&la=en-IN&hash=1E80CF674AF05BA1BFCDC7B0C9B25A37966FDCF7',
          total: 120,
          used: 90,
          discount: 10,
        },
        {
          rewardId: 1,
          title: 'NOW ONLY $40',
          description: '$20 Cash Voucher',
          image:
            'https://www.jabra.in/-/media/Images/Products/Jabra-Elite-85h/Product/elite_85h_navy_01.png?w=555&la=en-IN&hash=1E80CF674AF05BA1BFCDC7B0C9B25A37966FDCF7',
          total: 120,
          used: 100,
          discount: 15,
        },
        {
          rewardId: 1,
          title: 'NOW ONLY $40',
          description: '$20 Cash Voucher',
          image:
            'https://www.jabra.in/-/media/Images/Products/Jabra-Elite-85h/Product/elite_85h_navy_01.png?w=555&la=en-IN&hash=1E80CF674AF05BA1BFCDC7B0C9B25A37966FDCF7',
          total: 120,
          used: 80,
          discount: 20,
        },
      ],
      searchValue: '',
      refreshing: false,
      viewEmpty: false,
      page: 1,
      last_page: 1,
    };
    this.delay = 1000;
    this.last_text_edit = 0;
  }
  doOpenBag() {
    AsyncStorage.getItem(constants.IS_LOGIN).then(value => {
      if (value && value === 'true') {
        this.setState({isLoginAlert: false});
        this.props.navigation.navigate('BagTab');
      } else {
        this.setState({isLoginAlert: true});
      }
    });
  }
  doOpenMarketDetail = item => {
    this.props.navigation.navigate('MarketDetail', {item: item});
  };
  doSearch() {
    this.last_text_edit = Math.round(new Date().getTime());
    clearTimeout(this.searchWaiting);
    this.searchWaiting = setTimeout(() => {
      if (
        Math.round(new Date().getTime()) >
        this.last_text_edit + this.delay - 500
      ) {
        this.getMarketList();
      }
    }, this.delay);
  }
  doOpenAdLink = () => {
    if (this.state.advertisement_link === '') {
      return;
    }
    Linking.openURL(this.state.advertisement_link);
  };
  getMarketList() {
    const params = {
      apiToken:
        this.state.currentUser !== undefined
          ? this.state.currentUser.token
          : '',
      search: this.state.searchValue,
      page: this.state.page,
    };
    this.props.doGetMarketList(params);
  }

  renderHeader = () => {
    console.log('image ==><', this.state.advertisement_image)
    return (
      <View style={{marginStart: wp(3), marginEnd: wp(3)}}>
        <View style={MarketTabStyle.viewSearchBackground}>
          <Image
            source={icons.SEARCH}
            style={MarketTabStyle.imgSearch}
            resizeMode="contain"
          />
          <View style={{flex: 1, justifyContent: 'center'}}>
            <TextInput
              placeholder="Search"
              style={MarketTabStyle.inputSearch}
              placeholderTextColor={colors.white}
              underlineColorAndroid={colors.colorTransparent}
              onChangeText={text => {
                this.setState({searchValue: text}, () => {
                  this.doSearch();
                });
              }}
              value={this.state.searchValue}
              returnKeyType="done"
              clearButtonMode="while-editing"
            />
            <View style={{position: 'absolute', end: 0, marginEnd: 10}}>
              {this.state.searchValue !== '' && this.props.isBusyMarketList ? (
                <BallIndicator size={20} color={colors.white} count={10} />
              ) : null}
            </View>
          </View>
        </View>
        {this.state.advertisement_image === '' || this.state.advertisement_image === null ? null : (
          <View style={MarketTabStyle.viewBanner}>
              <TouchableOpacity onPress={() => this.doOpenAdLink()}>
                <Image
                  source={{uri: this.state.advertisement_image !== '' && this.state.advertisement_image}}
                  style={{width: wp(100), height: hp(16)}}
                />
              </TouchableOpacity>
          </View>
        )}
      </View>
    );
  };
  onRefresh() {
    this.setState({page: 1, refreshing: true}, () => {
      this.getMarketList();
    });
  }
  handleLoadMore() {
    if (this.state.page < this.state.last_page) {
      this.setState(
        (prevState, nextProps) => ({
          page: prevState.page + 1,
        }),
        () => {
          this.getMarketList();
        },
      );
    }
  }
  componentDidMount() {
    this.getMarketList();
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.currentUser !== prevProps.currentUser) {
      this.setState({currentUser: this.props.currentUser}, () => {
        this.getMarketList();
      });
    }
    if (this.props.marketListUpdate !== prevProps.marketListUpdate) {
      const {enable} = this.props.marketListUpdate;
      if (enable) {
        this.getMarketList();
        this.props.doUpdateMarketList({enable: false, key: ''});
      }
    }
    if (this.props.responseMarketList !== prevProps.responseMarketList) {
      if (this.props.responseMarketList !== undefined) {
        const {detail} = this.props.responseMarketList;
        if (detail === undefined) {
          const {code, status, message} = this.props.responseMarketList;
          switch (code) {
            case 200:
              const {result} = this.props.responseMarketList;
              const {data, links, advertisement} = result;
              const {last, current} = links;

              if (current === 1) {
                this.setState({
                  advertisement_image:
                    advertisement === undefined ? '' : advertisement.image,
                  advertisement_link:
                    advertisement === undefined ? '' : advertisement.link,
                  marketItems: data,
                  refreshing: false,
                  last_page: last,
                  viewEmpty: data.length <= 0,
                });
              } else {
                this.setState({
                  advertisement_image:
                    advertisement === undefined ? '' : advertisement.image,
                  advertisement_link:
                    advertisement === undefined ? '' : advertisement.link,
                  marketItems: [...this.state.marketItems, ...data],
                  refreshing: false,
                  last_page: last,
                  viewEmpty: false,
                });
              }
              break;
            case 400:
              this.setState({
                marketItems: [],
                refreshing: false,
                last_page: 0,
                viewEmpty: true,
              });
              break;
            default:
              showErrorMessage(message);
              break;
          }
        } else {
          showErrorMessage(detail);
          AsyncStorage.removeItem(constants.USER);
          AsyncStorage.removeItem(constants.IS_LOGIN);
          this.props.doSetUser(undefined);
          this.props.navigation.replace('LoginRegister');
        }
      }
    }
  }
  renderEmpty = () => {
    return (
      <View style={MarketTabStyle.viewEmptyMain}>
        <Image
          resizeMode="contain"
          source={icons.NO_REWARDS}
          style={MarketTabStyle.imgNoReward}
        />
        <Text style={MarketTabStyle.textNoRewards}>
          {getLangValue(strings.NO_MARKETPLACE, this.state.currentLang)}
        </Text>
      </View>
    );
  };
  render() {
    return (
      <View style={MarketTabStyle.container}>
        <View style={MarketTabStyle.viewHeader}>
          <Text style={MarketTabStyle.textHeaderHome}>
            {getLangValue(strings.HEADER_MARKET, this.state.currentLang)}
          </Text>
          <View style={MarketTabStyle.viewBag}>
            <TouchableOpacity onPress={() => this.doOpenBag()}>
              <Image
                source={icons.BAG}
                style={MarketTabStyle.imgBag}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={MarketTabStyle.viewBorderLine} />

        <View>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={() => this.onRefresh()}
              />
            }>
            <View style={MarketTabStyle.viewList}>
              {this.renderHeader()}
              <FlatGrid
                key={4112128}
                itemDimension={Dimensions.get('screen').width / 3}
                items={this.state.marketItems}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    onPress={() => this.doOpenMarketDetail(item)}>
                    <MarketListItem
                      item={item}
                      image_url={item.image}
                      title={item.name}
                      description={`$${item.price}`}
                      total={item.quantity}
                      used={item.quantity - item.quantity_left}
                      left={item.quantity_left + ' Left'}
                      discount={item.discount}
                      is_redeem={item.is_redeem}
                    />
                  </TouchableOpacity>
                )}
                keyExtractor={(item, index) => index.toString()}
                refreshing={this.state.refreshing}
                onRefresh={() => this.onRefresh()}
                onEndReached={() => this.handleLoadMore()}
                onEndReachedThreshold={15}
                ListEmptyComponent={
                  this.state.viewEmpty ? this.renderEmpty : null
                }
              />
            </View>
          </ScrollView>
        </View>
        {this.props.isBusyMarketList && this.state.searchValue === '' ? (
          <Loader />
        ) : (
          undefined
        )}
        {this.state.isLoginAlert ? (
          <LoginAlert
            isLoginAlert={this.state.isLoginAlert}
            navigation={this.props.navigation}
            doNoClick={value => {
              this.setState({isLoginAlert: value});
            }}
          />
        ) : (
          undefined
        )}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    marketListUpdate: state.app.marketListUpdate,
    isBusyMarketList: state.app.isBusyMarketList,
    responseMarketList: state.app.responseMarketList,
    errorMarketList: state.app.errorMarketList,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {doGetMarketList, doUpdateMarketList, doSetUser},
      dispatch,
    ),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(MarketTabScreen);
