import {createBottomTabNavigator} from 'react-navigation-tabs';
import UserTabScreen from './UserTabScreen';
import HomeTab from '../../components/tabs/HomeTab';
import HomeTabScreen from './HomeTabScreen';
import MarketTabScreen from './MarketTabScreen';

const HomeIndex = createBottomTabNavigator(
  {
    HomeTab: {
      screen: HomeTabScreen,
    },
    MarketTab: {
      screen: MarketTabScreen,
    },
    UserTab: {
      screen: UserTabScreen,
    },
  },
  {
    lazy: true,
    initialRouteName: 'HomeTab',
    tabBarOptions: {
      showLabel: false,
    },
    tabBarComponent: HomeTab,
  },
);
export default HomeIndex;
