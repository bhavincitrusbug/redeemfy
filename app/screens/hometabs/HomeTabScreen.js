import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {View, Text, TouchableOpacity, Image, FlatList} from 'react-native';
import {getLangValue} from '../../resources/languages/language';
import strings from '../../resources/strings';
import HomeTabStyle from '../../resources/styles/HomeTabStyle';
import icons from '../../resources/icons';
import OfferTab from '../../components/tabs/OfferTab';
import colors from '../../resources/colors';
import CountDown from '../../components/countdown/CountDown';
import {RFPercentage} from 'react-native-responsive-fontsize';
import moment from 'moment';
import RewardListItem from '../../components/list/RewardListItem';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../../resources/constants';
import Loader from '../../components/progress/Loader';
import {
  doGetOnGoingRewards,
  doGetComingRewards,
  doUpdateCouponList,
  doSetUser,
  doSetCurrentTimeTimer,
  doSetLastTimeTimer,
  doSetServerTime,
  doGetServerTime,
} from '../../redux/actions/AppActions';
import {showErrorMessage} from '../../resources/validation';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import LoginAlert from '../../components/alert/LoginAlert';
import NetInfo from '@react-native-community/netinfo';

class HomeTabScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentLang: strings.LAG_ENG,
      currentUser: props.currentUser,
      isConnected: true,
      isLoginAlert: false,
      viewEmptyOnGoing: false,
      viewEmptyComing: false,
      refreshing_ongoing: false,
      refreshing_coming: false,
      isActiveOnGoing: true,
      isActiveComing: false,
      reward_type: 0,
      ongoing_page: 1,
      ongoing_last_page: 0,
      coming_page: 1,
      coming_last_page: 0,
      rewardsOngoing: [],
      rewardsComing: [],
      expired: '',
      currentTimeTimer: props.currentTimeTimer,
      lastTimeTimer: props.lastTimeTimer,
      serverTime: '',
    };
  }
  doOpenBag() {
    AsyncStorage.getItem(constants.IS_LOGIN).then(value => {
      if (value && value === 'true') {
        this.setState({isLoginAlert: false});
        this.props.navigation.navigate('BagTab');
      } else {
        this.setState({isLoginAlert: true});
      }
    });
  }
  doOpenRewardDetail(item, type) {
    this.props.navigation.navigate('RewardDetail', {item: item, type: type});
  }
  renderEmpty = () => {
    return (
      <View style={HomeTabStyle.viewEmptyMain}>
        <Image
          resizeMode="contain"
          source={icons.NO_REWARDS}
          style={HomeTabStyle.imgNoReward}
        />
        <Text style={HomeTabStyle.textNoRewards}>
          {getLangValue(this.state.viewEmptyOnGoing? strings.ONGOING_NO_COUPON : this.state.viewEmptyComing? strings.COMMING_NO_COUPON : '', this.state.currentLang)}
        </Text>
      </View>
    );
  };
  renderTabOne() {
    // Do your operations

    return (
      <View
        style={{
          marginTop: hp(2),
          marginBottom: hp(5),
        }}>
        <View style={HomeTabStyle.viewTabMain}>
          <FlatList
            key={4112128}
            data={this.state.rewardsOngoing}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => this.doOpenRewardDetail(item, 0)}>
                <RewardListItem
                  image_url={item.image}
                  title={item.name}
                  description={`$${item.price}`}
                  total={item.quantity}
                  used={item.quantity - item.quantity_left}
                  leftItem={item.quantity_left + ' Left'}
                  is_redeem={item.is_redeem}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index.toString()}
            onRefresh={() =>
              this.onRefresh(
                getLangValue(strings.ONGOING, this.state.currentLang),
              )
            }
            refreshing={this.state.refreshing_ongoing}
            onEndReached={() =>
              this.handleLoadMore(
                getLangValue(strings.ONGOING, this.state.currentLang),
              )
            }
            onEndReachedThreshold={15}
            contentContainerStyle={
              this.state.viewEmptyOnGoing && HomeTabStyle.listContent
            }
            ListEmptyComponent={
              this.state.viewEmptyOnGoing ? this.renderEmpty : null
            }
          />
        </View>
      </View>
    );
  }
  onCountDownChange = e => {
    if (this.state.isConnected) {
      const params = {
        apiToken:
          this.state.currentUser !== undefined
            ? this.state.currentUser.token
            : '',
        status: this.state.reward_type,
        page: this.state.coming_page,
      };

      this.props.doGetServerTime(params);
      this.props.doSetLastTimeTimer(e);
    }
  };
  renderHeader = expired => {
    if (this.state.rewardsComing.length <= 0) {
      return null;
    }
    return (
      <View style={HomeTabStyle.viewCountDown}>
        <CountDown
          until={this.state.lastTimeTimer}
          currentDate={
            this.state.serverTime === ''
              ? 0
              : new Date(this.state.serverTime).getTime()
          }
          onFinish={() =>
            this.getRewards(
              getLangValue(strings.COMMING_SOON, this.state.currentLang),
            )
          }
          onPress={() => console.log('finished')}
          onChange={e => this.onCountDownChange(e)}
          size={RFPercentage(3)}
          digitStyle={{backgroundColor: colors.colorCountDownBackground}}
          digitTxtStyle={{color: colors.white}}
          showSeparator={true}
        />
      </View>
    );
  };
  renderTabTwo() {
    // Do your operations

    return (
      <View>
        <View style={HomeTabStyle.viewTabMain}>
          <FlatList
            key={4112129}
            ListHeaderComponent={() => this.renderHeader(this.state.expired)}
            data={this.state.rewardsComing}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => this.doOpenRewardDetail(item, 1)}>
                <RewardListItem
                  image_url={item.image}
                  title={item.name}
                  description={`$${item.price}`}
                  total={item.quantity}
                  used={item.quantity - item.quantity_left}
                  leftItem={item.quantity_left + ' Left'}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index.toString()}
            onRefresh={() =>
              this.onRefresh(
                getLangValue(strings.COMMING_SOON, this.state.currentLang),
              )
            }
            refreshing={this.state.refreshing_coming}
            onEndReached={() =>
              this.handleLoadMore(
                getLangValue(strings.COMMING_SOON, this.state.currentLang),
              )
            }
            onEndReachedThreshold={15}
            contentContainerStyle={
              this.state.viewEmptyComing && HomeTabStyle.listContent
            }
            ListEmptyComponent={
              this.state.viewEmptyComing ? this.renderEmpty : null
            }
          />
        </View>
      </View>
    );
  }
  getRewards(title) {
    if (title === getLangValue(strings.ONGOING, this.state.currentLang)) {
      this.setState(
        {reward_type: 0, isActiveOnGoing: true, isActiveComing: false},
        () => {
          const params = {
            apiToken:
              this.state.currentUser !== undefined
                ? this.state.currentUser.token
                : '',
            status: this.state.reward_type,
            page: this.state.ongoing_page,
          };
          this.props.doGetOnGoingRewards(params);
        },
      );
    } else {
      this.setState(
        {reward_type: 1, isActiveOnGoing: false, isActiveComing: true},
        () => {
          const params = {
            apiToken:
              this.state.currentUser !== undefined
                ? this.state.currentUser.token
                : '',
            status: this.state.reward_type,
            page: this.state.coming_page,
          };
          this.props.doGetComingRewards(params);
        },
      );
    }
  }
  onRefresh(title) {
    if (title === getLangValue(strings.ONGOING, this.state.currentLang)) {
      this.setState({ongoing_page: 1, refreshing_ongoing: true}, () => {
        this.getRewards(title);
      });
    } else {
      this.setState({coming_page: 1, refreshing_coming: true}, () => {
        this.getRewards(title);
      });
    }
  }
  handleLoadMore(title) {
    if (title === getLangValue(strings.ONGOING, this.state.currentLang)) {
      if (this.state.ongoing_last_page !== 0) {
        if (this.state.ongoing_last_page !== this.state.ongoing_page) {
          this.setState(
            (prevState, nextProps) => ({
              ongoing_page: prevState.ongoing_page + 1,
            }),
            () => {
              this.getRewards(title);
            },
          );
        }
      }
    } else {
      if (this.state.coming_last_page !== 0) {
        if (this.state.coming_last_page !== this.state.coming_page) {
          this.setState(
            (prevState, nextProps) => ({
              coming_page: prevState.coming_page + 1,
            }),
            () => {
              this.getRewards(title);
            },
          );
        }
      }
    }
  }
  _handleConnectivityChange = state => {
    if (this._isMounted) {
      this.setState({
        isConnected: state.isConnected,
      });
    }
  };
  componentDidMount() {
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    this.getRewards(getLangValue(strings.ONGOING, this.state.currentLang));
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.responseServerTime != prevProps.responseServerTime) {
      this.props.doSetServerTime(
        new Date(this.props.responseServerTime).getTime(),
      );
      var serverDateTime = moment.utc(
        moment(this.props.responseServerTime, 'YYYY-MM-DD HH:mm:ss'),
      );
      var date = moment.utc(moment(this.state.expired, 'YYYY-MM-DD HH:mm:ss'));
      var seconds = date.diff(serverDateTime, 'seconds');
      this.props.doSetLastTimeTimer(Math.max(0, seconds));
    }
    if (this.props.lastTimeTimer !== prevProps.lastTimeTimer) {
      this.setState({lastTimeTimer: this.props.lastTimeTimer});
    }
    if (this.props.currentUser !== prevProps.currentUser) {
      this.setState({currentUser: this.props.currentUser}, () => {
        if (this.state.isActiveOnGoing) {
          this.getRewards(
            getLangValue(strings.ONGOING, this.state.currentLang),
          );
        } else {
          this.getRewards(
            getLangValue(strings.COMMING_SOON, this.state.currentLang),
          );
        }
      });
    }
    if (this.props.couponListUpdate !== prevProps.couponListUpdate) {
      const {enable} = this.props.couponListUpdate;
      if (enable) {
        if (this.state.isActiveOnGoing) {
          this.getRewards(
            getLangValue(strings.ONGOING, this.state.currentLang),
          );
        } else {
          this.getRewards(
            getLangValue(strings.COMMING_SOON, this.state.currentLang),
          );
        }
        this.props.doUpdateCouponList({enable: false, key: ''});
      }
    }
    if (
      this.props.responseOnGoingRewards !== prevProps.responseOnGoingRewards
    ) {
      if (this.props.responseOnGoingRewards !== undefined) {
        const {detail} = this.props.responseOnGoingRewards;
        if (detail === undefined) {
          const {code, status, message} = this.props.responseOnGoingRewards;
          switch (code) {
            case 200:
              const {result} = this.props.responseOnGoingRewards;
              const {data, links, server, expired} = result;
              this.props.doSetServerTime(new Date(server).getTime());
              const {last, current} = links;
              if (current === 1) {
                this.setState({
                  rewardsOngoing: data,
                  refreshing_ongoing: false,
                  ongoing_last_page: last,
                  viewEmptyOnGoing: data.length <= 0,
                  serverTime: server,
                  expired: expired,
                });
              } else {
                this.setState({
                  rewardsOngoing: [...this.state.rewardsOngoing, ...data],
                  refreshing_ongoing: false,
                  ongoing_last_page: last,
                  viewEmptyOnGoing: false,
                  serverTime: server,
                  expired: expired,
                });
              }

              break;
            case 400:
              this.setState({
                rewardsOngoing: [],
                refreshing_ongoing: false,
                ongoing_last_page: undefined,
                viewEmptyOnGoing: true,
              });
              break;
            default:
              showErrorMessage(message);
              break;
          }
        } else {
          showErrorMessage(detail);
          AsyncStorage.removeItem(constants.USER);
          AsyncStorage.removeItem(constants.IS_LOGIN);
          this.props.doSetUser(undefined);
          this.props.navigation.replace('LoginRegister');
        }
      }
    }
    if (this.props.responseComingRewards !== prevProps.responseComingRewards) {
      if (this.props.responseComingRewards !== undefined) {
        const {code, status, message} = this.props.responseComingRewards;
        switch (code) {
          case 200:
            const {result} = this.props.responseComingRewards;
            const {data, links, expired, server} = result;
            this.props.doSetServerTime(new Date(server).getTime());
            const {last, current} = links;
            if (current === 1) {
              this.setState({
                rewardsComing: data,
                refreshing_coming: false,
                coming_last_page: last,
                viewEmptyComing: data.length <= 0,
                serverTime: server,
                expired: expired,
              });
              var serverDateTime = moment.utc(
                moment(server, 'YYYY-MM-DD HH:mm:ss'),
              );
              var date = moment.utc(moment(expired, 'YYYY-MM-DD HH:mm:ss'));
              var seconds = date.diff(serverDateTime, 'seconds');

              this.props.doSetCurrentTimeTimer(seconds);
              if (
                this.state.lastTimeTimer === undefined ||
                this.state.lastTimeTimer === 0
              ) {
                this.props.doSetLastTimeTimer(Math.max(0, seconds));
              }
            } else {
              this.setState({
                rewardsComing: [...this.state.rewardsComing, ...data],
                refreshing_coming: false,
                coming_last_page: last,
                viewEmptyComing: false,
                serverTime: server,
                expired: expired,
              });
            }
            break;
          case 400:
            this.setState({
              rewardsComing: [],
              refreshing_coming: false,
              coming_last_page: undefined,
              viewEmptyComing: true,
            });
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  componentWillUnmount() {
    NetInfo.removeEventListener(this._handleConnectivityChange);
  }
  render() {
    return (
      <View style={HomeTabStyle.container}>
        <View style={HomeTabStyle.viewHeader}>
          <Text style={HomeTabStyle.textHeaderHome}>
            {getLangValue(strings.HEADER_HOME, this.state.currentLang)}
          </Text>
          <View style={HomeTabStyle.viewBag}>
            <TouchableOpacity onPress={() => this.doOpenBag()}>
              <Image
                source={icons.BAG}
                style={HomeTabStyle.imgBag}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={HomeTabStyle.viewFlex}>
          <OfferTab
            tabOneTitle={getLangValue(strings.ONGOING, strings.LAG_ENG)}
            tabTwoTitle={getLangValue(strings.COMMING_SOON, strings.LAG_ENG)}
            renderTabOne={() => this.renderTabOne()}
            renderTabTwo={() => this.renderTabTwo()}
            getRewards={title => this.getRewards(title)}
          />
        </View>
        {this.props.isBusyOnGoingRewards || this.props.isBusyComingRewards ? (
          <Loader />
        ) : (
          undefined
        )}
        {this.state.isLoginAlert ? (
          <LoginAlert
            isLoginAlert={this.state.isLoginAlert}
            navigation={this.props.navigation}
            doNoClick={value => {
              this.setState({isLoginAlert: value});
            }}
          />
        ) : (
          undefined
        )}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    currentTimeTimer: state.app.currentTimeTimer,
    lastTimeTimer: state.app.lastTimeTimer,
    currentUser: state.app.currentUser,
    couponListUpdate: state.app.couponListUpdate,
    isBusyOnGoingRewards: state.app.isBusyOnGoingRewards,
    responseOnGoingRewards: state.app.responseOnGoingRewards,
    errorOnGoingRewards: state.app.errorOnGoingRewards,
    isBusyComingRewards: state.app.isBusyComingRewards,
    responseComingRewards: state.app.responseComingRewards,
    errorComingRewards: state.app.errorComingRewards,
    isBusyServerTime: state.app.isBusyServerTime,
    responseServerTime: state.app.responseServerTime,
    errorServerTime: state.app.errorServerTime,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {
        doGetOnGoingRewards,
        doGetComingRewards,
        doUpdateCouponList,
        doSetUser,
        doSetCurrentTimeTimer,
        doSetLastTimeTimer,
        doSetServerTime,
        doGetServerTime,
      },
      dispatch,
    ),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(HomeTabScreen);
