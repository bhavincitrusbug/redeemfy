import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {View, Text, TouchableOpacity, Image, ScrollView} from 'react-native';
import colors from '../../resources/colors';
import fonts from '../../resources/fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ButtonGradient from '../../components/buttons/ButtonGradient';
import {getLangValue, getErrorValue} from '../../resources/languages/language';
import strings from '../../resources/strings';
import icons from '../../resources/icons';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../../resources/constants';
import UserPageStyle from '../../resources/styles/UserPageStyle';
import {doLogout} from '../../redux/actions/AuthActions';
import {
  doSetUser,
  doGetNotificationSwitch,
  doPostNotificationSwitch,
} from '../../redux/actions/AppActions';
import NetInfo from '@react-native-community/netinfo';
import {showErrorMessage} from '../../resources/validation';
import errors from '../../resources/errors';
import Loader from '../../components/progress/Loader';
import {StackActions, NavigationActions} from 'react-navigation';
class UserTabScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentLang: strings.LAG_ENG,
      currentUser: props.currentUser,
      onoffnotification: false,
      fullName:
        props.currentUser !== undefined ? props.currentUser.full_name : '--',
      userName:
        props.currentUser !== undefined ? props.currentUser.email : '--',
      aboutUs:
        props.currentUser !== undefined ? props.currentUser.about_us : '--',
    };
  }
  toggleNotifications() {
    if (this.state.isConnected) {
      const params = {
        apiToken:
          this.state.currentUser !== undefined
            ? this.state.currentUser.token
            : '',
      };
      this.props.doPostNotificationSwitch(params);
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  }
  renderSwitch() {
    return (
      <TouchableOpacity onPress={() => this.toggleNotifications()}>
        <Image
          source={this.state.onoffnotification ? icons.ON : icons.OFF}
          style={UserPageStyle.imgSwitch}
          resizeMode="contain"
        />
      </TouchableOpacity>
    );
  }
  doRedirect = screen => {
    this.props.navigation.navigate(screen);
  };
  doLogout = () => {
    const {isConnected} = this.state;
    if (isConnected) {
      const params = {
        apiToken:
          this.state.currentUser !== undefined
            ? this.state.currentUser.token
            : '',
      };
      // this.props.doSetUser(undefined);
      // AsyncStorage.removeItem(constants.IS_LOGIN);
      // this.props.navigation.replace('LoginRegister');
      this.props.doLogout(params);
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  };
  renderTouchable(icon, title, right, switchEnable, onPress) {
    return (
      <TouchableOpacity onPress={onPress} disabled={onPress === undefined}>
        <View style={UserPageStyle.viewImg}>
          <Image
            source={icon}
            style={{height: wp(8), width: wp(8)}}
            resizeMode="contain"
          />
          <Text style={UserPageStyle.textTitle}>{title}</Text>
          {right !== undefined ? (
            <Image
              source={icons.RIGHT}
              style={UserPageStyle.imgRight}
              resizeMode="contain"
            />
          ) : (
            undefined
          )}
          {switchEnable !== undefined && switchEnable
            ? this.renderSwitch()
            : undefined}
        </View>
      </TouchableOpacity>
    );
  }
  getNotificationSwitch() {
    if (this.state.isConnected) {
      const params = {
        apiToken:
          this.state.currentUser !== undefined
            ? this.state.currentUser.token
            : '',
      };
      this.props.doGetNotificationSwitch(params);
    }
  }
  doFinish(screen) {
    AsyncStorage.removeItem(constants.USER);
    AsyncStorage.removeItem(constants.IS_LOGIN);
    AsyncStorage.clear();
    this.props.doSetUser(undefined);
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: screen,
        }),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  }
  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected,
    });
  };
  componentDidMount() {
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    this.getNotificationSwitch();
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.currentUser !== prevProps.currentUser) {
      this.setState({
        currentUser: this.props.currentUser,
        fullName:
          this.props.currentUser !== undefined
            ? this.props.currentUser.full_name
            : '--',
        userName:
          this.props.currentUser !== undefined
            ? this.props.currentUser.email
            : '--',
        aboutUs:
          this.props.currentUser !== undefined
            ? this.props.currentUser.about_us
            : '--',
      });
    }
    if (
      this.props.responseGetNotificationSwitch !==
      prevProps.responseGetNotificationSwitch
    ) {
      const {detail} = this.props.responseGetNotificationSwitch;
      if (detail === undefined) {
        const {
          code,
          status,
          message,
        } = this.props.responseGetNotificationSwitch;
        switch (code) {
          case 200:
            const {data} = this.props.responseGetNotificationSwitch.result;
            const {notification} = data;
            this.setState({onoffnotification: notification});
            break;
          default:
            showErrorMessage(message);
            break;
        }
      } else {
        showErrorMessage(detail);
        this.doFinish('LoginRegister');
      }
    }
    if (
      this.props.responseSetNotificationSwitch !==
      prevProps.responseSetNotificationSwitch
    ) {
      const {detail} = this.props.responseSetNotificationSwitch;
      if (detail === undefined) {
        const {
          code,
          status,
          message,
        } = this.props.responseSetNotificationSwitch;
        switch (code) {
          case 200:
            this.setState({onoffnotification: !this.state.onoffnotification});
            break;
          default:
            showErrorMessage(message);
            break;
        }
      } else {
        showErrorMessage(detail);
        this.doFinish('LoginRegister');
      }
    }
    if (this.props.responseLogout !== prevProps.responseLogout) {
      if (this.props.responseLogout !== undefined) {
        const {code, status, message} = this.props.responseLogout;
        switch (code) {
          case 200:
            this.doFinish('LoginRegister');
            break;
          default:
            this.doFinish('LoginRegister');
            // showErrorMessage(message);
            break;
        }
      }
    }
    if (this.props.errorLogout !== prevProps.errorLogout) {
      if (this.props.errorLogout !== undefined) {
        this.doFinish('LoginRegister');
      }
    }
  }

  componentWillUnmount() {
    this._subscription();
  }
  render() {
    return (
      <View style={UserPageStyle.container}>
        <ScrollView>
          <View style={UserPageStyle.viewUserInfo}>
            <Text style={UserPageStyle.textName}>{this.state.fullName}</Text>
            <Text style={UserPageStyle.textUserName}>
              {this.state.userName}
            </Text>
            <Text style={UserPageStyle.textAboutUs}>{this.state.aboutUs}</Text>
            <View style={UserPageStyle.viewButtonEditProfile}>
              <ButtonGradient
                title={getLangValue(
                  strings.BTN_EDIT_PROFILE,
                  this.state.currentLang,
                )}
                backgroundColor={colors.orange}
                fontFamily={fonts.Dosis_Bold}
                fontSize={RFPercentage(3)}
                onPress={() => this.doRedirect('EditProfile')}
                width={wp(90)}
                height={hp(5)}
                currentLang={this.state.currentLang}
                borderRadius={10}
              />
            </View>
            <View style={UserPageStyle.viewTouchInfo}>
              {this.renderTouchable(
                icons.BANK,
                getLangValue(strings.BANK_DETAIL, this.state.currentLang),
                icons.RIGHT,
                false,
                () => this.doRedirect('EditBankDetail'),
              )}
              <View style={UserPageStyle.viewBorderLine} />
              {this.renderTouchable(
                icons.WALLET,
                getLangValue(strings.WALLET, this.state.currentLang),
                icons.RIGHT,
                false,
                () => this.doRedirect('Wallet'),
              )}
              <View style={UserPageStyle.viewBorderLine} />
              {this.renderTouchable(
                icons.CASH,
                getLangValue(strings.CASH_OUT, this.state.currentLang),
                icons.RIGHT,
                false,
                () => this.doRedirect('CashoutRequest'),
              )}
              <View style={UserPageStyle.viewBorderLine} />
              {this.renderTouchable(
                icons.NOTIFICATION,
                getLangValue(strings.NOTIFICATIONS, this.state.currentLang),
                icons.RIGHT,
                false,
                () => this.doRedirect('Notification'),
              )}
              <View style={UserPageStyle.viewBorderLine} />
              {this.renderTouchable(
                icons.PRIVACY_POLICY,
                getLangValue(strings.PRIVACY_POLICY, this.state.currentLang),
                icons.RIGHT,
                false,
                () => this.doRedirect('PrivacyPolicy'),
              )}
              <View style={UserPageStyle.viewBorderLine} />
              {this.renderTouchable(
                icons.TC,
                getLangValue(
                  strings.TERMS_AND_CONDITION,
                  this.state.currentLang,
                ),
                icons.RIGHT,
                false,
                () => this.doRedirect('TermsCondition'),
              )}
              <View style={UserPageStyle.viewBorderLine} />
              {this.renderTouchable(
                icons.SETTING,
                getLangValue(strings.ONOFFNOTIFICATION, this.state.currentLang),
                undefined,
                true,
                undefined,
              )}
              <View style={UserPageStyle.viewBorderLine} />

              {this.renderTouchable(
                icons.TC,
                getLangValue(strings.CHANGE_PASSWORD, this.state.currentLang),
                icons.RIGHT,
                false,
                () => this.doRedirect('ChangePassword'),
              )}
              <View style={UserPageStyle.viewBorderLine} />
              {this.renderTouchable(
                icons.LOGOUT,
                getLangValue(strings.LOGOUT, this.state.currentLang),
                undefined,
                false,
                () => this.doLogout(),
              )}
            </View>
          </View>
        </ScrollView>
        {this.props.isBusyLogout ||
        this.props.isBusyGetNotificationSwitch ||
        this.props.isBusySetNotificationSwitch ? (
          <Loader />
        ) : (
          undefined
        )}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    isBusyGetNotificationSwitch: state.app.isBusyGetNotificationSwitch,
    responseGetNotificationSwitch: state.app.responseGetNotificationSwitch,
    errorGetNotificationSwitch: state.app.errorGetNotificationSwitch,
    isBusySetNotificationSwitch: state.app.isBusySetNotificationSwitch,
    responseSetNotificationSwitch: state.app.responseSetNotificationSwitch,
    errorSetNotificationSwitch: state.app.errorSetNotificationSwitch,
    isBusyLogout: state.auth.isBusyLogout,
    responseLogout: state.auth.responseLogout,
    errorLogout: state.auth.errorLogout,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {doSetUser, doLogout, doGetNotificationSwitch, doPostNotificationSwitch},
      dispatch,
    ),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(UserTabScreen);
