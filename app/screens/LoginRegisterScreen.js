import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image,
  ScrollView,
  Platform,
  TouchableHighlight,
} from 'react-native';
import colors from '../resources/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {getLangValue, getErrorValue} from '../resources/languages/language';
import strings from '../resources/strings';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../resources/fonts';
import icons from '../resources/icons';
import {showErrorMessage, validateEmail} from '../resources/validation';
import constants from '../resources/constants';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import errors from '../resources/errors';
import firebase from 'react-native-firebase';
import ButtonGradient from '../components/buttons/ButtonGradient';
import Reinput from 'reinput';
import LoginRegisterStyle from '../resources/styles/LoginRegisterStyle';
import {doLogin, doRegister} from '../redux/actions/AuthActions';
import {doSetUser} from '../redux/actions/AppActions';
import Loader from '../components/progress/Loader';
import PhoneNumberInput from '../components/texts/PhoneNumberInput';
import {StackActions, NavigationActions} from 'react-navigation';
import CheckBox from 'react-native-check-box';

_isMounted = false;
class LoginRegisterScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentLang: strings.LAG_ENG,
      firebaseToken: '',
      emailLogin: '',
      errEmailLogin: undefined,
      passwordLogin: '',
      errPasswordLogin: undefined,
      fullName: '',
      errFullName: undefined,
      email: '',
      errEmail: undefined,
      password: '',
      errPassword: undefined,
      phoneNumber: '',
      errPhoneNumber: undefined,
      activeTabOne: true,
      activeTabTwo: false,
      loader: true,
      country_code: '+65',
      errCountryCode: undefined,
      isPhoneNumberFocus: false,
      isTermsPrivacyPolicy: false,
    };
  }
  _bootstrapAsync = async () => {
    const isLogin = await AsyncStorage.getItem(constants.IS_LOGIN);
    const user = await AsyncStorage.getItem(constants.USER);
    clearTimeout(this.timeoutSkip);
    this.timeoutSkip = setTimeout(() => {
      if (user && user !== undefined) {
        this.props.doSetUser(JSON.parse(user));
      }
      if (isLogin && isLogin === 'true') {
        this.doGuestLogin();
      }

      this.setState({loader: false});
    }, 500);
  };
  async getToken() {
    let fcmToken = await firebase.messaging().getToken();
    this.setState({firebaseToken: fcmToken});
  }
  doRedirect = screen => {
    this.props.navigation.navigate(screen);
  };
  doGuestLogin = () => {
    this.doFinish('Home');
  };
  doLogin = () => {
    const {isConnected, firebaseToken, emailLogin, passwordLogin} = this.state;
    if (emailLogin === '') {
      this.setState(
        {
          errEmailLogin: getErrorValue(
            errors.ENTER_EMAIL,
            this.state.currentLang,
          ),
        },
        () => {
          this.emailLoginInput.focus();
        },
      );
      return;
    }
    if (!validateEmail(emailLogin)) {
      this.setState(
        {
          errEmailLogin: getErrorValue(
            errors.ENTER_VALID_EMAIL,
            this.state.currentLang,
          ),
        },
        () => {
          this.emailLoginInput.focus();
        },
      );
      return;
    }
    this.setState({errEmailLogin: undefined});
    if (passwordLogin === '') {
      this.setState(
        {
          errPasswordLogin: getErrorValue(
            errors.ENTER_PASSWORD,
            this.state.currentLang,
          ),
        },
        () => {
          this.passwordLoginInput.focus();
        },
      );
      return;
    }
    this.setState({errEmailLogin: undefined, errPasswordLogin: undefined});
    if (isConnected) {
      const params = {
        email: emailLogin,
        password: passwordLogin,
        deviceType: Platform.OS,
        deviceToken: firebaseToken,
      };
      this.props.doLogin(params).catch(error => {
        showErrorMessage(error.message);
      });
      // AsyncStorage.setItem(constants.IS_LOGIN, 'true');
      // this.props.navigation.replace('Home');
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  };

  doRegister = () => {
    const {
      isConnected,
      firebaseToken,
      fullName,
      email,
      password,
      phoneNumber,
      country_code,
      errPhoneNumber,
      isTermsPrivacyPolicy,
    } = this.state;
    this.phoneNumberInput.updateState(
      country_code,
      phoneNumber,
      errPhoneNumber,
    );
    if (fullName === '') {
      this.setState(
        {
          errFullName: getErrorValue(
            errors.ENTER_FULL_NAME,
            this.state.currentLang,
          ),
        },
        () => {
          this.fullNameInput.focus();
        },
      );
      return;
    }
    this.setState({errFullName: undefined});
    if (email === '') {
      this.setState(
        {
          errEmail: getErrorValue(errors.ENTER_EMAIL, this.state.currentLang),
        },
        () => {
          this.emailInput.focus();
        },
      );
      return;
    }
    if (!validateEmail(email)) {
      this.setState(
        {
          errEmail: getErrorValue(
            errors.ENTER_VALID_EMAIL,
            this.state.currentLang,
          ),
        },
        () => {
          this.emailInput.focus();
        },
      );
      return;
    }
    this.setState({errEmail: undefined});
    if (password === '') {
      this.setState(
        {
          errPassword: getErrorValue(
            errors.ENTER_PASSWORD,
            this.state.currentLang,
          ),
        },
        () => {
          this.passwordInput.focus();
        },
      );
      return;
    }
    if (phoneNumber === '') {
      this.setState(
        {
          errPhoneNumber: getErrorValue(
            errors.ENTER_PHONE_NUMBER,
            this.state.currentLang,
          ),
        },
        () => {
          this.phoneNumberInput.updateState(
            country_code,
            phoneNumber,
            errPhoneNumber,
          );
          this.phoneInput.focus();
        },
      );
      return;
    }
    if (country_code === null || country_code === undefined) {
      this.setState(
        {
          errPhoneNumber: getErrorValue(
            errors.SELECT_COUNTRY_CODE,
            this.state.currentLang,
          ),
        },
        () => {
          this.phoneNumberInput.phoneInput.focus();
        },
      );
      return;
    }
    if (!isTermsPrivacyPolicy) {
      showErrorMessage(
        getErrorValue(errors.ACCEPT_TERMS_PRIVACY, this.state.currentLang),
      );
      return;
    }
    this.setState({
      errEmail: undefined,
      errPassword: undefined,
      errPhoneNumber: undefined,
    });

    if (isConnected) {
      const params = {
        full_name: fullName,
        email: email,
        password: password,
        contact: country_code + phoneNumber.replace(/[^0-9]/g, ''),
        deviceType: Platform.OS,
        deviceToken: firebaseToken,
        profile_image: '',
      };
      // this.props.navigation.navigate('VerifyPhone', {
      //   phoneNumber: phoneNumber.replace(/[^0-9]/g, ''),
      // });
      this.props.doRegister(params).catch(error => {
        showErrorMessage(error.message);
      });
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  };
  doFinish(screen) {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: screen,
        }),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  }
  _handleConnectivityChange = state => {
    if (this._isMounted) {
      this.setState({
        isConnected: state.isConnected,
      });
    }
  };
  componentDidMount() {
    this._isMounted = true;
    this.getToken();
    this._bootstrapAsync();
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
  }

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.responseLogin !== prevProps.responseLogin) {
      if (this.props.responseLogin !== undefined) {
        const {code, status, message} = this.props.responseLogin;
        switch (code) {
          case 200:
            if (status) {
              const {data} = this.props.responseLogin.result;
              this.setState({emailLogin: '', passwordLogin: ''});
              this.props.doSetUser(data);
              AsyncStorage.setItem(constants.USER, JSON.stringify(data));
              AsyncStorage.setItem(constants.IS_LOGIN, 'true');
              this.doFinish('Home');
            } else {
              showErrorMessage(message);
            }

            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
    if (this.props.responseRegister !== prevProps.responseRegister) {
      if (this.props.responseRegister !== undefined) {
        const {code, status, message} = this.props.responseRegister;
        switch (code) {
          case 200:
            const {data} = this.props.responseRegister.result;
            if (status) {
              this.setState({
                fullName: '',
                email: '',
                password: '',
              });
              this.props.doSetUser(data);
              AsyncStorage.setItem(constants.USER, JSON.stringify(data));
              AsyncStorage.setItem(constants.IS_LOGIN, 'true');
              this.props.navigation.navigate('VerifyPhone', {
                phoneNumber: this.state.phoneNumber.replace(/[^0-9]/g, ''),
                email: data.email,
                from: 'LoginRegister',
              });
            } else {
              showErrorMessage(message);
            }

            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
    NetInfo.removeEventListener(this._handleConnectivityChange);
  }
  renderTabOne() {
    return (
      <View>
        <View style={{marginStart: wp(5), marginEnd: wp(5), marginTop: hp(2)}}>
          <Reinput
            ref={input => (this.emailLoginInput = input)}
            label={getLangValue(strings.EMAIL, this.state.currentLang)}
            labelColor={colors.colorLabelInActive}
            labelActiveColor={colors.colorLabelActive}
            color={colors.white}
            activeColor={colors.white}
            fontFamily={fonts.Dosis_Regular}
            value={this.state.emailLogin}
            paddingTop={5}
            paddingBottom={5}
            onChangeText={text => {
              this.setState({emailLogin: text}, () => {
                this.state.emailLogin === ''
                  ? this.setState({
                      errEmailLogin: getErrorValue(
                        errors.ENTER_EMAIL,
                        this.state.currentLang,
                      ),
                    })
                  : !validateEmail(this.state.emailLogin)
                  ? this.setState({
                      errEmailLogin: getErrorValue(
                        errors.ENTER_VALID_EMAIL,
                        this.state.currentLang,
                      ),
                    })
                  : this.setState({errEmailLogin: undefined});
              });
            }}
            error={this.state.errEmailLogin}
            fontSize={RFPercentage(3)}
            underlineColor={colors.colorLine}
            keyboardType="email-address"
            autoCapitalize="none"
            returnKeyType="next"
            onSubmitEditing={() => {
              this.passwordLoginInput.focus();
            }}
          />
          <View
            style={{
              flexDirection: 'row',
            }}>
            <View style={{flex: 1}}>
              <Reinput
                ref={input => (this.passwordLoginInput = input)}
                label={getLangValue(strings.PASSWORD, this.state.currentLang)}
                fontFamily={fonts.Dosis_Regular}
                labelColor={colors.colorLabelInActive}
                labelActiveColor={colors.colorLabelActive}
                color={colors.white}
                activeColor={colors.white}
                value={this.state.passwordLogin}
                paddingTop={5}
                paddingBottom={5}
                onChangeText={text => {
                  this.setState({passwordLogin: text}, () => {
                    this.state.passwordLogin === ''
                      ? this.setState({
                          errPasswordLogin: getErrorValue(
                            errors.ENTER_PASSWORD,
                            this.state.currentLang,
                          ),
                        })
                      : this.setState({errPasswordLogin: undefined});
                  });
                }}
                error={this.state.errPasswordLogin}
                fontSize={RFPercentage(3)}
                underlineColor={colors.colorLine}
                secureTextEntry={!this.state.isPasswordVisibleLogin}
                autoCapitalize="none"
                returnKeyType="done"
                onSubmitEditing={() => {
                  this.state.passwordLogin.length > 0
                    ? this.setState({errPasswordLogin: undefined})
                    : null;
                }}
              />
            </View>
            <View
              style={{
                position: 'absolute',
                end: 0,
                top: hp(3),
                marginBottom: 5,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    isPasswordVisibleLogin: !this.state.isPasswordVisibleLogin,
                  });
                }}>
                <Image
                  resizeMode="contain"
                  source={icons.EYE}
                  style={{height: hp(5), width: wp(5)}}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{marginTop: hp(5), alignSelf: 'center'}}>
            <ButtonGradient
              title={getLangValue(strings.BTN_SIGN_IN, this.state.currentLang)}
              backgroundColor={colors.orange}
              fontFamily={fonts.Dosis_Bold}
              fontSize={RFPercentage(3)}
              onPress={() => this.doLogin()}
              width={wp(80)}
              height={hp(6)}
              currentLang={this.state.currentLang}
              borderRadius={23}
            />
          </View>
          <View
            style={{
              alignSelf: 'center',
              marginTop: hp(5),
              marginBottom: hp(5),
            }}>
            <TouchableOpacity onPress={() => this.doRedirect('ForgotPassword')}>
              <Text
                style={{
                  color: colors.white,
                  fontSize:
                    Platform.OS === 'ios' ? RFPercentage(2) : RFPercentage(2.5),
                  fontFamily: fonts.Dosis_Regular,
                }}>
                {getLangValue(strings.I_FORGOT_PASSWORD, strings.LAG_ENG)}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
  renderTabTwo() {
    return (
      <View style={{marginStart: wp(5), marginEnd: wp(5), marginTop: hp(2)}}>
        <Reinput
          ref={input => (this.fullNameInput = input)}
          label={getLangValue(strings.FULL_NAME, this.state.currentLang)}
          labelColor={colors.colorLabelInActive}
          labelActiveColor={colors.colorLabelActive}
          color={colors.white}
          activeColor={colors.white}
          fontFamily={fonts.Dosis_Regular}
          value={this.state.fullName}
          paddingTop={5}
          paddingBottom={5}
          onChangeText={text => {
            this.setState({fullName: text}, () => {
              this.state.fullName === ''
                ? this.setState({
                    errFullName: getErrorValue(
                      errors.ENTER_FULL_NAME,
                      this.state.currentLang,
                    ),
                  })
                : this.setState({errFullName: undefined});
            });
          }}
          error={this.state.errFullName}
          fontSize={RFPercentage(3)}
          underlineColor={colors.colorLine}
          returnKeyType="next"
          onSubmitEditing={() => {
            this.state.fullName.length > 0
              ? this.emailInput.focus()
              : this.fullNameInput.focus();
          }}
        />
        <Reinput
          ref={input => (this.emailInput = input)}
          label={getLangValue(strings.EMAIL, this.state.currentLang)}
          labelColor={colors.colorLabelInActive}
          labelActiveColor={colors.colorLabelActive}
          color={colors.white}
          activeColor={colors.white}
          fontFamily={fonts.Dosis_Regular}
          value={this.state.email}
          paddingTop={5}
          paddingBottom={5}
          onChangeText={text => {
            this.setState({email: text}, () => {
              this.state.email === ''
                ? this.setState({
                    errEmail: getErrorValue(
                      errors.ENTER_EMAIL,
                      this.state.currentLang,
                    ),
                  })
                : !validateEmail(this.state.email)
                ? this.setState({
                    errEmail: getErrorValue(
                      errors.ENTER_VALID_EMAIL,
                      this.state.currentLang,
                    ),
                  })
                : this.setState({errEmail: undefined});
            });
          }}
          error={this.state.errEmail}
          fontSize={RFPercentage(3)}
          underlineColor={colors.colorLine}
          keyboardType="email-address"
          autoCapitalize="none"
          returnKeyType="next"
          onSubmitEditing={() => {
            this.passwordInput.focus();
          }}
        />
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 1}}>
            <Reinput
              ref={input => (this.passwordInput = input)}
              label={getLangValue(strings.PASSWORD, this.state.currentLang)}
              fontFamily={fonts.Dosis_Regular}
              labelColor={colors.colorLabelInActive}
              labelActiveColor={colors.colorLabelActive}
              color={colors.white}
              activeColor={colors.white}
              value={this.state.password}
              paddingTop={5}
              paddingBottom={5}
              onChangeText={text => {
                this.setState({password: text}, () => {
                  this.state.password === ''
                    ? this.setState({
                        errPassword: getErrorValue(
                          errors.ENTER_PASSWORD,
                          this.state.currentLang,
                        ),
                      })
                    : this.setState({errPassword: undefined});
                });
              }}
              error={this.state.errPassword}
              fontSize={RFPercentage(3)}
              underlineColor={colors.colorLine}
              secureTextEntry={!this.state.isPasswordVisibleRegister}
              autoCapitalize="none"
              returnKeyType="next"
              onSubmitEditing={() => {
                this.state.password.length > 0
                  ? this.setState({errPassword: undefined})
                  : this.phoneInput.focus();
              }}
            />
          </View>
          <View
            style={{
              position: 'absolute',
              end: 0,
              top: hp(2.5),
              marginBottom: 5,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  isPasswordVisibleRegister: !this.state
                    .isPasswordVisibleRegister,
                });
              }}>
              <Image
                resizeMode="contain"
                source={icons.EYE}
                style={{height: hp(5), width: wp(5)}}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{marginTop: Platform.OS === 'ios' ? hp(2) : hp(1)}}>
          <PhoneNumberInput
            ref={input => (this.phoneNumberInput = input)}
            currentLang={this.state.currentLang}
            label={getLangValue(strings.PHONE_NUMBER, this.state.currentLang)}
            labelColor={colors.colorLabelInActive}
            onComplete={(
              country_code,
              phoneNumber,
              errPhoneNumber,
              phoneInput,
            ) => {
              this.phoneInput = phoneInput;
              this.setState({
                country_code: country_code,
                phoneNumber: phoneNumber,
                errPhoneNumber: errPhoneNumber,
              });
            }}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: hp(2),
            marginStart: wp(2),
            marginEnd: wp(2),

            marginBottom: hp(2),
          }}>
          <CheckBox
            onClick={() => {
              this.setState({
                isTermsPrivacyPolicy: !this.state.isTermsPrivacyPolicy,
              });
            }}
            isChecked={this.state.isTermsPrivacyPolicy}
            checkedCheckBoxColor={colors.white}
            uncheckedCheckBoxColor="rgba(255,255,255,0.5)"
          />
          <View>
            <Text
              style={{
                color: colors.white,
                fontSize:
                  Platform.OS === 'ios' ? RFPercentage(2) : RFPercentage(2.5),
                fontFamily: fonts.Dosis_Light,
                marginStart: 5,
                fontWeight: '400',
              }}>
              {getLangValue(strings.BY_CLICK_REGISTER, strings.LAG_ENG)}
            </Text>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => this.doRedirect('TermsCondition')}>
                <Text
                  style={{
                    marginStart: 5,
                    color: colors.white,
                    fontSize:
                      Platform.OS === 'ios'
                        ? RFPercentage(2)
                        : RFPercentage(2.5),
                    fontFamily: fonts.Dosis_Bold,
                    fontWeight: '600',
                  }}>
                  {getLangValue(strings.TERMS_AND_CONDITION, strings.LAG_ENG)}
                </Text>
              </TouchableOpacity>
              <Text
                style={{
                  marginStart: 5,
                  color: colors.white,
                  fontSize:
                    Platform.OS === 'ios' ? RFPercentage(2) : RFPercentage(2.5),
                  fontFamily: fonts.Dosis_Bold,
                  fontWeight: '600',
                }}>
                &
              </Text>
              <TouchableOpacity
                onPress={() => this.doRedirect('PrivacyPolicy')}>
                <Text
                  style={{
                    marginStart: 5,
                    color: colors.white,
                    fontSize:
                      Platform.OS === 'ios'
                        ? RFPercentage(2)
                        : RFPercentage(2.5),
                    fontFamily: fonts.Dosis_Bold,
                    fontWeight: '600',
                  }}>
                  {getLangValue(strings.PRIVACY_POLICY, strings.LAG_ENG)}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View
          style={{
            marginTop: hp(2.5),
            alignSelf: 'center',
            marginBottom: hp(5),
          }}>
          <ButtonGradient
            title={getLangValue(strings.BTN_REGISTER, this.state.currentLang)}
            backgroundColor={colors.orange}
            fontFamily={fonts.Dosis_Bold}
            fontSize={RFPercentage(3)}
            onPress={() => this.doRegister()}
            width={wp(80)}
            height={hp(6)}
            currentLang={this.state.currentLang}
            borderRadius={23}
          />
        </View>
      </View>
    );
  }
  render() {
    return (
      <View style={LoginRegisterStyle.container}>
        <ScrollView>
          <View>
            <View style={LoginRegisterStyle.viewGuest}>
              <TouchableOpacity onPress={this.doGuestLogin}>
                <Text style={LoginRegisterStyle.textGuest}>
                  {getLangValue(strings.GUEST, strings.LAG_ENG)}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={LoginRegisterStyle.viewLogo}>
              <Image source={icons.LOGO} style={LoginRegisterStyle.imgLogo} />
            </View>
            <View style={LoginRegisterStyle.viewTabLabel}>
              <TouchableHighlight
                activeOpacity={1.0}
                underlayColor={colors.colorTransparent}
                onPress={() =>
                  this.setState({
                    activeTabOne: true,
                    activeTabTwo: false,
                    emailLogin: '',
                    passwordLogin: '',
                    errEmailLogin: undefined,
                    errPasswordLogin: undefined,
                  })
                }>
                <ImageBackground
                  style={
                    this.state.activeTabOne
                      ? LoginRegisterStyle.viewImgActive
                      : LoginRegisterStyle.viewImgInActive
                  }
                  imageStyle={LoginRegisterStyle.imgTabLeft}
                  source={
                    this.state.activeTabOne
                      ? icons.BG_TAB_1_ACTIVE
                      : icons.BG_TAB_1_IN_ACTIVE
                  }>
                  <View style={LoginRegisterStyle.viewFlex}>
                    <Text style={LoginRegisterStyle.textSignIn}>
                      {getLangValue(strings.SIGNIN, strings.LAG_ENG)}
                    </Text>
                  </View>
                </ImageBackground>
              </TouchableHighlight>
              <TouchableHighlight
                activeOpacity={1.0}
                underlayColor={colors.colorTransparent}
                onPress={() =>
                  this.setState({
                    activeTabOne: false,
                    activeTabTwo: true,
                    fullName: '',
                    email: '',
                    password: '',
                    phoneNumber: '',
                    errFullName: undefined,
                    errEmail: undefined,
                    errPassword: undefined,
                    errPhoneNumber: undefined,
                  })
                }>
                <ImageBackground
                  style={
                    this.state.activeTabTwo
                      ? LoginRegisterStyle.viewImgActive
                      : LoginRegisterStyle.viewImgInActive
                  }
                  imageStyle={LoginRegisterStyle.imgTabRight}
                  source={
                    this.state.activeTabTwo
                      ? icons.BG_TAB_2_ACTIVE
                      : icons.BG_TAB_2_IN_ACTIVE
                  }>
                  <View style={LoginRegisterStyle.viewFlex}>
                    <Text style={LoginRegisterStyle.textRegister}>
                      {getLangValue(strings.REGISTER, strings.LAG_ENG)}
                    </Text>
                  </View>
                </ImageBackground>
              </TouchableHighlight>
            </View>
            <View style={LoginRegisterStyle.viewTabItem}>
              {this.state.activeTabOne ? this.renderTabOne() : undefined}
              {this.state.activeTabTwo ? this.renderTabTwo() : undefined}
            </View>
          </View>
        </ScrollView>
        {this.props.isBusyLogin ||
        this.props.isBusyRegister ||
        this.state.loader ? (
          <Loader />
        ) : (
          undefined
        )}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    isBusyLogin: state.auth.isBusyLogin,
    responseLogin: state.auth.responseLogin,
    errorLogin: state.auth.errorLogin,
    isBusyRegister: state.auth.isBusyRegister,
    responseRegister: state.auth.responseRegister,
    errorRegister: state.auth.errorRegister,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({doLogin, doRegister, doSetUser}, dispatch),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginRegisterScreen);
