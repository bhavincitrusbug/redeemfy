import React, {PureComponent} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  BackHandler,
} from 'react-native';
import colors from '../resources/colors';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import fonts from '../resources/fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ButtonGradient from '../components/buttons/ButtonGradient';
import {getLangValue} from '../resources/languages/language';
import strings from '../resources/strings';
import BuySuccessStyle from '../resources/styles/BuySuccessStyle';
import {StackActions, NavigationActions} from 'react-navigation';
class BuySuccessScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      message: 'Successfully buy!',
      description: 'Please wait',
    };
  }
  doFinish(screen) {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: screen,
        }),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  }
  doBackClick = () => {
    this.doFinish('Home');
  };
  handleBackPress = () => {
    this.doBackClick(); // works best when the goBack is async
    return true;
  };
  componentDidMount() {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackPress,
    );
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  render() {
    return (
      <View style={BuySuccessStyle.container}>
        <View style={BuySuccessStyle.viewHeader}>
          <TouchableOpacity onPress={() => this.doBackClick()}>
            <Image source={icons.BACK} style={BuySuccessStyle.imgBack} />
          </TouchableOpacity>
        </View>
        <ScrollView>
          <View style={BuySuccessStyle.viewMain}>
            <Text style={BuySuccessStyle.textTitle}>{this.state.message}</Text>
            <Text style={BuySuccessStyle.textDescription}>
              {this.state.description}
            </Text>
          </View>
        </ScrollView>
        <View style={BuySuccessStyle.viewButtonClaim}>
          <ButtonGradient
            title={getLangValue(
              strings.BTN_BACK_TO_HOME,
              this.state.currentLang,
            )}
            backgroundColor={colors.orange}
            fontFamily={fonts.Dosis_Bold}
            fontSize={RFPercentage(3)}
            onPress={() => this.doBackClick()}
            width={wp(100)}
            height={hp(10)}
            currentLang={this.state.currentLang}
            borderRadius={0}
          />
        </View>
      </View>
    );
  }
}
export default BuySuccessScreen;
