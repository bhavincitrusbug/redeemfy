import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Modal,
  TextInput,
  TouchableHighlight,
} from 'react-native';
import icons from '../resources/icons';
import CashoutRequestStyle from '../resources/styles/CashoutRequestStyle';
import {RFPercentage} from 'react-native-responsive-fontsize';
import colors from '../resources/colors';
import fonts from '../resources/fonts';
import strings from '../resources/strings';
import {getLangValue, getErrorValue} from '../resources/languages/language';
import {doPostCashoutRequest} from '../redux/actions/AppActions';
import ButtonGradient from '../components/buttons/ButtonGradient';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import NetInfo from '@react-native-community/netinfo';
import {showErrorMessage, showSuccessMessage} from '../resources/validation';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../resources/constants';
import Loader from '../components/progress/Loader';
import errors from '../resources/errors';
_isMounted = false;
class CashoutRequestScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentUser: props.currentUser,
      currentLang: props.currentLang,
      isAmountModelVisible: false,
      title: 'TITLE',
      message: 'Condition to Cash Out',
      terms: 'More terms and condition',
      amount: '1',
    };
  }
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  showAmountModal() {
    this.setState({isAmountModelVisible: true});
  }
  hideSellModal() {
    this.setState({isAmountModelVisible: false});
  }

  doRequestCash = () => {
    this.showAmountModal();
  };
  doConfirmSell = () => {
    this.hideSellModal();
    const {currentUser, isConnected, amount} = this.state;
    if (isConnected) {
      const params = {
        apiToken: currentUser !== undefined ? currentUser.token : '',
        userId: currentUser !== undefined ? currentUser.id : '',
        amount: amount,
      };
      this.props.doPostCashoutRequest(params);
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  };

  renderAmountModel() {
    return (
      <Modal
        animationType="slide"
        presentationStyle="overFullScreen"
        transparent={true}
        visible={this.state.isAmountModelVisible}>
        <TouchableHighlight
          style={CashoutRequestStyle.touchModalMain}
          underlayColor={colors.colorTransparent}
          onPress={() => this.hideSellModal()}
          activeOpacity={1.0}>
          <View style={CashoutRequestStyle.viewModalMain}>
            <View style={CashoutRequestStyle.viewModalBackground}>
              <Text style={CashoutRequestStyle.textEnterQty}>
                {getLangValue(
                  strings.ENTER_AMOUNT_TO_CASHOUT,
                  this.state.currentLang,
                )}
              </Text>
              <View style={CashoutRequestStyle.viewQtyItem}>
                <View style={CashoutRequestStyle.viewQtyText}>
                  <TextInput
                    style={CashoutRequestStyle.inputQty}
                    value={`$` + this.state.amount}
                    returnKeyType="done"
                    keyboardType="numeric"
                    onChangeText={text => {
                      this.setState({amount: text.replace(/\D/g, '')});
                    }}
                    underlineColorAndroid={colors.colorTransparent}
                  />
                </View>
              </View>
              <View style={CashoutRequestStyle.viewButtonConfirm}>
                <ButtonGradient
                  title={getLangValue(
                    strings.BTN_CONFIRM,
                    this.state.currentLang,
                  )}
                  backgroundColor={colors.orange}
                  fontFamily={fonts.Dosis_Bold}
                  fontSize={RFPercentage(2.5)}
                  onPress={() => this.doConfirmSell()}
                  width={wp(70)}
                  height={hp(6)}
                  currentLang={this.state.currentLang}
                  borderRadius={23}
                />
              </View>
            </View>
          </View>
        </TouchableHighlight>
      </Modal>
    );
  }
  _handleConnectivityChange = state => {
    if (this._isMounted) {
      this.setState({
        isConnected: state.isConnected,
      });
    }
  };
  componentDidMount() {
    this._isMounted = true;

    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
  }

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.responseCashout !== prevProps.responseCashout) {
      if (this.props.responseCashout !== undefined) {
        const {code, status, message} = this.props.responseCashout;
        switch (code) {
          case 200:
            if (status) {
              this.props.navigation.navigate('CashoutSuccess');
            }
            showSuccessMessage(message);

            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
    this._subscription();
  }
  render() {
    return (
      <View style={CashoutRequestStyle.container}>
        <View style={CashoutRequestStyle.viewHeader}>
          <TouchableOpacity onPress={() => this.doBackClick()}>
            <Image source={icons.BACK} style={CashoutRequestStyle.imgBack} />
          </TouchableOpacity>
        </View>
        <ScrollView>
          <View style={CashoutRequestStyle.viewMain}>
            <Text style={CashoutRequestStyle.textTitle}>
              {this.state.title}
            </Text>
            <Text style={CashoutRequestStyle.textMessage}>
              {this.state.message}
            </Text>
            <Text style={CashoutRequestStyle.textTerms}>
              {this.state.terms}
            </Text>
          </View>
        </ScrollView>
        <View style={CashoutRequestStyle.viewButtonRequest}>
          <ButtonGradient
            title={getLangValue(strings.BTN_REQUEST, this.state.currentLang)}
            backgroundColor={colors.orange}
            fontFamily={fonts.Dosis_Bold}
            fontSize={RFPercentage(2.5)}
            onPress={() => this.doRequestCash()}
            width={wp(90)}
            height={hp(6)}
            currentLang={this.state.currentLang}
            borderRadius={23}
          />
        </View>
        {this.renderAmountModel()}
        {this.props.isBusyCashout ? <Loader /> : undefined}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    isBusyCashout: state.app.isBusyCashout,
    responseCashout: state.app.responseCashout,
    errorCashout: state.app.errorCashout,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({doPostCashoutRequest}, dispatch),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CashoutRequestScreen);
