import React, {PureComponent} from 'react';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import AppIndex from '.';
import IntroductionScreen from './IntroductionScreen';
import {View, Image} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../resources/constants';
import styles from '../resources/styles/index';
import {BallIndicator} from 'react-native-indicators';
import colors from '../resources/colors';
import icons from '../resources/icons';
import {
  widthPercentageToDP,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
class LoadingScreen extends PureComponent {
  constructor() {
    super();
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem(constants.SKIP_INTRO);
    clearTimeout(this.timeoutSkip);
    this.timeoutSkip = setTimeout(() => {
      this.props.navigation.navigate(
        userToken && userToken === 'true' ? 'AppIndex' : 'Introduction',
      );
    }, 500);
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={icons.LOGO}
          style={{
            width: 300,
            height: 56,
            alignSelf: 'center',
            marginTop: heightPercentageToDP(30),
          }}
          resizeMode="contain"
          resizeMethod="scale"
        />
        <BallIndicator color={colors.white} count={10} />
      </View>
    );
  }
}

const SwitchStack = createSwitchNavigator(
  {
    Loading: LoadingScreen,
    Introduction: {
      screen: IntroductionScreen,
      navigationOptions: {header: null},
    },
    AppIndex: AppIndex,
  },
  {
    initialRouteName: 'Loading',
  },
);
export default createAppContainer(SwitchStack);
