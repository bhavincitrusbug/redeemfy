import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import ForgotPasswordScreen from './ForgotPasswordScreen';
import HomeIndex from './hometabs';
import EditBankDetailScreen from './EditBankDetailScreen';
import RewardDetailScreen from './RewardDetailScreen';
import MarketDetailScreen from './MarketDetailScreen';
import NotificationScreen from './NotificationScreen';
import LoginPopUpScreen from './LoginPopUpScreen';
import BagTabScreen from './BagTabScreen';
import BagDetailScreen from './BagDetailScreen';
import SellSuccessScreen from './SellSuccessScreen';
import ClaimSuccessScreen from './ClaimSuccessScreen';
import VerifyPhoneScreen from './VerifyPhoneScreen';
import TermsConditionScreen from './TermsConditionScreen';
import CashoutRequestScreen from './CashoutRequestScreen';
import LoginRegisterScreen from './LoginRegisterScreen';
import BuySuccessScreen from './BuySuccessScreen';
import ResetPasswordScreen from './ResetPasswordScreen';
import RedeemSuccessScreen from './RedeemSuccessScreen';
import EditProfileScreen from './EditProfileScreen';
import ChangePasswordScreen from './ChangePasswordScreen';
import WalletScreen from './WalletScreen';
import WalletViewMoreScreen from './WalletViewMoreScreen';
import PrivacyPolicyScreen from './PrivacyPolicyScreen';
import CashoutSuccessScreen from './CashoutSuccessScreen';
import EditVerifyPhoneScreen from './EditVerifyPhoneScreen';
import TransacationDetailScreen from './TransacationDetailScreen';
import TransactionSearchScreen from './TransactionSearchScreen';
import BuyPreviewScreen from './BuyPreviewScreen';
const AppStack = createStackNavigator(
  {
    LoginRegister: {
      screen: LoginRegisterScreen,
      navigationOptions: {header: null},
    },
    LoginPopUp: {
      screen: LoginPopUpScreen,
      navigationOptions: {header: null},
    },
    VerifyPhone: {
      screen: VerifyPhoneScreen,
      navigationOptions: {header: null},
    },
    ForgotPassword: {
      screen: ForgotPasswordScreen,
      navigationOptions: {header: null},
    },
    ResetPassword: {
      screen: ResetPasswordScreen,
      navigationOptions: {header: null},
    },
    ChangePassword: {
      screen: ChangePasswordScreen,
      navigationOptions: {header: null},
    },
    Home: {
      screen: HomeIndex,
      navigationOptions: {header: null},
    },
    EditBankDetail: {
      screen: EditBankDetailScreen,
      navigationOptions: {header: null},
    },
    RewardDetail: {
      screen: RewardDetailScreen,
      navigationOptions: {header: null},
    },
    MarketDetail: {
      screen: MarketDetailScreen,
      navigationOptions: {header: null},
    },
    Notification: {
      screen: NotificationScreen,
      navigationOptions: {header: null},
    },
    BagTab: {
      screen: BagTabScreen,
      navigationOptions: {header: null},
    },
    BagDetail: {
      screen: BagDetailScreen,
      navigationOptions: {header: null},
    },
    SellSuccess: {
      screen: SellSuccessScreen,
      navigationOptions: {header: null},
    },
    ClaimSuccess: {
      screen: ClaimSuccessScreen,
      navigationOptions: {header: null},
    },
    RedeemSuccess: {
      screen: RedeemSuccessScreen,
      navigationOptions: {header: null},
    },
    TermsCondition: {
      screen: TermsConditionScreen,
      navigationOptions: {header: null},
    },
    PrivacyPolicy: {
      screen: PrivacyPolicyScreen,
      navigationOptions: {header: null},
    },
    CashoutRequest: {
      screen: CashoutRequestScreen,
      navigationOptions: {header: null},
    },
    BuySuccess: {
      screen: BuySuccessScreen,
      navigationOptions: {header: null},
    },
    EditProfile: {
      screen: EditProfileScreen,
      navigationOptions: {header: null},
    },
    Wallet: {
      screen: WalletScreen,
      navigationOptions: {header: null},
    },
    WalletViewMore: {
      screen: WalletViewMoreScreen,
      navigationOptions: {header: null},
    },
    CashoutSuccess: {
      screen: CashoutSuccessScreen,
      navigationOptions: {header: null},
    },
    EditVerifyPhone: {
      screen: EditVerifyPhoneScreen,
      navigationOptions: {header: null},
    },
    TransacationDetail: {
      screen: TransacationDetailScreen,
      navigationOptions: {header: null},
    },
    TransactionSearch: {
      screen: TransactionSearchScreen,
      navigationOptions: {header: null},
    },
    BuyPreview: {
      screen: BuyPreviewScreen,
      navigationOptions: {header: null},
    },
  },
  {
    initialRouteName: 'LoginRegister',
    headerMode: 'screen',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

const AppIndex = createAppContainer(AppStack);
export default AppIndex;
