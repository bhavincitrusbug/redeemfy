import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {View, Text, Image, TouchableOpacity, ScrollView} from 'react-native';
import colors from '../resources/colors';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import fonts from '../resources/fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ButtonGradient from '../components/buttons/ButtonGradient';
import {getLangValue, getErrorValue} from '../resources/languages/language';
import strings from '../resources/strings';
import RewardDetailStyle from '../resources/styles/RewardDetailStyle';
import {showErrorMessage, generateKey} from '../resources/validation';
import {
  doGetRewardDetail,
  doPostRewardDetail,
  doUpdateCouponList,
} from '../redux/actions/AppActions';
import Loader from '../components/progress/Loader';
import NetInfo from '@react-native-community/netinfo';
import errors from '../resources/errors';
import constants from '../resources/constants';
import AsyncStorage from '@react-native-community/async-storage';
import LoginAlert from '../components/alert/LoginAlert';

class RewardDetailScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentUser: props.currentUser,
      currentLang: props.currentLang,
      rewardId: 0,
      imageUrl: '',
      title: '--',
      description: '--',
      price: '--',
      terms: '--',
      rewardStatus: 0,
      type: 0,
      isLoginAlert: false,
      is_redeem: false
    };
    this._isMounted = false;
  }
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  doClaimNow = () => {
    AsyncStorage.getItem(constants.IS_LOGIN).then(value => {
      if (value && value === 'true') {
        this.setState({isLoginAlert: false});
        const {isConnected, rewardId, currentUser} = this.state;
        if (isConnected) {
          const params = {
            apiToken: currentUser !== undefined ? currentUser.token : '',
            rewardId: rewardId,
            status: constants.CLAIMED,
            user: currentUser !== undefined ? currentUser.id : '',
          };
          this.props.doPostRewardDetail(params).catch(error => {
            showErrorMessage(error.message);
          });
        } else {
          showErrorMessage(
            getErrorValue(
              errors.NO_INTERNET_CONNECTION,
              this.state.currentLang,
            ),
          );
        }
      } else {
        this.setState({isLoginAlert: true});
      }
    });
  };
  getRewardDetail() {
    const params = {
      apiToken:
        this.state.currentUser !== undefined
          ? this.state.currentUser.token
          : '',
      rewardId: this.state.rewardId,
    };
    this.props.doGetRewardDetail(params).catch(error => {
      showErrorMessage(error.message);
    });
  }
  _handleConnectivityChange = state => {
    if (this._isMounted) {
      this.setState({
        isConnected: state.isConnected,
      });
    }
  };
  componentDidMount() {
    this._isMounted = true;
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    const {item, type} = this.props.navigation.state.params;
    if (item) {
      this.setState({rewardId: item.id, type: type}, () => {
        this.getRewardDetail();
      });
    }
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.currentUser !== prevProps.currentUser) {
      this.setState({currentUser: this.props.currentUser}, () => {
        this.getRewardDetail();
      });
    }
    if (this.props.responseRewardDetail !== prevProps.responseRewardDetail) {
      if (this.props.responseRewardDetail !== undefined) {
        const {code, status, message} = this.props.responseRewardDetail;
        switch (code) {
          case 200:
            const {result} = this.props.responseRewardDetail;
            const {data} = result;
            this.setState({
              imageUrl: data.image,
              title: data.name,
              description: data.description,
              terms: data.terms,
              rewardStatus: data.status,
              price: data.price,
              is_redeem: data.is_redeem
            });
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
    if (this.props.responseRewardClaim !== prevProps.responseRewardClaim) {
      if (this.props.responseRewardDetail !== undefined) {
        const {code, status, message, result} = this.props.responseRewardClaim;
        switch (code) {
          case 200:
            if (status) {
              const {data} = result;
              this.props.doUpdateCouponList({
                enable: true,
                key: generateKey(5),
              });
              this.props.navigation.navigate('ClaimSuccess', {data: data});
            }
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
    this._subscription();
  }
  renderButtons() {
    const {type} = this.state;
    if (type === 0) {
      return (
        <View style={RewardDetailStyle.viewButtonClaim}>
          <ButtonGradient
            title={getLangValue(strings.BTN_CLAIM_NOW, this.state.currentLang)}
            backgroundColor={colors.orange}
            fontFamily={fonts.Dosis_Bold}
            fontSize={RFPercentage(3)}
            onPress={() => this.doClaimNow()}
            width={wp(100)}
            height={hp(10)}
            currentLang={this.state.currentLang}
            borderRadius={0}
          />
        </View>
      );
    } else {
      return undefined;
    }
  }
  render() {
    return (
      <View style={RewardDetailStyle.container}>
        <View style={RewardDetailStyle.viewHeader}>
          <TouchableOpacity onPress={() => this.doBackClick()}>
            <Image source={icons.BACK} style={RewardDetailStyle.imgBack} />
          </TouchableOpacity>
        </View>
        <ScrollView
          style={{flex: 1, marginBottom: this.state.type === 0 ? hp(10) : 0}}>
          <View style={RewardDetailStyle.viewMain}>
            <View style={RewardDetailStyle.viewRewardImg}>
              <Image
                source={{uri: this.state.imageUrl}}
                style={RewardDetailStyle.imgReward}
                resizeMode="cover"
              />
            </View>
            <Text style={RewardDetailStyle.textTitle}>{this.state.title}</Text>
            {this.state.is_redeem &&
            <Text style={[RewardDetailStyle.textDescription, {opacity: 1}]}>REDEEMED</Text>
          
          }
            <Text style={RewardDetailStyle.textDescription}>
              {`$${this.state.price}`}
            </Text>
            <Text style={RewardDetailStyle.textTerms}>
              {this.state.description}
            </Text>
            <Text style={RewardDetailStyle.textTerms}>{this.state.terms}</Text>
          </View>
        </ScrollView>
        {this.renderButtons()}

        {this.props.isBusyRewardDetail || this.props.isBusyRewardClaim ? (
          <Loader />
        ) : (
          undefined
        )}
        {this.state.isLoginAlert ? (
          <LoginAlert
            isLoginAlert={this.state.isLoginAlert}
            navigation={this.props.navigation}
            doNoClick={value => {
              this.setState({isLoginAlert: value});
            }}
          />
        ) : (
          undefined
        )}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    isBusyRewardDetail: state.app.isBusyRewardDetail,
    responseRewardDetail: state.app.responseRewardDetail,
    errorRewardDetail: state.app.errorRewardDetail,
    isBusyRewardClaim: state.app.isBusyRewardClaim,
    responseRewardClaim: state.app.responseRewardClaim,
    errorRewardClaim: state.app.errorRewardClaim,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {doGetRewardDetail, doPostRewardDetail, doUpdateCouponList},
      dispatch,
    ),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RewardDetailScreen);
