import React, {PureComponent} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  BackHandler,
} from 'react-native';
import colors from '../resources/colors';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import fonts from '../resources/fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ButtonGradient from '../components/buttons/ButtonGradient';
import {getLangValue} from '../resources/languages/language';
import strings from '../resources/strings';
import CashoutSuccessStyle from '../resources/styles/CashoutSuccessStyle';
class CashoutSuccessScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      message: 'Your request has been sent for Cashout',
      description: '',
    };
  }
  doBackClick = () => {
    this.props.navigation.replace('Home');
  };
  handleBackPress = () => {
    this.doBackClick(); // works best when the goBack is async
    return true;
  };
  componentDidMount() {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackPress,
    );
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }
  render() {
    return (
      <View style={CashoutSuccessStyle.container}>
        <View style={CashoutSuccessStyle.viewHeader}>
          <TouchableOpacity onPress={() => this.doBackClick()}>
            <Image source={icons.BACK} style={CashoutSuccessStyle.imgBack} />
          </TouchableOpacity>
        </View>
        <ScrollView>
          <View style={CashoutSuccessStyle.viewMain}>
            <Text style={CashoutSuccessStyle.textTitle}>
              {this.state.message}
            </Text>
            <Text style={CashoutSuccessStyle.textDescription}>
              {this.state.description}
            </Text>
            <View style={CashoutSuccessStyle.viewRewardImg}>
              <Image
                source={icons.HOURGLASS}
                style={CashoutSuccessStyle.imgReward}
                resizeMode="contain"
              />
            </View>
          </View>
        </ScrollView>
        <View style={CashoutSuccessStyle.viewButtonClaim}>
          <ButtonGradient
            title={getLangValue(
              strings.BTN_BACK_TO_HOME,
              this.state.currentLang,
            )}
            backgroundColor={colors.orange}
            fontFamily={fonts.Dosis_Bold}
            fontSize={RFPercentage(3)}
            onPress={() => this.doBackClick()}
            width={wp(100)}
            height={hp(10)}
            currentLang={this.state.currentLang}
            borderRadius={0}
          />
        </View>
      </View>
    );
  }
}
export default CashoutSuccessScreen;
