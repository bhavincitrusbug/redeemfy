import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  FlatList,
  ScrollView,
} from 'react-native';
import strings from '../resources/strings';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {getLangValue} from '../resources/languages/language';
import WalletStyle from '../resources/styles/WalletStyle';
import TrasacationListItem from '../components/list/TrasacationListItem';
import {
  doGetTransacations,
  doGetCashoutRequests,
  doGetSearchHistory,
  doSetSearchOptions,
} from '../redux/actions/AppActions';
import {showErrorMessage} from '../resources/validation';
import NetInfo from '@react-native-community/netinfo';
import Loader from '../components/progress/Loader';
import moment from 'moment';
import WalletTab from '../components/tabs/WalletTab';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../resources/constants';
_isMounted = false;
class WalletScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentLang: props.currentLang,
      currentUser: props.currentUser,
      balanceValue: '0',
      earningValue: '0',
      spendValue: '0',

      type: getLangValue(strings.TRANSACTIONS, props.currentLang),
      transacation_page: 1,
      transacation_last_page: 1,
      transacation_refreshing: false,
      transacation_view_empty: false,
      cashout_page: 1,
      cashout_last_page: 1,
      cashout_refreshing: false,
      cashout_view_empty: false,
      transacationListData: [],
      cashoutListData: [],
    };
  }
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  doSearchClick = () => {
    this.props.navigation.navigate('TransactionSearch', {
      type: this.state.type,
    });
  };

  doOpenTransactionDetail = item => {
    this.props.navigation.navigate('TransacationDetail', {item: item});
  };

  onRefresh() {
    if (
      this.state.type ===
      getLangValue(strings.TRANSACTIONS, this.state.currentLang)
    ) {
      this.setState(
        {transacation_page: 1, transacation_refreshing: true},
        () => {
          this.getTransacationList(
            getLangValue(strings.TRANSACTIONS, this.state.currentLang),
          );
        },
      );
    } else {
      this.setState({cashout_page: 1, cashout_refreshing: true}, () => {
        this.getTransacationList(
          getLangValue(strings.CASHOUTS, this.state.currentLang),
        );
      });
    }
  }
  handleLoadMore() {
    if (
      this.state.type ===
      getLangValue(strings.TRANSACTIONS, this.state.currentLang)
    ) {
      if (this.state.transacation_page < this.state.transacation_last_page) {
        this.setState(
          (prevState, nextProps) => ({
            transacation_page: prevState.transacation_page + 1,
          }),
          () => {
            const {enable} = this.props.searchOptions;
            if (enable) {
              this.getSearchTransacationList(
                this.state.type,
                this.props.searchOptions,
              );
            } else {
              this.getTransacationList(
                getLangValue(strings.TRANSACTIONS, this.state.currentLang),
              );
            }
          },
        );
      }
    } else {
      if (this.state.cashout_page < this.state.cashout_last_page) {
        this.setState(
          (prevState, nextProps) => ({
            cashout_last_page: prevState.cashout_last_page + 1,
          }),
          () => {
            const {enable} = this.props.searchOptions;
            if (enable) {
              this.getSearchTransacationList(
                this.state.type,
                this.props.searchOptions,
              );
            } else {
              this.getTransacationList(
                getLangValue(strings.CASHOUTS, this.state.currentLang),
              );
            }
          },
        );
      }
    }
  }
  renderHeader = () => {
    return (
      <View style={{marginStart: wp(5), marginEnd: wp(5)}}>
        <View style={WalletStyle.viewWallet}>
          <View style={WalletStyle.viewWalletItem}>
            <Text style={WalletStyle.textWalletLabel}>
              {getLangValue(strings.BALALNCE, this.state.currentLang)}
            </Text>
            <Text style={WalletStyle.textWalletValue}>
              ${this.state.balanceValue}
            </Text>
          </View>
          <View style={WalletStyle.viewWalletItemBorder}>
            <Text style={WalletStyle.textWalletLabel}>
              {getLangValue(strings.EARNING, this.state.currentLang)}
            </Text>
            <Text style={WalletStyle.textWalletValue}>
              +${this.state.earningValue}
            </Text>
          </View>
          <View style={WalletStyle.viewWalletItem}>
            <Text style={WalletStyle.textWalletLabel}>
              {getLangValue(strings.SPEND, this.state.currentLang)}
            </Text>
            <Text style={WalletStyle.textWalletValue}>
              -${this.state.spendValue}
            </Text>
          </View>
        </View>
        <View style={WalletStyle.rowCenter}>
          <Text style={WalletStyle.textTransacationHistory}>
            {getLangValue(strings.TRANSACTIONS_HISTORY, this.state.currentLang)}
          </Text>
          <View style={WalletStyle.viewSearch}>
            <TouchableOpacity onPress={() => this.doSearchClick()}>
              <Image source={icons.SEARCH} style={WalletStyle.imgSearch} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };
  renderEmpty = () => {
    return (
      <View style={WalletStyle.viewEmptyMain}>
        <Image
          resizeMode="contain"
          source={icons.NO_TRANSACTIONS}
          style={WalletStyle.imgNoReward}
        />
        <Text style={WalletStyle.textNoRewards}>
          {getLangValue(
            strings.NO_TRANSACATIONS_HISTORY,
            this.state.currentLang,
          )}
        </Text>
      </View>
    );
  };
  renderEmptyRequests = () => {
    return (
      <View style={WalletStyle.viewEmptyMain}>
        <Image
          resizeMode="contain"
          source={icons.NO_CASHOUTS}
          style={WalletStyle.imgNoReward}
        />
        <Text style={WalletStyle.textNoRewards}>
          {getLangValue(strings.NO_CASHOUT_REQUESTS, this.state.currentLang)}
        </Text>
      </View>
    );
  };
  getTransacationList(title) {
    this.setState({type: title}, () => {
      if (
        title === getLangValue(strings.TRANSACTIONS, this.state.currentLang)
      ) {
        const params = {
          apiToken:
            this.state.currentUser !== undefined
              ? this.state.currentUser.token
              : '',
          userId:
            this.state.currentUser !== undefined
              ? this.state.currentUser.id
              : '0',
          page: this.state.transacation_page,
          status: '',
        };
        this.props.doGetTransacations(params);
      } else {
        const params = {
          apiToken:
            this.state.currentUser !== undefined
              ? this.state.currentUser.token
              : '',
          userId:
            this.state.currentUser !== undefined
              ? this.state.currentUser.id
              : '0',
          page: this.state.cashout_page,
        };
        this.props.doGetCashoutRequests(params);
      }
    });
  }
  getSearchTransacationList(title, searchOptions) {
    this.setState({type: title}, () => {
      if (
        title === getLangValue(strings.TRANSACTIONS, this.state.currentLang)
      ) {
        const user = {
          apiToken:
            this.state.currentUser !== undefined
              ? this.state.currentUser.token
              : '',
          userId:
            this.state.currentUser !== undefined
              ? this.state.currentUser.id
              : '0',
        };
        const params = {
          page: this.state.transacation_page,
          created_at__gte: searchOptions.fromDate,
          created_at__lte: searchOptions.toDate,
          coupons_id__name__icontains: searchOptions.keyword,
          payment_types: searchOptions.transacation_type,
          amount__gte: searchOptions.minValue,
          amount__lte: searchOptions.maxValue,
          user:
            this.state.currentUser !== undefined
              ? this.state.currentUser.id
              : '0',
        };
        this.props.doGetSearchHistory(params, user);
      } else {
        const user = {
          apiToken:
            this.state.currentUser !== undefined
              ? this.state.currentUser.token
              : '',
          userId:
            this.state.currentUser !== undefined
              ? this.state.currentUser.id
              : '0',
        };
        const params = {
          page: this.state.cashout_page,
          created_at__gte: searchOptions.fromDate,
          created_at__lte: searchOptions.toDate,
          status: searchOptions.sort_by,
          amount__gte: searchOptions.minValue,
          amount__lte: searchOptions.maxValue,
          user:
            this.state.currentUser !== undefined
              ? this.state.currentUser.id
              : '0',
        };
        this.props.doGetSearchHistory(params, user);
      }
    });
  }
  renderTabOne() {
    return (
      <View>
        <View style={{marginTop: hp(2)}}>
          <FlatList
            key={4112130}
            data={this.state.transacationListData}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => this.doOpenTransactionDetail(item)}>
                <TrasacationListItem
                  image={item.coupons_id.image}
                  title={item.coupons_id.name}
                  date={moment(
                    item.created_date,
                    'YYYY-MM-DD  HH:mm:ss',
                  ).format('Do MMM, YYYY hh:mm A')}
                  price={item.amount}
                  type={''}
                  payment_types={item.payment_types}
                  currentLang={this.state.currentLang}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index.toString()}
            refreshing={this.state.transacation_refreshing}
            onRefresh={() => this.onRefresh()}
            onEndReached={() => this.handleLoadMore()}
            onEndReachedThreshold={15}
            ListEmptyComponent={
              this.state.transacation_view_empty ? this.renderEmpty : null
            }
          />
        </View>
      </View>
    );
  }
  renderTabTwo() {
    return (
      <View>
        <View style={{marginTop: hp(2)}}>
          <FlatList
            key={4112135}
            data={this.state.cashoutListData}
            renderItem={({item, index}) => (
              <TouchableOpacity disabled={true}>
                <TrasacationListItem
                  title={`#RQ${item.id}`}
                  date={moment(
                    item.created_date,
                    'YYYY-MM-DD  HH:mm:ss',
                  ).format('Do MMM, YYYY hh:mm A')}
                  price={item.amount}
                  type={'cashout'}
                  transacationType={item.status}
                  currentLang={this.state.currentLang}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index.toString()}
            refreshing={this.state.cashout_refreshing}
            onRefresh={() => this.onRefresh()}
            onEndReached={() => this.handleLoadMore()}
            onEndReachedThreshold={15}
            ListEmptyComponent={
              this.state.cashout_view_empty ? this.renderEmptyRequests() : null
            }
          />
        </View>
      </View>
    );
  }
  _handleConnectivityChange = state => {
    if (this._isMounted) {
      this.setState({
        isConnected: state.isConnected,
      });
    }
  };
  componentDidMount() {
    this._isMounted = true;
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    this.getTransacationList(this.state.type);
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.searchOptions !== prevProps.searchOptions) {
      const {enable} = this.props.searchOptions;
      if (enable) {
        this.getSearchTransacationList(
          this.state.type,
          this.props.searchOptions,
        );
      }
    }

    if (this.props.responseTransactions !== prevProps.responseTransactions) {
      if (this.props.responseTransactions !== undefined) {
        const {detail} = this.props.responseTransactions;
        if (detail === undefined) {
          const {code, status, message} = this.props.responseTransactions;
          switch (code) {
            case 200:
              const {
                data,
                links,
                wallet,
              } = this.props.responseTransactions.result;
              const {last, current} = links;
              this.setState({
                balanceValue: wallet.amount,
                earningValue: wallet.earn,
                spendValue: wallet.spend,
              });
              if (current === 1) {
                this.setState({
                  transacationListData: data,
                  transacation_refreshing: false,
                  transacation_last_page: last,
                  transacation_view_empty: data.length <= 0,
                });
              } else {
                this.setState({
                  transacationListData: [
                    ...this.state.transacationListData,
                    ...data,
                  ],
                  transacation_refreshing: false,
                  transacation_last_page: last,
                  transacation_view_empty: false,
                });
              }
              break;
            case 400:
              this.setState({
                transacationListData: [],
                transacation_refreshing: false,
                transacation_last_page: 1,
                transacation_view_empty: true,
              });
              break;
            default:
              showErrorMessage(message);
              break;
          }
        } else {
          if (detail.trim() === '') {
          } else {
            showErrorMessage(detail);
          }

          AsyncStorage.removeItem(constants.USER);
          AsyncStorage.removeItem(constants.IS_LOGIN);
          AsyncStorage.clear();
          this.props.doSetUser(undefined);
          this.props.navigation.replace('LoginRegister');
        }
      }
    }
    if (
      this.props.responseCashoutRequest !== prevProps.responseCashoutRequest
    ) {
      if (this.props.responseCashoutRequest !== undefined) {
        const {detail} = this.props.responseCashoutRequest;
        if (detail === undefined) {
          const {code, status, message} = this.props.responseCashoutRequest;
          switch (code) {
            case 200:
              const {
                data,
                links,
                wallet,
              } = this.props.responseCashoutRequest.result;
              const {current, last} = links;

              this.setState({balanceValue: wallet.amount});
              if (current === 1) {
                this.setState({
                  cashoutListData: data,
                  cashout_refreshing: false,
                  cashout_last_page: last,
                  cashout_view_empty: data.length <= 0,
                });
              } else {
                this.setState({
                  cashoutListData: [...this.state.cashoutListData, ...data],
                  cashout_refreshing: false,
                  cashout_last_page: last,
                  cashout_view_empty: false,
                });
              }
              break;
            case 400:
              this.setState({
                cashoutListData: [],
                cashout_refreshing: false,
                cashout_last_page: 1,
                cashout_view_empty: true,
              });
              break;
            default:
              showErrorMessage(message);
              break;
          }
        } else {
          if (detail.trim() === '') {
          } else {
            showErrorMessage(detail);
          }
          AsyncStorage.removeItem(constants.USER);
          AsyncStorage.removeItem(constants.IS_LOGIN);
          AsyncStorage.clear();
          this.props.doSetUser(undefined);
          this.props.navigation.replace('LoginRegister');
        }
      }
    }
    if (this.props.responseSearchHistory !== prevProps.responseSearchHistory) {
      const {code, message, status} = this.props.responseSearchHistory;
      switch (code) {
        case 200:
          const {data, links} = this.props.responseSearchHistory.result;
          const {current, last} = links;
          if (current === 1) {
            if (
              this.state.type ===
              getLangValue(strings.TRANSACTIONS, this.state.currentLang)
            ) {
              this.setState({
                transacationListData: data,
                transacation_refreshing: false,
                transacation_last_page: last,
                transacation_view_empty: data.length <= 0,
              });
            } else {
              this.setState({
                cashoutListData: data,
                cashout_refreshing: false,
                cashout_last_page: last,
                cashout_view_empty: data.length <= 0,
              });
            }
          } else {
            if (
              this.state.type ===
              getLangValue(strings.TRANSACTIONS, this.state.currentLang)
            ) {
              this.setState({
                transacationListData: [
                  ...this.state.transacationListData,
                  ...data,
                ],
                transacation_refreshing: false,
                transacation_last_page: last,
                transacation_view_empty: false,
              });
            } else {
              this.setState({
                cashoutListData: [...this.state.cashoutListData, ...data],
                cashout_refreshing: false,
                cashout_last_page: last,
                cashout_view_empty: false,
              });
            }
          }
          const searchOptions = {
            enable: false,
            fromDate: '',
            toDate: '',
            keyword: '',
            transacation_type: '',
            sort_by: '',
            minValue: '',
            maxValue: '',
            key: '',
          };
          this.props.doSetSearchOptions(searchOptions);
          break;
        case 400:
          if (
            this.state.type ===
            getLangValue(strings.TRANSACTIONS, this.state.currentLang)
          ) {
            this.setState({
              transacationListData: [],
              transacation_refreshing: false,
              transacation_last_page: 1,
              transacation_view_empty: true,
            });
          } else {
            this.setState({
              cashoutListData: [],
              cashout_refreshing: false,
              cashout_last_page: 1,
              cashout_view_empty: true,
            });
          }
          break;
        default:
          showErrorMessage(message);
          break;
      }
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
    this._subscription();
  }
  render() {
    return (
      <View style={WalletStyle.container}>
        <View style={WalletStyle.viewHeader}>
          <Text style={WalletStyle.textHeader}>
            {getLangValue(strings.HEADER_MY_WALLET, this.state.currentLang)}
          </Text>
          <View style={WalletStyle.viewBack}>
            <TouchableOpacity onPress={() => this.doBackClick()}>
              <Image source={icons.BACK} style={WalletStyle.imgBack} />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView>
          <View style={{marginTop: hp(2)}}>
            {this.renderHeader()}
            <WalletTab
              tabOneTitle={getLangValue(strings.TRANSACTIONS, strings.LAG_ENG)}
              tabTwoTitle={getLangValue(strings.CASHOUTS, strings.LAG_ENG)}
              renderTabOne={() => this.renderTabOne()}
              renderTabTwo={() => this.renderTabTwo()}
              getTransacationList={title => this.getTransacationList(title)}
            />
          </View>
        </ScrollView>

        {this.props.isBusyTransactions ||
        this.props.isBusyCashoutRequest ||
        this.props.isBusySearchHistory ? (
          <Loader />
        ) : (
          undefined
        )}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    searchOptions: state.app.searchOptions,
    isBusyTransactions: state.app.isBusyTransactions,
    responseTransactions: state.app.responseTransactions,
    errorTransactions: state.app.errorTransactions,
    isBusyCashoutRequest: state.app.isBusyCashoutRequest,
    responseCashoutRequest: state.app.responseCashoutRequest,
    errorCashoutRequest: state.app.errorCashoutRequest,
    isBusySearchHistory: state.app.isBusySearchHistory,
    responseSearchHistory: state.app.responseSearchHistory,
    errorSearchHistory: state.app.errorSearchHistory,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {
        doGetTransacations,
        doGetCashoutRequests,
        doGetSearchHistory,
        doSetSearchOptions,
      },
      dispatch,
    ),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(WalletScreen);
