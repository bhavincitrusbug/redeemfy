import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Platform,
  ScrollView,
} from 'react-native';
import colors from '../resources/colors';
import fonts from '../resources/fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Reinput from 'reinput';
import NetInfo from '@react-native-community/netinfo';
import EditProfileStyle from '../resources/styles/EditProfileStyle';
import {getLangValue, getErrorValue} from '../resources/languages/language';
import strings from '../resources/strings';
import {
  validateEmail,
  showErrorMessage,
  checkOldNewPhone,
  showSuccessMessage,
} from '../resources/validation';
import errors from '../resources/errors';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../resources/constants';
import firebase from 'react-native-firebase';
import {
  doSetUser,
  doPostEditProfile,
  doPostEditOtpSend,
} from '../redux/actions/AppActions';
import ButtonGradient from '../components/buttons/ButtonGradient';
import PhoneNumberInput from '../components/texts/PhoneNumberInput';
import Loader from '../components/progress/Loader';
import PhoneNumber from 'awesome-phonenumber';

class EditProfileScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentUser: props.currentUser,
      currentLang: strings.LAG_ENG,
      firebaseToken: '',
      about_me:
        props.currentUser !== undefined ? props.currentUser.about_us : '',
      errAboutMe: undefined,
      fullName:
        props.currentUser !== undefined ? props.currentUser.full_name : '',
      errFullName: undefined,
      email: props.currentUser !== undefined ? props.currentUser.email : '',
      errEmail: undefined,
      phoneNumber:
        props.currentUser !== undefined ? props.currentUser.contact : '',
      errPhoneNumber: undefined,
      country_code: '+65',
      errCountryCode: undefined,
      contactWithoutCode: '',
      isPhoneVerified: true,
      params: undefined,
    };
  }
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  doSubmit = () => {
    const {
      isConnected,
      firebaseToken,
      currentUser,
      about_me,
      fullName,
      email,
      phoneNumber,
      isPhoneVerified,
      country_code,
      errPhoneNumber,
    } = this.state;
    this.phoneNumberInput.updateState(
      country_code,
      phoneNumber,
      errPhoneNumber,
    );
    if (fullName.trim() === '') {
      this.setState(
        {
          errFullName: getErrorValue(
            errors.ENTER_FULL_NAME,
            this.state.currentLang,
          ),
        },
        () => {
          this.fullNameInput.focus();
        },
      );
      return;
    }
    this.setState({errFullName: undefined});

    if (phoneNumber.trim() === '') {
      this.setState(
        {
          errPhoneNumber: getErrorValue(
            errors.ENTER_PHONE_NUMBER,
            this.state.currentLang,
          ),
        },
        () => {
          this.phoneNumberInput.updateState(
            country_code,
            phoneNumber,
            errPhoneNumber,
          );
          this.phoneInput.focus();
        },
      );
      return;
    }
    if (country_code.trim() === undefined) {
      this.setState(
        {
          errPhoneNumber: getErrorValue(
            errors.SELECT_COUNTRY_CODE,
            this.state.currentLang,
          ),
        },
        () => {
          this.phoneNumberInput.phoneInput.focus();
        },
      );
      return;
    }
    this.setState({
      errEmail: undefined,
      errPassword: undefined,
      errPhoneNumber: undefined,
    });

    if (isConnected) {
      if (isPhoneVerified) {
        const params = {
          apiToken: currentUser !== undefined ? currentUser.token : '',
          userId: currentUser !== undefined ? currentUser.id : '',
          about_us: about_me.trim(),
          full_name: fullName.trim(),
          email: email.trim(),
          contact:
            country_code.trim() + phoneNumber.replace(/[^0-9]/g, '').trim(),
          deviceType: Platform.OS,
          deviceToken: firebaseToken,
        };
        this.props.doPostEditProfile(params).catch(error => {
          showErrorMessage(error.message);
        });
      } else {
        try {
          const params = {
            apiToken: currentUser !== undefined ? currentUser.token : '',
            userId: currentUser !== undefined ? currentUser.id : '',
            about_us: about_me.trim(),
            full_name: fullName.trim(),
            email: email.trim(),
            contact:
              country_code.trim() + phoneNumber.replace(/[^0-9]/g, '').trim(),
            deviceType: Platform.OS,
            deviceToken: firebaseToken,
          };
          this.setState({params: params});
          this.props.doPostEditOtpSend(params).catch(error => {
            showErrorMessage(error.message);
          });
        } catch (error) {
          showErrorMessage(error.message);
        }
      }
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  };
  onNumberChange = text => {
    this.setState({contactWithoutCode: text}, () => {
      if (this.state.currentUser !== undefined) {
        if (this.state.currentUser.contact !== '') {
          this.setState({
            isPhoneVerified: checkOldNewPhone(
              this.state.contactWithoutCode,
              this.state.currentUser.contact,
            ),
          });
        }
      }
    });
  };
  async getToken() {
    let fcmToken = await firebase.messaging().getToken();
    this.setState({firebaseToken: fcmToken});
  }
  _handleConnectivityChange = state => {
    if (this._isMounted) {
      this.setState({
        isConnected: state.isConnected,
      });
    }
  };
  componentDidMount() {
    this._isMounted = true;
    this.getToken();
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    if (this.state.phoneNumber !== '') {
      let number = new PhoneNumber(this.state.phoneNumber);
      this.phoneNumberInput.updateState(
        '+' + number.getCountryCode(),
        this.state.phoneNumber
          .replace('+', '')
          .replace(number.getCountryCode(), ''),
        this.state.errPhoneNumber,
      );
    }
  }

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.currentUser !== prevProps.currentUser) {
      this.setState({
        currentUser: this.props.currentUser,
        about_me:
          this.props.currentUser !== undefined
            ? this.props.currentUser.about_us
            : '',
        fullName:
          this.props.currentUser !== undefined
            ? this.props.currentUser.full_name
            : '',
        email:
          this.props.currentUser !== undefined
            ? this.props.currentUser.email
            : '',
        phoneNumber:
          this.props.currentUser !== undefined
            ? this.props.currentUser.contact
            : '',
        isPhoneVerified: checkOldNewPhone(
          this.state.contactWithoutCode,
          this.props.currentUser !== undefined
            ? this.props.currentUser.contact
            : '',
        ),
      });
    }
    if (this.props.responseEditProfile !== prevProps.responseEditProfile) {
      if (this.props.responseEditProfile !== undefined) {
        const {code, status, message} = this.props.responseEditProfile;
        switch (code) {
          case 200:
            if (status) {
              const {data} = this.props.responseEditProfile.result;
              this.props.doSetUser(data);
              AsyncStorage.setItem(constants.USER, JSON.stringify(data));
              showSuccessMessage(message);
              this.doBackClick();
            } else {
              showSuccessMessage(message);
            }

            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
    if (this.props.responseEditSendOtp !== prevProps.responseEditSendOtp) {
      if (this.props.responseEditSendOtp !== undefined) {
        const {code, status, message} = this.props.responseEditSendOtp;
        switch (code) {
          case 200:
            if (status) {
              this.props.navigation.navigate('EditVerifyPhone', {
                phoneNumber: this.state.phoneNumber.replace(/[^0-9]/g, ''),
                userData: this.state.params,
              });
            } else {
              showErrorMessage(message);
            }

            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
    this._subscription();
  }
  render() {
    return (
      <View style={EditProfileStyle.container}>
        <View style={EditProfileStyle.rowCenter}>
          <Text style={EditProfileStyle.textHeader}>
            {getLangValue(strings.HEADER_EDIT_PROFILE, this.state.currentLang)}
          </Text>
          <View style={EditProfileStyle.viewBack}>
            <TouchableOpacity onPress={() => this.doBackClick()}>
              <Image source={icons.BACK} style={EditProfileStyle.imgBack} />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView>
          <View
            style={{marginStart: wp(5), marginEnd: wp(5), marginTop: hp(2)}}>
            <Reinput
              ref={input => (this.fullNameInput = input)}
              label={getLangValue(strings.FULL_NAME, this.state.currentLang)}
              labelColor={colors.colorLabelInActive}
              labelActiveColor={colors.colorLabelActive}
              color={colors.white}
              activeColor={colors.white}
              fontFamily={fonts.Dosis_Regular}
              value={this.state.fullName}
              onChangeText={text => {
                this.setState({fullName: text}, () => {
                  this.state.fullName === ''
                    ? this.setState({
                        errFullName: getErrorValue(
                          errors.ENTER_FULL_NAME,
                          this.state.currentLang,
                        ),
                      })
                    : this.setState({errFullName: undefined});
                });
              }}
              error={this.state.errFullName}
              fontSize={RFPercentage(3)}
              underlineColor={colors.colorLine}
              returnKeyType="next"
              onSubmitEditing={() => {
                this.state.fullName.length > 0
                  ? this.emailInput.focus()
                  : this.fullNameInput.focus();
              }}
            />
            <View
              style={{
                flexDirection: 'row',marginTop: Platform.OS === 'ios' ? hp(1) : hp(1)
              }}>
              <View style={{flex: 1}}>
                <Reinput
                  ref={input => (this.emailInput = input)}
                  label={getLangValue(strings.EMAIL, this.state.currentLang)}
                  labelColor={colors.colorLabelInActive}
                  labelActiveColor={colors.colorLabelActive}
                  color={colors.white}
                  activeColor={colors.white}
                  fontFamily={fonts.Dosis_Regular}
                  value={this.state.email}
                  editable={false}
                  onChangeText={text => {
                    this.setState({email: text}, () => {
                      this.state.email === ''
                        ? this.setState({
                            errEmail: getErrorValue(
                              errors.ENTER_EMAIL,
                              this.state.currentLang,
                            ),
                          })
                        : !validateEmail(this.state.email)
                        ? this.setState({
                            errEmail: getErrorValue(
                              errors.ENTER_VALID_EMAIL,
                              this.state.currentLang,
                            ),
                          })
                        : this.setState({errEmail: undefined});
                    });
                  }}
                  error={this.state.errEmail}
                  fontSize={RFPercentage(3)}
                  underlineColor={colors.colorLine}
                  keyboardType="email-address"
                  autoCapitalize="none"
                  returnKeyType="next"
                  onSubmitEditing={() => {
                    this.phoneInput.focus();
                  }}
                />
              </View>
              <View
                style={{
                  position: 'absolute',
                  end: 0,
                  top: hp(2),
                  marginBottom: 5,
                }}>
                <Image
                  resizeMode="contain"
                  source={icons.VERIFIED}
                  style={{height: hp(5), width: wp(5)}}
                />
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flex: 1,
                  marginTop: Platform.OS === 'ios' ? hp(2) : hp(1),
                }}>
                <PhoneNumberInput
                  ref={input => (this.phoneNumberInput = input)}
                  onNumberChange={text => this.onNumberChange(text)}
                  currentLang={this.state.currentLang}
                  label={getLangValue(
                    strings.PHONE_NUMBER,
                    this.state.currentLang,
                  )}
                  labelColor={colors.colorLabelInActive}
                  onComplete={(
                    country_code,
                    phoneNumber,
                    errPhoneNumber,
                    phoneInput,
                  ) => {
                    this.phoneInput = phoneInput;
                    this.setState({
                      country_code: country_code,
                      phoneNumber: phoneNumber,
                      errPhoneNumber: errPhoneNumber,
                    });
                  }}
                />
              </View>
              <View
                style={{
                  position: 'absolute',
                  end: 0,
                  top: hp(2),
                }}>
                <Image
                  resizeMode="contain"
                  source={icons.VERIFIED}
                  style={{height: hp(5), width: wp(5)}}
                />
              </View>
            </View>

            <Reinput
              ref={input => (this.aboutInput = input)}
              label={getLangValue(strings.ABOUT_ME, this.state.currentLang)}
              labelColor={colors.colorLabelInActive}
              labelActiveColor={colors.colorLabelActive}
              color={colors.white}
              activeColor={colors.white}
              fontFamily={fonts.Dosis_Regular}
              value={this.state.about_me}
              onChangeText={text => {
                this.setState({about_me: text});
              }}
              error={this.state.errAboutMe}
              fontSize={RFPercentage(3)}
              underlineColor={colors.colorLine}
              returnKeyType="done"
              multiline={true}
              numberOfLines={3}
            />
            <View style={EditProfileStyle.viewButton}>
              <ButtonGradient
                title={
                  this.state.isPhoneVerified
                    ? getLangValue(strings.BTN_SAVE, this.state.currentLang)
                    : getLangValue(
                        strings.BTN_VERIFY_NUMBER,
                        this.state.currentLang,
                      )
                }
                backgroundColor={colors.orange}
                fontFamily={fonts.Dosis_Bold}
                fontSize={RFPercentage(3)}
                onPress={() => this.doSubmit()}
                width={wp(80)}
                height={hp(6)}
                currentLang={this.state.currentLang}
                borderRadius={23}
              />
            </View>
          </View>
        </ScrollView>
        {this.props.isBusyEditProfile || this.props.isBusyEditSendOtp ? (
          <Loader />
        ) : (
          undefined
        )}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    isBusyEditProfile: state.app.isBusyEditProfile,
    responseEditProfile: state.app.responseEditProfile,
    errorEditProfile: state.app.errorEditProfile,
    isBusyEditSendOtp: state.app.isBusyEditSendOtp,
    responseEditSendOtp: state.app.responseEditSendOtp,
    errorEditSendOtp: state.app.errorEditSendOtp,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {doSetUser, doPostEditProfile, doPostEditOtpSend},
      dispatch,
    ),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(EditProfileScreen);
