import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {View, Text, Image, TouchableOpacity, ScrollView} from 'react-native';
import colors from '../resources/colors';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import fonts from '../resources/fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ButtonGradient from '../components/buttons/ButtonGradient';
import {getLangValue, getErrorValue} from '../resources/languages/language';
import strings from '../resources/strings';
import MarketDetailStyle from '../resources/styles/MarketDetailStyle';
import {
  doGetMarketDetail,
  doUpdateMarketList,
  doPostBuyCoupon,
} from '../redux/actions/AppActions';
import Loader from '../components/progress/Loader';
import {showErrorMessage, generateKey} from '../resources/validation';
import stripe from 'tipsi-stripe';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../resources/constants';
import NetInfo from '@react-native-community/netinfo';
import errors from '../resources/errors';
import LoginAlert from '../components/alert/LoginAlert';

stripe.setOptions({
  publishableKey: constants.STRIPE_KEY,
});
class MarketDetailScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentUser: props.currentUser,
      currentLang: props.currentLang,
      coupons_id: 0,
      marketId: 0,
      codeId: 0,
      imageUrl: '',
      title: '--',
      description: '--',
      price: '--',
      terms: '--',
      loading: false,
      isLoginAlert: false,
      is_user: false,
      token: undefined,
      couponData: undefined,
      walletData: undefined,
      is_redeem: false,
    };
  }
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  doBuyNow = () => {
    AsyncStorage.getItem(constants.IS_LOGIN).then(value => {
      if (value && value === 'true') {
        this.setState({isLoginAlert: false});
        // this.props.navigation.navigate('CardTextField');
        if (this.state.isConnected) {
          const params = {
            user:
              this.state.currentUser !== undefined
                ? this.state.currentUser.id
                : '',
            coupon_name: this.state.title,
            amount: this.state.price,
            customer_token: '',
            payment_types: 'buy',
            coupon_quantity: 1,
            coupons_id: this.state.coupons_id,
            apiToken:
              this.state.currentUser !== undefined
                ? this.state.currentUser.token
                : '',
            marketId: this.state.marketId,
            code_id: this.state.codeId,
            coupon_flag: 0,
            userId:
              this.state.currentUser !== undefined
                ? this.state.currentUser.id
                : '',
          };
          this.props.navigation.navigate('BuyPreview', {
            buyParams: params,
            couponData: this.state.couponData,
            walletData: this.state.walletData,
          });
        } else {
          showErrorMessage(
            getErrorValue(
              errors.NO_INTERNET_CONNECTION,
              this.state.currentLang,
            ),
          );
        }
      } else {
        this.setState({isLoginAlert: true});
      }
    });
  };
  handleCardPayPress = async () => {
    try {
      this.setState({loading: true, token: null});
      const options = {
        theme: {
          primaryBackgroundColor: colors.white,
          secondaryBackgroundColor: colors.white,
          primaryForegroundColor: colors.colorBackground,
          secondaryForegroundColor: colors.colorBackground,
          accentColor: colors.colorTabBackground,
        },
      };
      const token = await stripe.paymentRequestWithCardForm(options);
      if (this.state.isConnected) {
        const params = {
          user:
            this.state.currentUser !== undefined
              ? this.state.currentUser.id
              : '',
          coupon_name: this.state.title,
          amount: this.state.price,
          customer_token: token.tokenId,
          payment_types: 'buy',
          coupon_quantity: 1,
          coupons_id: this.state.coupons_id,
          apiToken:
            this.state.currentUser !== undefined
              ? this.state.currentUser.token
              : '',
          marketId: this.state.marketId,
          code_id: this.state.codeId,
          coupon_flag: 0,
          userId:
            this.state.currentUser !== undefined
              ? this.state.currentUser.id
              : '',
        };
        this.props.doPostBuyCoupon(params).catch(error => {
          showErrorMessage(error.message);
        });
      } else {
        showErrorMessage(
          getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
        );
      }

      this.setState({loading: false, token});
    } catch (error) {
      this.setState({loading: false});
    }
  };

  getMarketDetail() {
    const params = {
      apiToken:
        this.state.currentUser !== undefined
          ? this.state.currentUser.token
          : '',
      marketId: this.state.marketId,
      userId:
        this.state.currentUser !== undefined ? this.state.currentUser.id : '',
    };
    this.props.doGetMarketDetail(params).catch(error => {
      showErrorMessage(error.message);
    });
  }
  _handleConnectivityChange = state => {
    if (this._isMounted) {
      this.setState({
        isConnected: state.isConnected,
      });
    }
  };
  componentDidMount() {
    this._isMounted = true;
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    const {item} = this.props.navigation.state.params;
    if (item) {
      this.setState(
        {marketId: item.id, codeId: item.id, is_user: item.is_user},
        () => {
          this.getMarketDetail();
        },
      );
    }
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.currentUser !== prevProps.currentUser) {
      this.setState({currentUser: this.props.currentUser}, () => {
        this.getMarketDetail();
      });
    }
    if (this.props.responseMarketDetail !== prevProps.responseMarketDetail) {
      if (this.props.responseMarketDetail !== undefined) {
        const {code, status, message} = this.props.responseMarketDetail;
        switch (code) {
          case 200:
            const {result} = this.props.responseMarketDetail;
            const {data, wallet} = result;
            this.setState({
              coupons_id: data.id,
              imageUrl: data.image,
              title: data.name,
              description: data.description,
              terms: data.terms,
              rewardStatus: data.status,
              price: data.price,
              couponData: data,
              walletData: wallet,
              is_redeem: data.is_redeem
            });
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
    if (this.props.responseRewardBuy !== prevProps.responseRewardBuy) {
      if (this.props.responseRewardBuy !== undefined) {
        const {code, status, message} = this.props.responseRewardBuy;
        switch (code) {
          case 200:
            if (status) {
              this.props.doUpdateMarketList({
                enable: true,
                key: generateKey(5),
              });
              this.props.navigation.navigate('BuySuccess');
            }

            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  render() {
    return (
      <View style={MarketDetailStyle.container}>
        <View style={MarketDetailStyle.viewHeader}>
          <TouchableOpacity onPress={() => this.doBackClick()}>
            <Image source={icons.BACK} style={MarketDetailStyle.imgBack} />
          </TouchableOpacity>
        </View>
        <ScrollView style={{flex: 1, marginBottom: hp(10)}}>
          <View style={MarketDetailStyle.viewMain}>
            <View style={MarketDetailStyle.viewRewardImg}>
              <Image
                source={{uri: this.state.imageUrl}}
                style={MarketDetailStyle.imgReward}
                resizeMode="cover"
              />
            </View>
            <Text style={MarketDetailStyle.textTitle}>{this.state.title}</Text>
            {this.state.is_redeem &&
            <Text style={[MarketDetailStyle.textDescription, {opacity: 1}]}>REDEEMED</Text>
          }
            <Text style={MarketDetailStyle.textDescription}>
              {`$${this.state.price}`}
            </Text>
            <Text style={MarketDetailStyle.textTerms}>
              {this.state.description}
            </Text>
            <Text style={MarketDetailStyle.textTerms}>{this.state.terms}</Text>
            {this.state.is_user ? (
              <Text
                style={{
                  textAlign: 'center',
                  color: colors.colorError,
                  fontSize: RFPercentage(3),
                  fontFamily: fonts.Dosis_Regular,
                  marginEnd: 5,
                  marginBottom: 5,
                }}>
                {getLangValue(strings.SORRY_OWN_BUY, this.state.currentLang)}
              </Text>
            ) : null}
          </View>
        </ScrollView>
        <View style={MarketDetailStyle.viewButtonClaim}>
          <ButtonGradient
            title={getLangValue(strings.BTN_BUY_NOW, this.state.currentLang)}
            backgroundColor={colors.orange}
            fontFamily={fonts.Dosis_Bold}
            fontSize={RFPercentage(3)}
            onPress={() => this.doBuyNow()}
            width={wp(100)}
            height={hp(10)}
            currentLang={this.state.currentLang}
            borderRadius={0}
            disable={this.state.is_user}
          />
        </View>
        {this.props.isBusyMarketDetail ||
        this.state.loading ||
        this.props.isBusyRewardBuy ? (
          <Loader />
        ) : (
          undefined
        )}
        {this.state.isLoginAlert ? (
          <LoginAlert
            isLoginAlert={this.state.isLoginAlert}
            navigation={this.props.navigation}
            doNoClick={value => {
              this.setState({isLoginAlert: value});
            }}
          />
        ) : (
          undefined
        )}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    currentLang: state.app.currentLang,
    isBusyMarketDetail: state.app.isBusyMarketDetail,
    responseMarketDetail: state.app.responseMarketDetail,
    errorMarketDetail: state.app.errorMarketDetail,
    isBusyRewardBuy: state.app.isBusyRewardBuy,
    responseRewardBuy: state.app.responseRewardBuy,
    errorRewardBuy: state.app.errorRewardBuy,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {
        doGetMarketDetail,
        doUpdateMarketList,
        doPostBuyCoupon,
      },
      dispatch,
    ),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(MarketDetailScreen);
