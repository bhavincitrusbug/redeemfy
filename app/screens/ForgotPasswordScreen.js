import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {View, Text, Image, TouchableOpacity, Platform} from 'react-native';
import colors from '../resources/colors';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../resources/fonts';
import Reinput from 'reinput';
import {getErrorValue, getLangValue} from '../resources/languages/language';
import {validateEmail, showErrorMessage, showSuccessMessage} from '../resources/validation';
import errors from '../resources/errors';
import strings from '../resources/strings';
import ButtonGradient from '../components/buttons/ButtonGradient';
import ForgotPasswordStyle from '../resources/styles/ForgotPasswordStyle';
import {doForgotPassword} from '../redux/actions/AuthActions';
import Loader from '../components/progress/Loader';
import NetInfo from '@react-native-community/netinfo';
import PhoneNumberInput from '../components/texts/PhoneNumberInput';

class ForgotPasswordScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentLang: strings.LAG_ENG,
      email: '',
      errEmail: undefined,
      phoneNumber: '',
      errPhoneNumber: undefined,
      country_code: '+65',
    };
  }
  doSubmit = () => {
    const {isConnected, phoneNumber, country_code, errPhoneNumber} = this.state;

    if (phoneNumber === '') {
      this.setState(
        {
          errPhoneNumber: getErrorValue(
            errors.ENTER_PHONE_NUMBER,
            this.state.currentLang,
          ),
        },
        () => {
          this.phoneNumberInput.updateState(
            country_code,
            phoneNumber,
            errPhoneNumber,
          );
          this.phoneInput.focus();
        },
      );
      return;
    }
    if (country_code === null || country_code === undefined) {
      this.setState(
        {
          errPhoneNumber: getErrorValue(
            errors.SELECT_COUNTRY_CODE,
            this.state.currentLang,
          ),
        },
        () => {
          this.phoneNumberInput.phoneInput.focus();
        },
      );
      return;
    }
    if (isConnected) {
      const params = {
        mobile: country_code + phoneNumber.replace(/[^0-9]/g, ''),
        deviceType: Platform.OS,
      };
      this.props.doForgotPassword(params).catch(error => {
        showErrorMessage(error.message);
      });
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  };
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  _handleConnectivityChange = state => {
    this.setState({
      isConnected: state.isConnected,
    });
  };
  componentDidMount() {
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
  }

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.responseForgot !== prevProps.responseForgot) {
      if (this.props.responseForgot !== undefined) {
        const {code, status, message} = this.props.responseForgot;
        switch (code) {
          case 200:
            showSuccessMessage(message);
            if (status) {
              const {data} = this.props.responseForgot.result;
              this.setState({email: ''});
              this.props.navigation.navigate('ResetPassword', {data: data});
            }

            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  componentWillUnmount() {
    this._subscription();
  }
  render() {
    return (
      <View style={ForgotPasswordStyle.container}>
        <View style={ForgotPasswordStyle.viewHeader}>
          <Text style={ForgotPasswordStyle.textHeaderForgot}>
            {getLangValue(
              strings.HEADER_FORGOT_PASSWORD,
              this.state.currentLang,
            )}
          </Text>
          <View style={ForgotPasswordStyle.viewBack}>
            <TouchableOpacity onPress={() => this.doBackClick()}>
              <Image
                source={icons.BACK}
                style={ForgotPasswordStyle.imgBack}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={ForgotPasswordStyle.viewMain}>
          <PhoneNumberInput
            ref={input => (this.phoneNumberInput = input)}
            currentLang={this.state.currentLang}
            label={getLangValue(strings.PHONE_NUMBER, this.state.currentLang)}
            labelColor={colors.colorLabelInActive}
            onComplete={(
              country_code,
              phoneNumber,
              errPhoneNumber,
              phoneInput,
            ) => {
              this.phoneInput = phoneInput;
              this.setState({
                country_code: country_code,
                phoneNumber: phoneNumber,
                errPhoneNumber: errPhoneNumber,
              });
            }}
          />
          <View style={ForgotPasswordStyle.viewBtnSubmit}>
            <ButtonGradient
              title={getLangValue(strings.BTN_SUBMIT, this.state.currentLang)}
              backgroundColor={colors.orange}
              fontFamily={fonts.Dosis_Bold}
              fontSize={RFPercentage(3)}
              onPress={() => this.doSubmit()}
              width={wp(80)}
              height={hp(6)}
              currentLang={this.state.currentLang}
              borderRadius={23}
            />
          </View>
        </View>
        {this.props.isBusyForgot ? <Loader /> : undefined}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isBusyForgot: state.auth.isBusyForgot,
    responseForgot: state.auth.responseForgot,
    errorForgot: state.auth.errorForgot,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({doForgotPassword}, dispatch),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ForgotPasswordScreen);
