import React, {PureComponent} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import IntroductionStyle from '../resources/styles/IntroductionStyle';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../resources/constants';
import strings from '../resources/strings';
import {getLangValue} from '../resources/languages/language';
import AppSlider from '../components/slider/AppSlider';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
class IntroductionScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isDone: false,
      currentIndex: 0,
      sliders: [
        {
          key: 0,
          title: 'SELL',
          description: 'Snatch and Sell Vouchers to Earn!',
          image: icons.sell,
        },
        {
          key: 1,
          title: 'REDEEM',
          description: 'Snatch and Redeem Free Vouchers!',
          image: icons.redeem,
        },
        {
          key: 2,
          title: 'ENJOY',
          description: 'Lastly, You can Just Enjoy Free Deals!',
          image: icons.enjoy,
        },
      ],
    };
  }
  onIndexChanged = index => {
    this.setState({currentIndex: index}, () => {
      this.setState({
        isDone: this.state.currentIndex === this.state.sliders.length - 1,
      });
    });
  };
  doSkipIntro = () => {
    AsyncStorage.setItem(constants.SKIP_INTRO, 'true');
    this.props.navigation.navigate('AppIndex');
  };
  renderSliders(value, index) {
    return (
      <View key={value.key} style={IntroductionStyle.viewSliderMain}>
        <Image
          source={value.image}
          style={{
            width: wp(80),
            height: wp(80),
            alignSelf: 'center',
            marginTop:hp(20)
          }}
          resizeMethod="auto"
          resizeMode="contain"
        />
        <Text style={IntroductionStyle.textTitle}>{value.title}</Text>
        <Text style={IntroductionStyle.textDescription}>
          {value.description}
        </Text>
      </View>
    );
  }
  render() {
    return (
      <View style={IntroductionStyle.container}>
        <View style={IntroductionStyle.viewSkip}>
          <TouchableOpacity onPress={this.doSkipIntro}>
            <Text style={IntroductionStyle.textSkip}>
              {getLangValue(
                this.state.isDone ? strings.DONE : strings.SKIP,
                strings.LAG_ENG,
              )}
            </Text>
          </TouchableOpacity>
        </View>

        <AppSlider
          onIndexChanged={index => {
            this.onIndexChanged(index);
          }}
          ref={ref => (this.appRef = ref)}
          sliders={this.state.sliders}
          renderSliders={(value, index) => this.renderSliders(value, index)}
        />
      </View>
    );
  }
}
export default IntroductionScreen;
