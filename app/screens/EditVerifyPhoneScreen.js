import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {View, Text, Image, TouchableOpacity, Platform} from 'react-native';
import colors from '../resources/colors';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../resources/fonts';
import Reinput from 'reinput';
import {getErrorValue, getLangValue} from '../resources/languages/language';
import {showErrorMessage, showSuccessMessage} from '../resources/validation';
import errors from '../resources/errors';
import strings from '../resources/strings';
import ButtonGradient from '../components/buttons/ButtonGradient';
import VerifyPhoneStyle from '../resources/styles/VerifyPhoneStyle';
import {NavigationActions, StackActions} from 'react-navigation';
import {
  doPostEditOtpVerify,
  doPostEditOtpResend,
  doSetUser,
} from '../redux/actions/AppActions';
import Loader from '../components/progress/Loader';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../resources/constants';

let _isMounted = false;

class EditVerifyPhoneScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentLang: strings.LAG_ENG,
      currentUser: props.currentUser,
      code: '',
      errCode: undefined,
      verifyMessage:
        'Please check your SMS. We just sent a verify code into your phone xxxx.',
      countdown: 'Resend code after 20',
      isDisableResend: true,
      timer: 20,
      userData: undefined,
    };
  }
  doResend = () => {
    const {isConnected, currentUser, userData} = this.state;
    if (isConnected) {
      this.setState({isDisableResend: true, timer: 20}, () => {
        this.doCountDownText();
      });

      if (userData !== undefined) {
        const params = {
          userId: userData.userId,
          apiToken: userData.apiToken,
          about_us: userData.about_us,
          full_name: userData.full_name,
          email: userData.email,
          contact: userData.contact,
          deviceType: Platform.OS,
          deviceToken: userData.firebaseToken,
        };
        this.props.doPostEditOtpResend(params).catch(error => {
          showErrorMessage(error.message);
        });
      }
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  };
  doSubmit = () => {
    const {isConnected, currentUser, code, userData} = this.state;

    if (code === '') {
      this.setState(
        {
          errCode: getErrorValue(
            errors.ENTER_VERIFY_CODE,
            this.state.currentLang,
          ),
        },
        () => {
          this.codeInput.focus();
        },
      );
      return;
    }

    this.setState({errCode: undefined});
    if (isConnected) {
      if (userData !== undefined) {
        const params = {
          otp: code,
          userId: userData.userId,
          apiToken: userData.apiToken,
          about_us: userData.about_us,
          full_name: userData.full_name,
          email: userData.email,
          contact: userData.contact,
          deviceType: Platform.OS,
          deviceToken: userData.firebaseToken,
        };
        this.props.doPostEditOtpVerify(params).catch(error => {
          showErrorMessage(error.message);
        });
      }
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  };
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  doCountDownText() {
    this.interval = setInterval(() => {
      this.setState(
        prevState => ({timer: prevState.timer - 1}),
        () => {
          this.setState({
            isDisableResend: this.state.timer === 0 ? false : true,
            countdown: `Resend code after ${this.state.timer}`,
          });
        },
      );
    }, 1000);
  }

  _handleConnectivityChange = state => {
    if (this._isMounted) {
      this.setState({
        isConnected: state.isConnected,
      });
    }
  };
  componentDidMount() {
    this._isMounted = true;
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    const {phoneNumber, userData} = this.props.navigation.state.params;
    // let phoneNumber = '8460785825';
    let phone_number = phoneNumber.replace(
      phoneNumber.substring(0, 6),
      'XXXXXX',
    );
    this.setState({
      verifyMessage: this.state.verifyMessage.replace('xxxx', phone_number),
      userData: userData,
    });
    this.doCountDownText();
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.state.timer === 0) {
      clearInterval(this.interval);
    }
    if (this.props.responseEditOtpVerify !== prevProps.responseEditOtpVerify) {
      if (this.props.responseEditOtpVerify !== undefined) {
        const {code, status, message} = this.props.responseEditOtpVerify;
        switch (code) {
          case 200:
            if (status) {
              this.setState({code: ''}, () => {
                const {data} = this.props.responseEditOtpVerify.result;
                this.props.doSetUser(data);
                AsyncStorage.setItem(constants.USER, JSON.stringify(data));
                showSuccessMessage(message);
                this.doBackClick();
              });
            } else {
              showErrorMessage(message);
            }
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
    if (this.props.responseEditReSendOtp !== prevProps.responseEditReSendOtp) {
      if (this.props.responseEditReSendOtp !== undefined) {
        const {code, status, message} = this.props.responseEditReSendOtp;
        switch (code) {
          case 200:
            showErrorMessage(message);
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  componentWillUnmount() {
    _isMounted = false;
    clearInterval(this.interval);
  }
  render() {
    return (
      <View style={VerifyPhoneStyle.container}>
        <View style={VerifyPhoneStyle.viewHeader}>
          <Text style={VerifyPhoneStyle.textHeaderForgot}>
            {getLangValue(strings.HEADER_VERIFY_CODE, this.state.currentLang)}
          </Text>
          <View style={VerifyPhoneStyle.viewBack}>
            <TouchableOpacity onPress={() => this.doBackClick()}>
              <Image
                source={icons.BACK}
                style={VerifyPhoneStyle.imgBack}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={VerifyPhoneStyle.viewMain}>
          <Text
            style={{
              color: colors.white,
              fontFamily: fonts.Dosis_Regular,
              fontSize: RFPercentage(2),
              marginBottom: hp(2),
            }}>
            {this.state.verifyMessage}
          </Text>
          <Reinput
            ref={input => (this.codeInput = input)}
            label={getLangValue(strings.VERIFY_CODE, this.state.currentLang)}
            labelColor={colors.colorLabelInActive}
            labelActiveColor={colors.colorLabelActive}
            color={colors.white}
            activeColor={colors.white}
            fontFamily={fonts.Dosis_Regular}
            value={this.state.code}
            onChangeText={text => {
              this.setState({code: text}, () => {
                this.state.code === ''
                  ? this.setState({
                      errCode: getErrorValue(
                        errors.ENTER_VERIFY_CODE,
                        this.state.currentLang,
                      ),
                    })
                  : this.setState({errCode: undefined});
              });
            }}
            error={this.state.errCode}
            fontSize={RFPercentage(3)}
            underlineColor={colors.colorLine}
            keyboardType="numeric"
            autoCapitalize="none"
            returnKeyType="done"
          />

          <View style={VerifyPhoneStyle.viewBtnResend}>
            {this.state.isDisableResend ? (
              <View style={VerifyPhoneStyle.viewCountDown}>
                <Text style={VerifyPhoneStyle.textCountDown}>
                  {this.state.countdown}
                </Text>
              </View>
            ) : (
              <ButtonGradient
                title={getLangValue(strings.BTN_RESEND, this.state.currentLang)}
                backgroundColor={colors.orange}
                fontFamily={fonts.Dosis_Bold}
                fontSize={RFPercentage(3)}
                onPress={() => this.doResend()}
                width={wp(80)}
                height={hp(6)}
                currentLang={this.state.currentLang}
                borderRadius={23}
              />
            )}
          </View>
          <View style={VerifyPhoneStyle.viewBtnSubmit}>
            <ButtonGradient
              title={getLangValue(strings.BTN_SUBMIT, this.state.currentLang)}
              backgroundColor={colors.orange}
              fontFamily={fonts.Dosis_Bold}
              fontSize={RFPercentage(3)}
              onPress={() => this.doSubmit()}
              width={wp(80)}
              height={hp(6)}
              currentLang={this.state.currentLang}
              borderRadius={23}
            />
          </View>
        </View>
        {this.props.isBusyEditOtpVerify || this.props.isBusyEditReSendOtp ? (
          <Loader />
        ) : (
          undefined
        )}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    isBusyEditOtpVerify: state.auth.isBusyEditOtpVerify,
    responseEditOtpVerify: state.app.responseEditOtpVerify,
    errorEditOtpVerify: state.app.errorEditOtpVerify,
    isBusyEditReSendOtp: state.app.isBusyEditReSendOtp,
    responseEditReSendOtp: state.app.responseEditReSendOtp,
    errorEditReSendOtp: state.app.errorEditReSendOtp,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {doPostEditOtpVerify, doPostEditOtpResend, doSetUser},
      dispatch,
    ),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditVerifyPhoneScreen);
