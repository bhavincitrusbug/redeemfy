import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {Text, View, TouchableOpacity, Image, ScrollView} from 'react-native';
import colors from '../resources/colors';
import {getLangValue} from '../resources/languages/language';
import strings from '../resources/strings';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../resources/fonts';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Reinput from 'reinput';
import moment from 'moment';
import {Dropdown} from 'react-native-material-dropdown';
import ButtonGradient from '../components/buttons/ButtonGradient';
import TransactionSearchStyle from '../resources/styles/TransactionSearchStyle';
import {doSetSearchOptions} from '../redux/actions/AppActions';
import {generateKey} from '../resources/validation';
import DateTimePicker from 'react-native-modal-datetime-picker';

class TransactionSearchScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      isFromDateTimePickerVisible: false,
      isToDateTimePickerVisible: false,
      fromDateLabel: getLangValue(strings.FROM_DATE, props.currentLang),
      toDateLabel: getLangValue(strings.TO_DATE, props.currentLang),
      selectedFromDate: new Date(),
      selectedToDate: new Date(),
      fromDate: '',
      toDate: '',
      errFromDate: undefined,
      errToDate: undefined,
      minValue: '',
      maxValue: '',
      errMinValue: undefined,
      errMaxValue: undefined,
      keyword: '',
      errKeyword: undefined,
      currentUser: props.currentUser,
      currentLang: props.currentLang,
      transacation_type: '',
      sort_by: '',
      transacation_types: [
        {
          value: getLangValue(strings.SELL, props.currentLang),
        },
        {
          value: getLangValue(strings.BUY, props.currentLang),
        },
      ],
      sort_by_status: [
        {
          value: getLangValue(strings.PROCESSING, props.currentLang),
        },
        {
          value: getLangValue(strings.COMPLETED, props.currentLang),
        },
        {
          value: getLangValue(strings.REJECTED, props.currentLang),
        },
      ],
      type: '',
    };
  }
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  doSearchClick = () => {
    const searchOptions = {
      enable: true,
      fromDate: this.state.fromDate,
      toDate: this.state.toDate,
      keyword: this.state.keyword,
      transacation_type: this.state.transacation_type.toLowerCase(),
      sort_by: this.state.sort_by.toLowerCase(),
      minValue: this.state.minValue,
      maxValue: this.state.maxValue,
      key: generateKey(5),
    };
    this.props.doSetSearchOptions(searchOptions);
    this.props.navigation.goBack(null);
  };
  componentDidMount() {
    const {type} = this.props.navigation.state.params;
    this.setState({type: type === undefined ? '' : type});
  }
  render() {
    return (
      <View style={TransactionSearchStyle.container}>
        <View style={TransactionSearchStyle.viewHeader}>
          <Text style={TransactionSearchStyle.textHeader}>
            {getLangValue(strings.SEARCH, this.state.currentLang)}
          </Text>
          <View style={TransactionSearchStyle.viewBack}>
            <TouchableOpacity onPress={() => this.doBackClick()}>
              <Image
                source={icons.BACK}
                style={{width: wp(6), height: wp(6)}}
              />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView style={TransactionSearchStyle.viewFlex}>
          <View style={TransactionSearchStyle.viewMain}>
            <View style={TransactionSearchStyle.rowCenter}>
              <View style={TransactionSearchStyle.rowItemLeft}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({isFromDateTimePickerVisible: true})
                  }>
                  <Reinput
                    ref={input => (this.fromDateInput = input)}
                    label={getLangValue(strings.FROM, this.state.currentLang)}
                    labelColor={colors.colorLabelInActive}
                    labelActiveColor={colors.colorLabelActive}
                    color={colors.white}
                    activeColor={colors.white}
                    fontFamily={fonts.Dosis_Regular}
                    value={this.state.fromDateLabel}
                    editable={false}
                    pointerEvents="none"
                    onChangeText={text => {
                      this.setState({fromDateLabel: text});
                    }}
                    error={this.state.errFromDate}
                    fontSize={RFPercentage(3)}
                    underlineColor={colors.colorLine}
                    returnKeyType="done"
                  />
                </TouchableOpacity>

                <DateTimePicker
                  isVisible={this.state.isFromDateTimePickerVisible}
                  onConfirm={date => {
                    this.setState({
                      selectedFromDate: date,
                      fromDate: moment(date).format('YYYY-MM-DD'),
                      fromDateLabel: moment(date).format('DD/MM/YYYY'),
                      isFromDateTimePickerVisible: false,
                    });
                  }}
                  onCancel={() => {
                    this.setState({isFromDateTimePickerVisible: false});
                  }}
                  date={this.state.selectedFromDate}
                />
              </View>
              <View style={TransactionSearchStyle.rowItemRight}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({isToDateTimePickerVisible: true})
                  }>
                  <Reinput
                    ref={input => (this.toDateInput = input)}
                    label={getLangValue(strings.TO, this.state.currentLang)}
                    labelColor={colors.colorLabelInActive}
                    labelActiveColor={colors.colorLabelActive}
                    color={colors.white}
                    activeColor={colors.white}
                    fontFamily={fonts.Dosis_Regular}
                    value={this.state.toDateLabel}
                    editable={false}
                    pointerEvents="none"
                    onChangeText={text => {
                      this.setState({toDateLabel: text});
                    }}
                    error={this.state.errToDate}
                    fontSize={RFPercentage(3)}
                    underlineColor={colors.colorLine}
                    returnKeyType="done"
                  />
                </TouchableOpacity>

                <DateTimePicker
                  isVisible={this.state.isToDateTimePickerVisible}
                  onConfirm={date => {
                    this.setState({
                      selectedToDate: date,
                      toDate: moment(date).format('YYYY-MM-DD'),
                      toDateLabel: moment(date).format('DD/MM/YYYY'),
                      isToDateTimePickerVisible: false,
                    });
                  }}
                  onCancel={() => {
                    this.setState({isToDateTimePickerVisible: false});
                  }}
                  date={this.state.selectedToDate}
                />
              </View>
            </View>
            {this.state.type ===
            getLangValue(strings.TRANSACTIONS, this.state.currentLang) ? (
              <Reinput
                ref={input => (this.keywordInput = input)}
                label={getLangValue(strings.KEYWORD, this.state.currentLang)}
                labelColor={colors.colorLabelInActive}
                labelActiveColor={colors.colorLabelActive}
                color={colors.white}
                activeColor={colors.white}
                fontFamily={fonts.Dosis_Regular}
                value={this.state.keyword}
                onChangeText={text => {
                  this.setState({keyword: text});
                }}
                error={this.state.errKeyword}
                fontSize={RFPercentage(3)}
                underlineColor={colors.colorLine}
                returnKeyType="done"
              />
            ) : (
              undefined
            )}
            {this.state.type ===
            getLangValue(strings.TRANSACTIONS, this.state.currentLang) ? (
              <View style={{marginBottom: hp(1)}}>
                <Dropdown
                  label={getLangValue(
                    strings.TRANSACTIONS_TYPES,
                    this.state.currentLang,
                  )}
                  data={this.state.transacation_types}
                  fontSize={RFPercentage(2.5)}
                  itemTextStyle={TransactionSearchStyle.textDropDown}
                  baseColor={colors.white}
                  textColor={colors.white}
                  selectedItemColor={colors.colorBackground}
                  itemColor={colors.colorBackground}
                  value={this.state.transacation_type}
                  onChangeText={(value, index, data) => {
                    this.setState({transacation_type: value});
                  }}
                  error={this.state.errPreference}
                />
              </View>
            ) : null}
            {this.state.type ===
            getLangValue(strings.CASHOUTS, this.state.currentLang) ? (
              <View
                style={{
                  marginBottom:
                    this.state.type ===
                    getLangValue(strings.TRANSACTIONS, this.state.currentLang)
                      ? hp(2)
                      : hp(2),
                }}>
                <Dropdown
                  label={getLangValue(
                    strings.SORT_BY_STATUS,
                    this.state.currentLang,
                  )}
                  data={this.state.sort_by_status}
                  fontSize={RFPercentage(2.5)}
                  itemTextStyle={TransactionSearchStyle.textDropDown}
                  baseColor={colors.white}
                  textColor={colors.white}
                  selectedItemColor={colors.colorBackground}
                  itemColor={colors.colorBackground}
                  value={this.state.sort_by}
                  onChangeText={(value, index, data) => {
                    this.setState({sort_by: value});
                  }}
                  error={this.state.errPreference}
                />
              </View>
            ) : null}

            <View style={TransactionSearchStyle.rowCenter}>
              <View style={TransactionSearchStyle.rowItemLeft}>
                <Reinput
                  ref={input => (this.minInput = input)}
                  label={getLangValue(strings.MIN, this.state.currentLang)}
                  labelColor={colors.colorLabelInActive}
                  labelActiveColor={colors.colorLabelActive}
                  color={colors.white}
                  activeColor={colors.white}
                  fontFamily={fonts.Dosis_Regular}
                  value={`$` + this.state.minValue}
                  onChangeText={text => {
                    this.setState({minValue: text.replace(/\D/g, '')});
                  }}
                  error={this.state.errMinValue}
                  fontSize={RFPercentage(3)}
                  underlineColor={colors.colorLine}
                  returnKeyType="done"
                />
              </View>
              <View style={TransactionSearchStyle.rowItemRight}>
                <Reinput
                  ref={input => (this.maxInput = input)}
                  label={getLangValue(strings.MAX, this.state.currentLang)}
                  labelColor={colors.colorLabelInActive}
                  labelActiveColor={colors.colorLabelActive}
                  color={colors.white}
                  activeColor={colors.white}
                  fontFamily={fonts.Dosis_Regular}
                  value={`$` + this.state.maxValue}
                  onChangeText={text => {
                    this.setState({maxValue: text.replace(/\D/g, '')});
                  }}
                  error={this.state.errMaxValue}
                  fontSize={RFPercentage(3)}
                  underlineColor={colors.colorLine}
                  returnKeyType="done"
                />
              </View>
            </View>
          </View>
        </ScrollView>
        <View style={TransactionSearchStyle.viewButton}>
          <ButtonGradient
            title={getLangValue(strings.BTN_SEARCH, this.state.currentLang)}
            backgroundColor={colors.orange}
            fontFamily={fonts.Dosis_Bold}
            fontSize={RFPercentage(2.5)}
            onPress={() => this.doSearchClick()}
            width={wp(80)}
            height={hp(6)}
            currentLang={this.state.currentLang}
            borderRadius={23}
          />
        </View>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    isBusyTransactions: state.app.isBusyTransactions,
    responseTransactions: state.app.responseTransactions,
    errorTransactions: state.app.errorTransactions,
    isBusyCashoutRequest: state.app.isBusyCashoutRequest,
    responseCashoutRequest: state.app.responseCashoutRequest,
    errorCashoutRequest: state.app.errorCashoutRequest,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({doSetSearchOptions}, dispatch),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TransactionSearchScreen);
