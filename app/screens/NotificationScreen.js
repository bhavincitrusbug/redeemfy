import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {View, Text, Image, TouchableOpacity, FlatList} from 'react-native';
import icons from '../resources/icons';
import {getLangValue, getErrorValue} from '../resources/languages/language';
import strings from '../resources/strings';
import NotificationStyle from '../resources/styles/NotificationStyle';
import NotificationListItem from '../components/list/NotificationListItem';
import {
  getTimeAgo,
  showErrorMessage,
  showSuccessMessage,
} from '../resources/validation';
import Loader from '../components/progress/Loader';
import {
  doGetNotificationList,
  doPostNotificationRead,
} from '../redux/actions/AppActions';
import errors from '../resources/errors';
import NetInfo from '@react-native-community/netinfo';
import moment from 'moment';
_isMounted = false;

class NotificationScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentLang: strings.LAG_ENG,
      currentUser: props.currentUser,
      refreshing: false,
      viewEmpty: false,
      last_page: undefined,
      page: 1,
      notificationsData: [],
    };
  }

  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  doReadNotification() {
    if (this.state.isConnected) {
      const params = {
        apiToken:
          this.state.currentUser !== undefined
            ? this.state.currentUser.token
            : '',
      };
      this.props.doPostNotificationRead(params);
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  }
  onRefresh() {
    this.setState({page: 1, refreshing: true}, () => {
      this.getNotificationList();
    });
  }
  handleLoadMore() {
    if (this.state.page < this.state.last_page) {
      this.setState(
        (prevState, nextProps) => ({
          page: prevState.page + 1,
        }),
        () => {
          this.getNotificationList();
        },
      );
    }
  }
  renderSeparator = () => {
    return <View style={NotificationStyle.viewSeprator} />;
  };
  renderEmpty = () => {
    return (
      <View style={NotificationStyle.viewEmptyMain}>
        <Image
          resizeMode="contain"
          source={icons.NO_NOTIFICATIONS}
          style={NotificationStyle.imgNoReward}
        />
        <Text style={NotificationStyle.textNoRewards}>
          {getLangValue(strings.NO_NOTIFICATIONS, this.state.currentLang)}
        </Text>
      </View>
    );
  };
  doOpenNotification(item) {}
  getNotificationList() {
    const params = {
      apiToken:
        this.state.currentUser !== undefined
          ? this.state.currentUser.token
          : '',
    };
    this.props.doGetNotificationList(params).catch(error => {
      showErrorMessage(error.message);
    });
  }
  _handleConnectivityChange = state => {
    if (this._isMounted) {
      this.setState({
        isConnected: state.isConnected,
      });
    }
  };
  componentDidMount() {
    this._isMounted = true;
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    this.getNotificationList();
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (
      this.props.responseNotificationList !== prevProps.responseNotificationList
    ) {
      if (this.props.responseNotificationList !== undefined) {
        const {code, status, message} = this.props.responseNotificationList;
        switch (code) {
          case 200:
            const {data, links} = this.props.responseNotificationList.result;
            const {last, current} = links;
            if (current === 1) {
              this.setState({
                notificationsData: data,
                refreshing: false,
                last_page: last,
                viewEmpty: data.length <= 0,
              });
            } else {
              this.setState({
                notificationsData: [...this.state.notificationsData, ...data],
                refreshing: false,
                last_page: last,
                viewEmpty: false,
              });
            }
            break;
          case 400:
            this.setState({
              notificationsData: [],
              refreshing: false,
              last_page: 1,
              viewEmpty: true,
            });
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
    if (
      this.props.responseNotificationRead !== prevProps.responseNotificationRead
    ) {
      const {code, status, message} = this.props.responseNotificationRead;
      switch (code) {
        case 200:
          if (status) {
            this.setState(
              {
                notificationsData: [],
                refreshing: false,
                last_page: undefined,
                viewEmpty: true,
              },
              () => {
                showSuccessMessage(message);
              },
            );
          }
          break;
        default:
          showErrorMessage(message);
          break;
      }
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
    this._subscription();
  }
  render() {
    return (
      <View style={NotificationStyle.container}>
        <View style={NotificationStyle.viewHeader}>
          <Text style={NotificationStyle.textHeaderForgot}>
            {getLangValue(strings.HEADER_NOTIFICATIONS, this.state.currentLang)}
          </Text>
          <View style={NotificationStyle.viewBack}>
            <TouchableOpacity onPress={() => this.doBackClick()}>
              <Image
                source={icons.BACK}
                style={NotificationStyle.imgBack}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={NotificationStyle.viewMain}>
          <View style={NotificationStyle.viewMarkRead}>
            <TouchableOpacity onPress={() => this.doReadNotification()}>
              <Text style={NotificationStyle.textMarkRead}>
                {getLangValue(strings.MARK_ALL_AS_ROAD, this.state.currentLang)}
              </Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={this.state.notificationsData}
            renderItem={({item, index}) => (
              <TouchableOpacity onPress={() => this.doOpenNotification(item)}>
                <NotificationListItem
                  title={item.title}
                  body={item.description}
                  date={getTimeAgo(index, item.date)}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index.toString()}
            ItemSeparatorComponent={this.renderSeparator}
            refreshing={this.state.refreshing}
            onRefresh={() => this.onRefresh()}
            onEndReached={() => this.handleLoadMore()}
            onEndReachedThreshold={15}
            ListEmptyComponent={this.state.viewEmpty ? this.renderEmpty : null}
            contentContainerStyle={
              this.state.viewEmpty && NotificationStyle.listContent
            }
          />
        </View>
        {this.props.isBusyNotificationList ||
        this.props.isBusyNotificationRead ? (
          <Loader />
        ) : (
          undefined
        )}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    isBusyNotificationList: state.app.isBusyNotificationList,
    responseNotificationList: state.app.responseNotificationList,
    errorNotificationList: state.app.errorNotificationList,
    isBusyNotificationRead: state.app.isBusyNotificationRead,
    responseNotificationRead: state.app.responseNotificationRead,
    errorNotificationRead: state.app.errorNotificationRead,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {doGetNotificationList, doPostNotificationRead},
      dispatch,
    ),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(NotificationScreen);
