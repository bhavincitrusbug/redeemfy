import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import colors from '../resources/colors';
import icons from '../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../resources/fonts';
import Reinput from 'reinput';
import {getErrorValue, getLangValue} from '../resources/languages/language';
import {showErrorMessage, showSuccessMessage, maskingEmail} from '../resources/validation';
import errors from '../resources/errors';
import strings from '../resources/strings';
import ButtonGradient from '../components/buttons/ButtonGradient';
import VerifyPhoneStyle from '../resources/styles/VerifyPhoneStyle';
import {NavigationActions, StackActions} from 'react-navigation';
import {
  doPostVerifyNumber,
  doPostResendCode,
} from '../redux/actions/AuthActions';
import Loader from '../components/progress/Loader';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../resources/constants';

let _isMounted = false;

class VerifyPhoneScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      currentLang: strings.LAG_ENG,
      currentUser: props.currentUser,
      code: '',
      errCode: undefined,
      verifyMessage:
        'Please check your SMS. We just sent a verify code into your phone xxxx.',
      countdown: 'Resend code after 20',
      isDisableResend: true,
      timer: 20,
      from: undefined,
    };
  }
  doResend = () => {
    const {isConnected, currentUser} = this.state;
    if (isConnected) {
      this.setState({isDisableResend: true, timer: 20}, () => {
        this.doCountDownText();
      });
      const params = {
        userId: currentUser !== undefined ? currentUser.id : 0,
      };
      this.props.doPostResendCode(params).catch(error => {
        showErrorMessage(error.message);
      });
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  };
  doSubmit = () => {
    const {isConnected, currentUser, code} = this.state;

    if (code === '') {
      this.setState(
        {
          errCode: getErrorValue(
            errors.ENTER_VERIFY_CODE,
            this.state.currentLang,
          ),
        },
        () => {
          this.codeInput.focus();
        },
      );
      return;
    }

    this.setState({errCode: undefined});
    if (isConnected) {
      const params = {
        otp: code,
        userId: currentUser !== undefined ? currentUser.id : 0,
      };
      this.props.doPostVerifyNumber(params).catch(error => {
        showErrorMessage(error.message);
      });
    } else {
      showErrorMessage(
        getErrorValue(errors.NO_INTERNET_CONNECTION, this.state.currentLang),
      );
    }
  };
  doBackClick = () => {
    this.props.navigation.goBack(null);
  };
  doCountDownText() {
    this.interval = setInterval(() => {
      this.setState(
        prevState => ({timer: prevState.timer - 1}),
        () => {
          this.setState({
            isDisableResend: this.state.timer === 0 ? false : true,
            countdown: `Resend code after ${this.state.timer}`,
          });
        },
      );
    }, 1000);
  }
  doFinish(screen) {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: screen,
        }),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  }

  _handleConnectivityChange = state => {
    if (this._isMounted) {
      this.setState({
        isConnected: state.isConnected,
      });
    }
  };
  componentDidMount() {
    this._isMounted = true;
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    const {phoneNumber, from, email} = this.props.navigation.state.params;
    // let phoneNumber = '8460785825';
    let maskedEmail = maskingEmail(email);
    let phone_number = phoneNumber.replace(
      phoneNumber.substring(0, 6),
      'XXXXXX',
    );
    this.setState({
      verifyMessage: this.state.verifyMessage
        .replace('xxxx', phone_number),
      from: from,
    });
    this.doCountDownText();
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.state.timer === 0) {
      clearInterval(this.interval);
    }
    if (this.props.responseVerify !== prevProps.responseVerify) {
      if (this.props.responseVerify !== undefined) {
        const {code, status, message} = this.props.responseVerify;
        switch (code) {
          case 200:
            if (status) {
              showSuccessMessage(message)
              this.setState({code: ''}, () => {
                if (this.state.from === 'LoginRegister') {
                  this.doFinish('Home');
                } else if (this.state.from === 'LoginPopUp') {
                  this.doBackClick();
                } else {
                  this.doFinish('Home');
                }
              });
            } else {
              showErrorMessage(message);
            }
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
    if (this.props.responseResend !== prevProps.responseResend) {
      if (this.props.responseResend !== undefined) {
        const {code, status, message} = this.props.responseResend;
        switch (code) {
          case 200:
            showErrorMessage(message);
            break;
          default:
            showErrorMessage(message);
            break;
        }
      }
    }
  }
  componentWillUnmount() {
    _isMounted = false;
    clearInterval(this.interval);
  }
  render() {
    return (
      <View style={VerifyPhoneStyle.container}>
        <View style={VerifyPhoneStyle.viewHeader}>
          <Text style={VerifyPhoneStyle.textHeaderForgot}>
            {getLangValue(strings.HEADER_VERIFY_CODE, this.state.currentLang)}
          </Text>
          <View style={VerifyPhoneStyle.viewBack}>
            <TouchableOpacity onPress={() => this.doBackClick()}>
              <Image
                source={icons.BACK}
                style={VerifyPhoneStyle.imgBack}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={VerifyPhoneStyle.viewMain}>
          <Text
            style={{
              color: colors.white,
              fontFamily: fonts.Dosis_Regular,
              fontSize: RFPercentage(2),
              marginBottom: hp(2),
            }}>
            {this.state.verifyMessage}
          </Text>
          <Reinput
            ref={input => (this.codeInput = input)}
            label={getLangValue(strings.VERIFY_CODE, this.state.currentLang)}
            labelColor={colors.colorLabelInActive}
            labelActiveColor={colors.colorLabelActive}
            color={colors.white}
            activeColor={colors.white}
            fontFamily={fonts.Dosis_Regular}
            value={this.state.code}
            onChangeText={text => {
              this.setState({code: text}, () => {
                this.state.code === ''
                  ? this.setState({
                      errCode: getErrorValue(
                        errors.ENTER_VERIFY_CODE,
                        this.state.currentLang,
                      ),
                    })
                  : this.setState({errCode: undefined});
              });
            }}
            error={this.state.errCode}
            fontSize={RFPercentage(3)}
            underlineColor={colors.colorLine}
            keyboardType="numeric"
            autoCapitalize="none"
            returnKeyType="done"
          />

          <View style={VerifyPhoneStyle.viewBtnResend}>
            {this.state.isDisableResend ? (
              <View style={VerifyPhoneStyle.viewCountDown}>
                <Text style={VerifyPhoneStyle.textCountDown}>
                  {this.state.countdown}
                </Text>
              </View>
            ) : (
              <ButtonGradient
                title={getLangValue(strings.BTN_RESEND, this.state.currentLang)}
                backgroundColor={colors.orange}
                fontFamily={fonts.Dosis_Bold}
                fontSize={RFPercentage(3)}
                onPress={() => this.doResend()}
                width={wp(80)}
                height={hp(6)}
                currentLang={this.state.currentLang}
                borderRadius={23}
              />
            )}
          </View>
          <View style={VerifyPhoneStyle.viewBtnSubmit}>
            <ButtonGradient
              title={getLangValue(strings.BTN_SUBMIT, this.state.currentLang)}
              backgroundColor={colors.orange}
              fontFamily={fonts.Dosis_Bold}
              fontSize={RFPercentage(3)}
              onPress={() => this.doSubmit()}
              width={wp(80)}
              height={hp(6)}
              currentLang={this.state.currentLang}
              borderRadius={23}
            />
          </View>
        </View>
        {this.props.isBusyVerify || this.props.isBusyResend ? (
          <Loader />
        ) : (
          undefined
        )}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
    isBusyVerify: state.auth.isBusyVerify,
    responseVerify: state.auth.responseVerify,
    errorVerify: state.auth.errorVerify,
    isBusyResend: state.auth.isBusyResend,
    responseResend: state.auth.responseResend,
    errorResend: state.auth.errorResend,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({doPostVerifyNumber, doPostResendCode}, dispatch),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(VerifyPhoneScreen);
