import colors from './colors';
import {Platform} from 'react-native';
import phoneNumberFormatter from 'phone-number-formats';
import {showMessage, hideMessage} from 'react-native-flash-message';
import fonts from './fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import PhoneNumber from 'awesome-phonenumber';
import moment from 'moment';
import momenttimezone from 'moment-timezone';
export function validateEmail(text) {
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (reg.test(text) === false) {
    return false;
  } else {
    return true;
  }
}
export function getPercentage(partialValue, totalValue) {
  return (100 * partialValue) / totalValue;
}
export function showErrorMessage(message) {
  // Snackbar.show({
  //   title: message,
  //   duration: Snackbar.LENGTH_SHORT,
  //   color: colors.white,
  // });
  showMessage({
    message: message,
    animationDuration: 400,
    duration: 1500,
    type: 'default',
    autoHide: true,
    backgroundColor: colors.colorError,
    titleStyle: {
      color: colors.white,
      fontFamily: fonts.Dosis_Regular,
      fontSize: RFPercentage(2.5),
      textAlign: 'center',
    },
  });
}

export function showSuccessMessage(message) {
  // Snackbar.show({
  //   title: message,
  //   duration: Snackbar.LENGTH_SHORT,
  //   color: colors.white,
  // });
  showMessage({
    message: message,
    animationDuration: 400,
    duration: 1500,
    type: 'default',
    autoHide: true,
    backgroundColor: colors.green,
    titleStyle: {
      color: colors.white,
      fontFamily: fonts.Dosis_Regular,
      fontSize: RFPercentage(2.5),
      textAlign: 'center',
    },
  });
}
function changeTimezone(date, ianatz) {
  // suppose the date is 12:00 UTC
  var invdate = new Date(
    date.toLocaleString('en-US', {
      timeZone: ianatz,
    }),
  );

  // then invdate will be 07:00 in Toronto
  // and the diff is 5 hours
  var diff = date.getTime() - invdate.getTime();

  // so 12:00 in Toronto is 17:00 UTC
  return new Date(date.getTime() + diff);
}

function timeDifference(current, previous) {
  var msPerMinute = 60 * 1000;
  var msPerHour = msPerMinute * 60;
  var msPerDay = msPerHour * 24;
  var msPerMonth = msPerDay * 30;
  var msPerYear = msPerDay * 365;

  var elapsed = current - previous;

  if (elapsed < msPerMinute) {
    return Math.round(elapsed / 1000) + ' seconds ago';
  } else if (elapsed < msPerHour) {
    return Math.round(elapsed / msPerMinute) + ' minutes ago';
  } else if (elapsed < msPerDay) {
    return Math.round(elapsed / msPerHour) + ' hours ago';
  } else if (elapsed < msPerMonth) {
    return '' + Math.round(elapsed / msPerDay) + ' days ago';
  } else if (elapsed < msPerYear) {
    return '' + Math.round(elapsed / msPerMonth) + ' months ago';
  } else {
    return '' + Math.round(elapsed / msPerYear) + ' years ago';
  }
}

export function calcTime(city, offset) {
  let d = new Date();

  let utc = d.getTime() + d.getTimezoneOffset() * 60000;

  let nd = new Date(utc + 3600000 * offset);
  return nd;
}

export function getTimeAgo(index, dateTime) {
  // var current = changeTimezone(new Date(), 'Asia/Singapore');
  var current = calcTime('Singapore', '+8');
  var previous = moment(dateTime, 'YYYY-MM-DD  HH:mm:ss');
  console.log('current', current);
  console.log('previous', dateTime);
  return '' + timeDifference(current, previous);
}

export function phoneNumberFormat(text) {
  // if (text.length > 10) {
  //   return text;
  // }
  var cleaned = ('' + text).replace(/\D/g, '');

  var match = cleaned.match(/(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    var number = [match[1], '-', match[2], '-', match[3]].join('');
    return number;
  }

  return text;
  // if (text.length < 10) {
  //   return text;
  // } else {
  //   let work = new phoneNumberFormatter(cleaned).format({
  //     type: 'international',
  //   });
  //   return work.toString();
  // }
}
export function testID(id) {
  return Platform.OS === 'android'
    ? {accessible: true, accessibilityLabel: id}
    : {testID: id};
}

export function add3Dots(string, limit) {
  var dots = '...';
  if (string.length > limit) {
    // you can also use substr instead of substring
    string = string.substring(0, limit) + dots;
  }

  return string;
}

export function generateKey(length) {
  var result = '';
  var characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export function checkOldNewPhone(newNumber, oldNumber) {
  let number = new PhoneNumber(oldNumber);
  let userContact = oldNumber
    .replace('+', '')
    .replace(number.getCountryCode(), '');
  if (newNumber.replace(/[^0-9]/g, '') === userContact) {
    return true;
  } else {
    return false;
  }
}

export function maskingEmail(email) {
  var maskedid = '';
  var myemailId = email;
  var index = myemailId.lastIndexOf('@');
  var indexDot = myemailId.lastIndexOf('.');
  var prefix = myemailId.substring(0, index);
  var postfix = myemailId.substring(index + 1, indexDot);
  var domain = myemailId.substring(indexDot);

  var maskPrefix = prefix
    .split('')
    .map(function(o, i) {
      if (i === 0 || i === index - 1) {
        return o;
      } else {
        return '*';
      }
    })
    .join('');
  var maskPostfix = postfix
    .split('')
    .map(function(o, i) {
      return '*';
    })
    .join('');

  maskedid = maskPrefix + myemailId.charAt(index) + maskPostfix + domain;
  return maskedid;
}
