const constants = {
  DEVICE_TOKEN: 'deviceToken',
  SKIP_INTRO: 'skipIntro',
  IS_LOGIN: 'isLogin',
  USER: 'user',
  CLAIMED: 'CLAIMED',
  REDEEMED: 'REDEEMED',
  SALE: 'SALE',
  BOUGHT: 'BOUGHT',
  UNUSED: 'UNUSED',
  STRIPE_KEY: 'pk_live_51HBuJzL2DB8Hy6gYB5xiLv1eUTwcSRyDpVNqfOFRxHVKnVM1i0TICYkplWHaWmgT26xS4JtZtswLQB3e0wLKMz3R00rGgf9xm3',
  PRIVACY_POLICY: 'https://rdfy-master.stablecompute.com/api/v1/terms&conditions',
};
export default constants;
