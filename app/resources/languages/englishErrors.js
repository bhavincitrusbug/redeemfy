const englishErrors = {
  NO_INTERNET_CONNECTION: 'No Internet Connection!',
  ENTER_EMAIL: 'Enter Email',
  ENTER_VALID_EMAIL: 'Enter Valid Email',
  ENTER_PASSWORD: 'Enter Password',
  ENTER_OLD_PASSWORD: 'Enter Old Password',
  ENTER_NEW_PASSWORD: 'Enter New Password',
  ENTER_CONFIRM_PASSWORD: 'Enter Confirm Password',
  VALID_CONFIRM_PASSWORD: 'Entered Password Does Not Match',
  ENTER_FULL_NAME: 'Enter Full Name',
  ENTER_ACCOUNT_NUMBER: 'Enter Account Number',
  ENTER_BRANCH_CODE: 'Enter Branch Code',
  ENTER_PHONE_NUMBER: 'Enter Phone Number',
  ENTER_VERIFY_CODE: 'Enter Verify Code',
  SELECT_COUNTRY_CODE: 'Select Code',
  ENTER_AMOUNT: 'Enter Amount',
  ACCEPT_TERMS_PRIVACY:
    'Please indicate that you accept the Terms and Conditions & Privacy Policy',
};
export default englishErrors;
