import english from './english';
import englishErrors from './englishErrors';
import japanese from './japanese';
import japaneseErrors from './japaneseErrors';
import strings from '../strings';
export function getLangValue(value, language) {
  if (language === strings.LAG_ENG) {
    return english[value];
  } else {
    return japanese[value];
  }
}

export function getErrorValue(value, language) {
  if (language === strings.LAG_ENG) {
    return englishErrors[value];
  } else {
    return japaneseErrors[value];
  }
}
