import {StyleSheet} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const CashoutRequestStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  viewHeader: {
    flexDirection: 'row',
    marginStart: wp(5),
    marginEnd: wp(5),
    marginTop: wp(5),
  },
  imgBack: {width: wp(5), height: wp(5)},
  viewMain: {
    marginStart: wp(5),
    marginEnd: wp(5),
    marginTop: hp(2),
  },
  viewRewardImg: {
    backgroundColor: 'rgba(255,255,255,0.10)',
    width: wp(90),
    height: wp(90),
  },
  imgReward: {flex: 1},
  textTitle: {
    marginTop: 10,
    color: colors.white,
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(4),
  },
  textMessage: {
    marginTop: 10,
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(3),
    opacity: 0.5,
  },
  textTerms: {
    marginTop: 10,
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2),
  },
  viewButtonClaim: {position: 'absolute', start: 0, end: 0, bottom: 0},
  touchModalMain: {
    flex: 1,
  },
  viewModalMain: {
    backgroundColor: 'rgba(0,0,0,0.30)',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewModalBackground: {
    width: wp(90),
    height: hp(40),
    backgroundColor: colors.white,
    borderRadius: 10,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textEnterQty: {
    color: colors.black,
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
    alignSelf: 'center',
  },
  viewQtyItem: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: wp(15),
    marginBottom: wp(15),
  },
  touchImg: {flex: 2, alignItems: 'center'},
  viewQtyText: {
    width: 177,
    height: 50,
    backgroundColor: '#FAFAFA',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  inputQty: {
    color: colors.black,
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
    flex: 1,
    textAlign: 'center',
  },
  imgArrow: {width: wp(6), height: wp(6)},
  viewButtonConfirm: {alignSelf: 'center'},
  viewButtonRequest: {
    alignSelf: 'center',
    marginBottom: hp(2),
    position: 'absolute',
    bottom: 0,
  },
});

export default CashoutRequestStyle;
