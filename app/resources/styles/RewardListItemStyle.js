import {StyleSheet} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const RewardListItemStyle = StyleSheet.create({
  viewListItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: hp(2),
    backgroundColor: 'rgba(255,255,255,0.10)',
    borderRadius: 8,
    padding: 10,
  },
  imgReward: {
    width: 100,
    height: 100,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },
  viewTextItem: {marginStart: 10, flex: 1, marginEnd: 5},
  textTitle: {
    color: colors.white,
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
    marginBottom: 10,
  },
  textDescription: {
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(3),
    marginBottom: 10,
    opacity: 0.5,
  },
  viewRowCenter: {flexDirection: 'row', alignItems: 'center'},
  textLeft: {
    marginStart: 10,
    marginEnd: 10,
    marginBottom: 5,
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2),
    opacity: 0.5,
  },
});

export default RewardListItemStyle;
