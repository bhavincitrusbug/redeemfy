import {StyleSheet} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const ResetPasswordStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  viewHeader: {flexDirection: 'row', alignItems: 'center', margin: wp(5)},
  viewBack: {position: 'absolute'},
  imgBack: {width: wp(5), height: hp(5)},
  textHeaderForgot: {
    textAlign: 'center',
    flex: 1,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Regular,
    color: colors.white,
    fontWeight: '400',
  },
  viewMain: {
    flex: 1,
    width: '90%',
    marginTop: hp(5),
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  viewBtnSubmit:{marginTop: hp(5), alignSelf: 'center'}
});

export default ResetPasswordStyle;
