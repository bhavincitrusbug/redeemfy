import {StyleSheet} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const TransactionListItemStyle = StyleSheet.create({
  viewListItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: hp(2),
    padding: 10,
    backgroundColor: 'rgba(0,0,0,0.10)',
    borderRadius: 8,
  },
  imgCoupon: {
    width: 80,
    height: 80,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },
  viewTextItem: {flex: 1, marginStart: 10},
  viewTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
  },
  textTitle: {
    color: colors.white,
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
    flex: 1,
  },
  imgRight: {
    width: 15,
    height: 15,
    tintColor: 'rgba(255,255,255,0.5)',
  },
  viewDate: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
  },
  textDate: {
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2.5),
    opacity: 0.5,
    flex: 1,
  },
  viewRejected: {
    backgroundColor: '#f45c56',
    borderRadius: 15,
    width: 100,
    height: 30,
    alignItems: 'center',
    justifyContent:'center'
  },
  textRejected: {
    padding: 5,
    color: colors.white,
    fontSize: RFPercentage(2),
    fontFamily: fonts.Dosis_Bold,
  },
  viewCompleted: {
    backgroundColor: '#3fbca4',
    borderRadius: 15,
    width: 100,
    height: 30,
    alignItems: 'center',
    justifyContent:'center'
  },
  textCompleted: {
    padding: 5,
    color: colors.white,
    fontSize: RFPercentage(2),
    fontFamily: fonts.Dosis_Bold,
  },
  viewProcessing: {
    backgroundColor: '#ff8d00',
    borderRadius: 15,
    width: 100,
    height: 30,
    alignItems: 'center',
    justifyContent:'center'
  },
  textProcessing: {
    padding: 5,
    color: colors.white,
    fontSize: RFPercentage(2),
    fontFamily: fonts.Dosis_Bold,
  },
  textPrice: {
    padding: 5,
    color: '#3fbca4',
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Regular,
  },
  textPriceBuy: {
    padding: 5,
    color: '#f45c56',
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Regular,
  },
});

export default TransactionListItemStyle;
