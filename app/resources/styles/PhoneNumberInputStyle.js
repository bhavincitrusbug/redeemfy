import {StyleSheet} from 'react-native';
import colors from '../colors';
import fonts from '../fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';

const PhoneNumberInputStyle = StyleSheet.create({
  viewBorderNormal: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: colors.colorLine,
    borderBottomWidth: 2,
  },
  viewBorderError: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: colors.colorError,
    borderBottomWidth: 2,
  },
  viewInput: {flex: 1, marginStart: 5},
  textError: {
    fontSize: RFPercentage(2),
    fontFamily: fonts.Dosis_Regular,
    color: colors.colorError,
    marginTop: 5,
  },
  inputNumber: {
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(3),
    marginBottom: 5,
    paddingBottom: 5,
  },
});

export default PhoneNumberInputStyle;
