import {StyleSheet} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const BagListItemStyle = StyleSheet.create({
  viewListItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: hp(2),
    padding: 10,
    backgroundColor: 'rgba(0,0,0,0.10)',
    borderRadius: 8,
  },
  imgReward: {
    width: 80,
    height: 80,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },
  viewTextItem: {marginStart: 10, flex: 1},
  textTitle: {
    color: colors.white,
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
  },
  textDescription: {
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2.5),
    opacity: 0.5,
    flex: 1,
  },
  viewRowCenter: {flexDirection: 'row', alignItems: 'center', marginTop: 5},
  textQuantity: {
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2),
    alignSelf: 'center',
  },
  viewTextQuantity: {
    backgroundColor: '#1E252C',
    borderRadius: 12,
    borderWidth: 1,
    borderColor: 'rgba(255,255,255,0.20)',
    width: 72,
    height: 24,
    alignItems: 'center',
    justifyContent: 'center',
    marginStart: 20,
  },
});

export default BagListItemStyle;
