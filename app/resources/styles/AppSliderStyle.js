import {StyleSheet} from 'react-native';
import colors from '../colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const AppSliderStyle = StyleSheet.create({
  container: {
    flex: 1,
  },
  inactiveDot: {
    backgroundColor: colors.white,
    width: 10,
    height: 10,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    opacity: 0.5,
  },
  activeDot: {
    backgroundColor: colors.white,
    borderRadius: 4,
    width: 10,
    height: 10,
    marginLeft: 3,
    marginRight: 3,
  },
  pagination: {
    marginStart: wp(8),
    marginBottom: wp(3),
    justifyContent: 'flex-start',
    bottom: 5,
  },
});

export default AppSliderStyle;
