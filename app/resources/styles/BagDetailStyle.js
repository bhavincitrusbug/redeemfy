import {StyleSheet} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const BagDetailStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  viewHeader: {
    flexDirection: 'row',
    marginStart: wp(5),
    marginEnd: wp(5),
    marginTop: wp(5),
  },
  imgBack: {width: wp(5), height: wp(5)},
  viewMain: {
    marginStart: wp(5),
    marginEnd: wp(5),
    marginTop: hp(5),
  },
  viewRewardImg: {
    backgroundColor: 'rgba(255,255,255,0.10)',
    width: wp(90),
    height: wp(90),
    borderRadius: 6,
  },
  imgReward: {flex: 1},
  textTitle: {
    marginTop: 10,
    color: colors.white,
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(5),
  },
  textDescription: {
    marginTop: hp(2),
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(3),
    opacity: 0.5,
  },
  textUniqueIdLabel: {
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2.5),
    opacity: 0.5,
  },
  textUniqueIdValue: {
    color: colors.colorBackground,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2.5),
  },
  textTerms: {
    marginTop: hp(2),
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2),
  },
  viewButtonRow: {
    position: 'absolute',
    start: 0,
    end: 0,
    bottom: 0,
    flexDirection: 'row',
  },
  viewButtonSell: {flex: 1, backgroundColor: colors.colorTabBackground},
  viewSell: {
    width: wp(50),
    height: hp(10),
    alignItems: 'center',
    justifyContent: 'center',
  },
  textSell: {
    color: colors.white,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Bold,
  },
  viewButtonRedeem: {flex: 1},
  imgIcon: {
    marginStart: 10,
    width: wp(6),
    height: wp(6),
  },
  touchModalMain: {
    flex: 1,
  },
  viewModalMain: {
    backgroundColor: 'rgba(0,0,0,0.30)',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewModalBackground: {
    width: wp(90),
    height: hp(40),
    backgroundColor: colors.white,
    borderRadius: 10,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textEnterQty: {
    color: colors.black,
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
    alignSelf: 'center',
  },
  viewQtyItem: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: wp(15),
    marginBottom: wp(15),
  },
  touchImg: {flex: 2, alignItems: 'center'},
  viewQtyText: {
    width: 83,
    height: 40,
    backgroundColor: '#FAFAFA',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textQty: {
    color: colors.black,
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
  },
  imgArrow: {width: wp(6), height: wp(6)},
  viewButtonConfirm: {alignSelf: 'center'},
});

export default BagDetailStyle;
