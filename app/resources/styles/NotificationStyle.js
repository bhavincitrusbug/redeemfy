import {StyleSheet} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const NotificationStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  viewHeader: {flexDirection: 'row', alignItems: 'center', margin: wp(5)},
  viewBack: {position: 'absolute'},
  imgBack: {width: wp(5), height: hp(5)},
  textHeaderForgot: {
    textAlign: 'center',
    flex: 1,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Regular,
    color: colors.white,
    fontWeight: '400',
  },
  viewMarkRead: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  textMarkRead: {
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2.5),
    color: colors.white,
  },
  viewSeprator: {
    height: 1,
    marginStart: wp(2),
    marginEnd: wp(2),
    backgroundColor: '#EAECEF',
  },
  viewMain: {
    flex: 1,
    marginStart: wp(5),
    marginEnd: wp(5),
  },
  viewEmptyMain: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgReward: {
    height: wp(20),
    width: wp(20),
    marginBottom: 10,
    tintColor: colors.white,
  },
  imgNoReward: {
    height: wp(20),
    width: wp(20),
    marginBottom: 10,
    tintColor: 'rgba(255,255,255,0.6)',
  },
  textNoRewards: {
    color: colors.white,
    textAlign: 'center',
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
    opacity: 0.6,
  },
  listContent: {
    height: '100%',
  },
});

export default NotificationStyle;
