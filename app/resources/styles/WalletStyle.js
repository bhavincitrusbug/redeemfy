import {StyleSheet} from 'react-native';
import colors from '../colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';

const WalletStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  rowCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: hp(3),
    marginBottom: hp(3),
  },
  viewHeader: {
    flexDirection: 'row',
    marginStart: wp(5),
    marginEnd: wp(5),
    marginTop: wp(5),
    alignItems: 'center',
  },
  textHeader: {
    textAlign: 'center',
    flex: 1,
    color: colors.white,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Regular,
  },
  viewBack: {position: 'absolute'},
  viewSearch: {position: 'absolute', end: 0},
  imgBack: {width: wp(6), height: wp(6)},
  imgSearch: {width: wp(6), height: wp(6), tintColor: colors.white},
  viewWallet: {
    backgroundColor: colors.colorTabBackground,
    borderRadius: 10,
    marginTop: wp(5),
    borderWidth: 0.5,
    borderColor: colors.colorLine1,
    flexDirection: 'row',
  },
  viewWalletItem: {flex: 1, alignItems: 'center', justifyContent: 'center'},
  viewWalletItemBorder: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderStartWidth: 0.5,
    borderStartColor: colors.colorLine1,
    borderEndWidth: 0.5,
    borderEndColor: colors.colorLine1,
  },
  textWalletLabel: {
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2),
    marginTop: 10,
    marginBottom: 10,
    opacity: 0.6,
  },
  textWalletValue: {
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(3),
    marginBottom: 10,
  },
  textTransacationHistory: {
    flex: 1,
    color: colors.white,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Bold,
  },
  textViewAll: {
    color: colors.white,
    fontSize: RFPercentage(2.5),
    fontFamily: fonts.Dosis_Regular,
    alignSelf: 'baseline',
  },
  viewEmptyMain: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgReward: {
    height: wp(20),
    width: wp(20),
    marginBottom: 10,
    tintColor: colors.white,
  },
  imgNoReward: {
    height: wp(20),
    width: wp(20),
    marginBottom: 10,
    marginTop: hp(10),
    tintColor: colors.white,
    opacity: 0.6,
  },
  textNoRewards: {
    color: colors.white,
    textAlign: 'center',
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
    opacity: 0.6,
  },
  listContent: {
    height: '100%',
  },
});

export default WalletStyle;
