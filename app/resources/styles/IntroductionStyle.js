import {StyleSheet, Dimensions, Platform} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const IntroductionStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  viewSkip: {
    alignSelf: 'flex-end',
    flexDirection: 'row',
    marginEnd: '5%',
  },
  textSkip: {
    color: colors.white,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Bold,
  },
  viewSliderMain: {
    flex: 1,
    width: Dimensions.get('screen').width,
  },
  textTitle: {
    color: colors.white,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Bold,
    marginStart: wp(10),
    marginTop: Platform.OS === 'ios' ? hp(12) : hp(8),
  },
  textDescription: {
    color: colors.white,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Regular,
    marginStart: wp(10),
  },
});

export default IntroductionStyle;
