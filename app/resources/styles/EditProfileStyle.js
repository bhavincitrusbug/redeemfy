import {StyleSheet} from 'react-native';
import colors from '../colors';
import fonts from '../fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const EditProfileStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  row: {flexDirection: 'row'},
  rowCenter: {flexDirection: 'row', alignItems: 'center'},
  textHeader: {
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(2.5),
    color: colors.white,
    margin: 10,
    textAlign: 'center',
    flex: 1,
  },
  viewBack: {
    position: 'absolute',
    top: 0,
    start: 0,
    marginStart: 20,
    marginTop: 12,
  },
  imgBack: {width: wp(5), height: wp(5)},
  viewAbout: {
    backgroundColor: colors.colorTabBackground,
    borderRadius: 8,
    height: 80,
    paddingStart: 5,
    paddingTop: 5,
    marginBottom: hp(2),
  },
  inputAboutMe: {
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(3),
    marginBottom: 5,
  },
  viewButton: {
    marginTop: hp(5),
    alignSelf: 'center',
    marginBottom: hp(2),
  },
});

export default EditProfileStyle;
