import {StyleSheet} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const RewardDetailStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  viewHeader: {
    flexDirection: 'row',
    marginStart: wp(5),
    marginEnd: wp(5),
    marginTop: wp(5),
  },
  imgBack: {width: wp(5), height: wp(5)},
  viewMain: {
    marginStart: wp(5),
    marginEnd: wp(5),
    marginTop: hp(5),
  },
  viewRewardImg: {
    backgroundColor: 'rgba(255,255,255,0.10)',
    width: wp(90),
    height: wp(90),
    borderRadius: 6,
  },
  imgReward: {flex: 1},
  textTitle: {
    marginTop: 10,
    color: colors.white,
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(5),
  },
  textDescription: {
    marginTop: hp(2),
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(3),
    opacity: 0.5,
  },
  textTerms: {
    marginTop: hp(2),
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2),
  },
  viewButtonClaim: {position: 'absolute', start: 0, end: 0, bottom: 0},
});

export default RewardDetailStyle;
