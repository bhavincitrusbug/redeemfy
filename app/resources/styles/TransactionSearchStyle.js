import {StyleSheet} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const TransactionSearchStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  viewFlex: {flex: 1},
  viewMain: {
    flex: 1,
    marginTop: hp(5),
    marginStart: wp(5),
    marginEnd: wp(5),
  },
  viewHeader: {flexDirection: 'row', alignItems: 'center', margin: 10},
  textHeader: {
    flex: 1,
    color: colors.white,
    textAlign: 'center',
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Regular,
  },
  viewBack: {position: 'absolute', marginStart: wp(2)},
  rowCenter: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowItemLeft: {flex: 1, marginEnd: 10},
  rowItemRight: {flex: 1, marginStart: 10},
  textDropDown: {
    fontFamily: fonts.Dosis_Regular,
    color: colors.colorBackground,
    padding: 0,
  },
  viewButton: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: 0,
    marginBottom: hp(5),
  },
});

export default TransactionSearchStyle;
