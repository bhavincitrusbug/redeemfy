import {StyleSheet} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const BagTabStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  viewHeader: {flexDirection: 'row', alignItems: 'center', margin: wp(5)},
  viewBack: {position: 'absolute', start: 0},
  imgBack: {width: wp(6), height: wp(6)},
  textHeaderHome: {
    textAlign: 'center',
    flex: 1,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Bold,
    color: colors.white,
  },
  viewCountDown: {
    backgroundColor: colors.colorCountDownBackground,
    marginTop: hp(2),
    marginBottom: hp(2),
    borderRadius: 8,
  },
  listContent: {
    height: '100%',
  },
  viewEmptyMain: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgReward: {
    height: wp(20),
    width: wp(20),
    marginBottom: 10,
    tintColor: colors.white,
  },
  imgNoReward: {
    height: wp(20),
    width: wp(20),
    marginTop: hp(5),
    marginBottom: 10,
    tintColor: 'rgba(255,255,255,0.6)',
  },
  textNoRewards: {
    color: colors.white,
    textAlign: 'center',
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
    opacity: 0.6,
  },
});

export default BagTabStyle;
