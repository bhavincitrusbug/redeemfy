import {StyleSheet} from 'react-native';
import colors from '../colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import fonts from '../fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
const LoginPopUpStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  viewFlex: {flex: 1},
  viewGuest: {
    flexDirection: 'row',
    marginTop: hp(3),
    marginStart: wp(5),
  },
  imgBack: {
    width: wp(6),
    height: wp(6),
  },
  viewLogo: {
    alignItems: 'center',
    marginStart: wp(5),
    marginEnd: wp(5),
    marginTop: hp(8),
    marginBottom: hp(8),
  },
  imgLogo: {width: 274, height: 51},
  viewTabLabel: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '90%',
    alignSelf: 'center',
  },
  viewImgActive: {
    flex: 1,
    width: wp(50),
    height: 60,
  },
  viewImgInActive: {
    flex: 1,
    width: wp(40),
    height: 60,
  },
  imgTabLeft: {
    borderTopLeftRadius: 10,
  },
  textSignIn: {
    color: colors.white,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Bold,
    paddingTop: 20,
    paddingBottom: 20,
    textAlign: 'center',
  },
  imgTabRight: {
    borderTopRightRadius: 10,
  },
  textRegister: {
    color: colors.white,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Bold,
    paddingTop: 20,
    paddingBottom: 20,
    textAlign: 'center',
  },
  viewTabItem: {
    backgroundColor: colors.colorTabBackground,
    alignItems: 'center',
    width: '90%',
    alignSelf: 'center',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    marginBottom: hp(5),
  },
});

export default LoginPopUpStyle;
