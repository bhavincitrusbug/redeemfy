import {StyleSheet} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const EditBankDetailStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  viewHeader: {flexDirection: 'row', alignItems: 'center', margin: wp(5)},
  viewBack: {position: 'absolute'},
  imgBack: {width: wp(6), height: hp(6)},
  textHeaderBankDetail: {
    textAlign: 'center',
    flex: 1,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Bold,
    color: colors.white,
  },
  viewMain: {
    flex: 1,
    width: '90%',
    marginTop: hp(5),
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  viewRowCenter: {flexDirection: 'row', alignItems: 'center'},
  viewBtnSubmit: {marginTop: hp(5), alignSelf: 'center'},
  textDropDown: {
    fontFamily: fonts.Dosis_Regular,
    color: colors.colorBackground,
    padding: 0,
  },
  textTitle: {
    padding: 10,
    color: colors.white,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Regular,
  },
  textTerms: {
    color: '#F0F0F0',
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2),
  },
});

export default EditBankDetailStyle;
