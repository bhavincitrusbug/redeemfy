import {StyleSheet} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const HomeTabStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  viewFlex: {flex: 1},
  viewHeader: {flexDirection: 'row', alignItems: 'center', margin: wp(5)},
  viewBag: {position: 'absolute', end: 0},
  imgBag: {width: wp(6), height: wp(6)},
  textHeaderHome: {
    textAlign: 'center',
    flex: 1,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Bold,
    color: colors.white,
  },
  viewTabMain: {
    marginStart: wp(2),
    marginEnd: wp(2),
    marginBottom: hp(5),
  },
  listContent: {
    height: '100%',
  },
  viewEmptyMain: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: hp(50)
  },
  viewCountDown: {
    backgroundColor: colors.colorCountDownBackground,
    marginTop: hp(2),
    marginBottom: hp(2),
    borderRadius: 8,
  },
  imgReward: {
    height: wp(20),
    width: wp(20),
    marginBottom: 10,
    tintColor: colors.white,
  },
  imgNoReward: {
    height: wp(20),
    width: wp(20),
    marginBottom: 10,
    tintColor: colors.white,
    alignSelf: 'center'
  },
  textNoRewards: {
    color: colors.white,
    textAlign: 'center',
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
  },
});

export default HomeTabStyle;
