import {StyleSheet} from 'react-native';
import colors from '../colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import fonts from '../fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
const BuyPreviewStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  viewHeader: {flexDirection: 'row', alignItems: 'center', margin: wp(5)},
  textMainHeader: {
    flex: 1,
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(3),
    textAlign: 'center',
  },
  viewBack: {position: 'absolute'},
  row: {flexDirection: 'row'},
  rowCenter: {flexDirection: 'row', alignItems: 'center'},
  viewMain: {marginStart: wp(5), marginEnd: wp(5)},
  textHeader: {
    color: colors.white,
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
    opacity: 0.6,
  },
  viewWalletDetail: {
    backgroundColor: colors.white,
    borderRadius: 10,
    padding: 10,
    marginTop: hp(2),
    marginBottom: hp(2),
  },
  textItemFlex: {
    flex: 1,
    color: colors.colorBackground,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(3),
  },
  textItem: {
    color: colors.colorBackground,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(3),
  },
  viewPaymentDetail: {
    backgroundColor: colors.white,
    borderRadius: 10,
    padding: 10,
    marginTop: hp(2),
  },
  viewRowItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
  },
  viewRowDiscount: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
  },
  viewRowPayable: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
  },
  viewWalletAmount: {
    marginTop: hp(5),
    backgroundColor: colors.white,
    padding: 10,
    borderRadius: 10,
  },
  viewRowWalletAmount: {
    marginBottom: hp(2),
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewButton: {position: 'absolute', bottom: 0},
});

export default BuyPreviewStyle;
