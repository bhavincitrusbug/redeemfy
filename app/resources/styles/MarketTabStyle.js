import {StyleSheet} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const MarketTabStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  viewHeader: {flexDirection: 'row', alignItems: 'center', margin: wp(5)},
  viewBorderLine: {
    height: 1,
    backgroundColor: 'rgba(255,255,255,0.10)',
    marginBottom: 20,
  },
  listContent: {
    height: '100%',
  },
  viewBag: {position: 'absolute', end: 0},
  imgBag: {width: wp(6), height: wp(6)},
  viewSearchBackground: {
    marginBottom: hp(2),
    backgroundColor: '#1E252C',
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  imgSearch: {
    width: wp(5),
    height: wp(5),
    tintColor: colors.white,
    marginStart: 15,
  },
  inputSearch: {
    padding: 15,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Regular,
    color: colors.white,
  },
  textHeaderHome: {
    textAlign: 'center',
    flex: 1,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Bold,
    color: colors.white,
  },
  viewCountDown: {
    backgroundColor: colors.colorCountDownBackground,
    marginTop: hp(2),
    marginBottom: hp(2),
    borderRadius: 8,
  },
  viewList: {
    marginStart: wp(2),
    marginEnd: wp(2),
    marginBottom: hp(15),
  },
  listWrapper: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  viewBanner: {
    marginBottom: hp(2),
    borderRadius: 8,
    // height: hp(16),
    // width: wp(100)

  },
  viewEmptyMain: {
    marginTop: hp(5),
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgReward: {
    height: wp(20),
    width: wp(20),
    marginBottom: 10,
    tintColor: colors.white,
  },
  imgNoReward: {
    height: wp(20),
    width: wp(20),
    marginBottom: 10,
    tintColor: colors.white,
  },
  textNoRewards: {
    color: colors.white,
    textAlign: 'center',
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
  },
});

export default MarketTabStyle;
