import {StyleSheet, Dimensions} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const MarketListItemStyle = StyleSheet.create({
  viewListItem: {},
  viewImgItem: {
    flexDirection: 'row',
    backgroundColor: 'rgba(0,0,0,0.10)',
    borderRadius: 8,
    height: Dimensions.get('screen').width / 2,
    justifyContent: 'center',
  },
  viewImg: {
    width: wp(34),
    height: wp(38),
    alignSelf: 'center',
  },
  imgMarket: {flex: 1},
  viewDiscount: {
    position: 'absolute',
    top: 0,
    start: 0,
    borderRadius: 4,
    backgroundColor: colors.colorDiscountBackground,
  },
  textDiscount: {
    color: colors.white,
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
    paddingStart: 5,
    paddingEnd: 5,
  },
  viewLeftItem: {
    position: 'absolute',
    end: 0,
  },
  textLeft: {
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2),
    opacity: 0.5,
    marginTop: 2,
    marginEnd: 5,
  },
  viewTitleItem: {
    marginStart: 5,
    marginTop: 5,
  },
  textTitle: {
    color: colors.white,
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(3),
    marginTop: 5,
    marginBottom: 5,
  },
  textDescription: {
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2.5),
    opacity: 0.5,
  },
});

export default MarketListItemStyle;
