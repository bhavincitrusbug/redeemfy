import {StyleSheet} from 'react-native';
import colors from '../colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const UserPageStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground,
  },
  viewUserInfo: {
    marginStart: wp(5),
    marginEnd: wp(5),
    marginTop: hp(10),
    marginBottom: hp(10),
  },
  textName: {
    color: colors.white,
    fontFamily: fonts.Dosis_Bold,
    fontSize: RFPercentage(4),
  },
  textUserName: {
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2),
    marginTop: 5,
    marginBottom: 5,
  },
  textAboutUs: {
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2),
  },
  viewButtonEditProfile: {
    marginTop: hp(5),
    marginBottom: hp(10),
    alignSelf: 'center',
  },
  viewTouchInfo: {
    backgroundColor: colors.colorTabBackground,
    borderRadius: 8,
    shadowOffset: {
      height: 2,
      width: 2,
    },
    shadowColor: 'rgba(0,0,0,0.08)',
    padding: 10,
  },
  viewBorderLine: {
    marginTop: 5,
    marginBottom: 5,
    height: 1,
    backgroundColor: 'rgba(245,245,245,0.4)',
  },
  viewImg: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 5,
  },
  imgSwitch: {width: wp(10), height: hp(4)},
  textTitle: {
    marginStart: 10,
    color: colors.white,
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(2.5),
    flex: 1,
  },
  imgRight: {
    height: wp(4),
    width: wp(4),
    tintColor: colors.colorRightArrow,
  },
});

export default UserPageStyle;
