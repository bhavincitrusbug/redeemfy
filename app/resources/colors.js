const colors = {
  white: '#FFFFFF',
  black: '#000000',
  green: '#008000',
  colorBackground: '#2C3E50',
  colorNotification: '#2C3E50',
  colorTabBackground: '#1E252C',
  colorLabelInActive: ' rgba(255,255,255,0.50)',
  colorLabelActive: '#F5F5F5',
  colorLine: '#F5F5F5',
  colorLine1: '#757575',
  colorBtnGradientOne: '#003E7D',
  colorBtnGradientTwo: '#646AB1',
  colorTransparent: 'transparent',
  colorCountDownBackground: '#F7BB76',
  colorDiscountBackground: '#A0CADD',
  colorRightArrow: '#C2C4CA',
  colorTextNotification: '#ACB1C0',
  colorError: '#fc1f4a',
  lightGray: '#F5F5F5',
  gray: '#F1F1F1'
};
export default colors;
