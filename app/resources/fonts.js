const fonts = {
  Helvetica: 'Helvetica',
  Dosis_Light: 'Dosis-Light',
  Dosis_Regular: 'Dosis-Regular',
  Dosis_Bold: 'Dosis-Bold',
};
export default fonts;
