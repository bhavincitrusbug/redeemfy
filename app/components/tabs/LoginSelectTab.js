import React, {PureComponent} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Animated,
  Dimensions,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import colors from '../../resources/colors';
import {RFPercentage} from 'react-native-responsive-fontsize';
import fonts from '../../resources/fonts';
import icons from '../../resources/icons';

const {width} = Dimensions.get('screen');

class LoginSelectTab extends PureComponent {
  state = {
    activeTabOne: true,
    activeTabTwo: false,
    active: 0,
    xTabOne: 0,
    xTabTwo: 0,
    translateX: new Animated.Value(0),
    translateXTabOne: new Animated.Value(0),
    translateXTabTwo: new Animated.Value(width),
    translateY: -1000,
  };

  handleSlide = type => {
    let {
      active,
      xTabOne,
      xTabTwo,
      translateX,
      translateXTabOne,
      translateXTabTwo,
    } = this.state;
    Animated.spring(translateX, {
      toValue: type,
      duration: 100,
    }).start();
    if (active === 0) {
      Animated.parallel([
        Animated.spring(translateXTabOne, {
          toValue: 0,
          duration: 100,
        }).start(),
        Animated.spring(translateXTabTwo, {
          toValue: width,
          duration: 100,
        }).start(),
      ]);
    } else {
      Animated.parallel([
        Animated.spring(translateXTabOne, {
          toValue: -width,
          duration: 100,
        }).start(),
        Animated.spring(translateXTabTwo, {
          toValue: 0,
          duration: 100,
        }).start(),
      ]);
    }
  };

  render() {
    let {
      xTabOne,
      xTabTwo,
      translateX,
      active,
      activeTabOne,
      activeTabTwo,
      translateXTabOne,
      translateXTabTwo,
      translateY,
    } = this.state;
    const {tabOneTitle, tabTwoTitle, renderTabOne, renderTabTwo} = this.props;
    return (
      <View style={LoginSelectTabStyle.container}>
        <View style={LoginSelectTabStyle.viewTabOne}>
          <Animated.View
            style={[
              LoginSelectTabStyle.animatedViewTabOne,
              {
                transform: [
                  {
                    translateX,
                  },
                ],
              },
            ]}
          />

          <TouchableOpacity
            style={[
              LoginSelectTabStyle.tabOneTouch,
              {
                backgroundColor: activeTabOne
                  ? colors.colorTransparent
                  : colors.colorTransparent,
              },
            ]}
            onLayout={event =>
              this.setState({
                xTabOne: event.nativeEvent.layout.x,
              })
            }
            onPress={() =>
              this.setState(
                {activeTabOne: true, activeTabTwo: false, active: 0},
                () => this.handleSlide(xTabOne),
              )
            }>
            <ImageBackground
              style={{flex: 1}}
              source={
                activeTabOne ? icons.BG_TAB_1_ACTIVE : icons.BG_TAB_1_IN_ACTIVE
              }>
              <View>
                <Text style={LoginSelectTabStyle.textTabOne}>
                  {tabOneTitle}
                </Text>
              </View>
            </ImageBackground>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              LoginSelectTabStyle.tabTwoTouch,
              {
                backgroundColor: activeTabTwo
                  ? colors.colorTransparent
                  : colors.colorTransparent,
              },
            ]}
            onLayout={event =>
              this.setState({
                xTabTwo: event.nativeEvent.layout.x,
              })
            }
            onPress={() =>
              this.setState(
                {activeTabOne: false, activeTabTwo: true, active: 1},
                () => this.handleSlide(xTabTwo),
              )
            }>
            <ImageBackground
              style={{flex: 1}}
              source={
                activeTabTwo ? icons.BG_TAB_2_ACTIVE : icons.BG_TAB_2_IN_ACTIVE
              }>
              <View>
                <Text style={LoginSelectTabStyle.textTabTwo}>
                  {tabTwoTitle}
                </Text>
              </View>
            </ImageBackground>
          </TouchableOpacity>
        </View>

        <Animated.View
          style={[
            {
              transform: [
                {
                  translateX: translateXTabOne,
                },
              ],
            },
          ]}
          onLayout={event =>
            this.setState({
              translateY: event.nativeEvent.layout.height,
            })
          }>
          {renderTabOne()}
        </Animated.View>

        <Animated.View
          style={[
            {
              transform: [
                {
                  translateX: translateXTabTwo,
                },
                {
                  translateY: -translateY,
                },
              ],
            },
          ]}>
          {renderTabTwo()}
        </Animated.View>
      </View>
    );
  }
}

const LoginSelectTabStyle = StyleSheet.create({
  container: {
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: colors.colorTabBackground,
    marginStart: 10,
    marginEnd: 10,
  },
  viewTabOne: {
    flexDirection: 'row',
    position: 'relative',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    height: 'auto',
    flex: 0,
  },
  animatedViewTabOne: {
    position: 'absolute',
    width: '50%',
    height: '100%',
    top: 0,
    left: 0,
    borderTopLeftRadius: 10,
  },
  tabOneTouch: {
    flex: 1,
    borderTopLeftRadius: 10,
  },
  tabTwoTouch: {
    flex: 1,
    borderTopRightRadius: 10,
  },
  textTabOne: {
    color: colors.white,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Bold,
    paddingTop: 20,
    paddingBottom: 20,
    textAlign: 'center',
  },
  textTabTwo: {
    color: colors.white,
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Bold,
    paddingTop: 20,
    paddingBottom: 20,
    textAlign: 'center',
  },
});
export default LoginSelectTab;
