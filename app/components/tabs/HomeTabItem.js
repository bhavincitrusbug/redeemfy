import React, {PureComponent} from 'react';
import {View, TouchableOpacity, StyleSheet, Image} from 'react-native';
import icons from '../../resources/icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../resources/colors';
class HomeTabItem extends PureComponent {
  onSelectItem = routeName => {
    this.props.onPress(routeName);
  };
  render() {
    const {index, focused, routeName} = this.props;
    let iconName = '';

    switch (index) {
      case 0:
        iconName = focused ? icons.TAB_HOME : icons.TAB_HOME;
        break;

      case 1:
        iconName = focused ? icons.TAB_MARKET : icons.TAB_MARKET;
        break;

      case 2:
        iconName = focused ? icons.TAB_USER : icons.TAB_USER;
        break;

      default:
        iconName = focused ? icons.TAB_HOME : icons.TAB_HOME;
    }
    return (
      <TouchableOpacity onPress={() => this.onSelectItem(routeName)}>
        <View style={styles.container}>
          <Image
            source={iconName}
            style={{
              height: wp(6),
              width: wp(6),
              tintColor: focused ? colors.white : 'rgba(255,255,255,0.3)',
            }}
            resizeMode="contain"
          />
          <View
            style={[
              styles.viewActiveDot,
              {
                backgroundColor: focused
                  ? colors.white
                  : colors.colorTransparent,
              },
            ]}
          />
        </View>
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingStart: 20,
    paddingEnd: 20,
  },
  viewActiveDot: {
    height: 5,
    width: 5,
    borderRadius: 5,
    margin: 5,
  },
});
export default HomeTabItem;
