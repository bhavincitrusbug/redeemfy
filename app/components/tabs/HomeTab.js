import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {View, StyleSheet} from 'react-native';
import colors from '../../resources/colors';
import HomeTabItem from './HomeTabItem';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../../resources/constants';
import LoginAlert from '../alert/LoginAlert';
class HomeTab extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoginAlert: false,
    };
  }
  navigationHandler = routeName => {
    if (routeName === 'UserTab') {
      AsyncStorage.getItem(constants.IS_LOGIN).then(value => {
        if (value && value === 'true') {
          this.setState({isLoginAlert: false});
          this.props.navigation.navigate(routeName);
        } else {
          this.setState({isLoginAlert: true});
        }
      });
    } else {
      this.props.navigation.navigate(routeName);
    }
  };
  componentDidUpdate(prevProps) {
    if (this.props.currentUser !== prevProps.currentUser) {
      this.setState({currentUser: this.props.currentUser}, () => {
        this.props.navigation.navigate('UserTab');
      });
    }
  }
  render() {
    const {navigation} = this.props;
    const routes = navigation.state.routes;

    return (
      <View style={styles.viewTabItem}>
        {routes.map((route, index) => {
          return (
            <View style={styles.viewTabRow} key={route.key}>
              <HomeTabItem
                key={route.key}
                routeName={route.routeName}
                onPress={() => this.navigationHandler(route.routeName)}
                focused={navigation.state.index === index}
                index={index}
                route={route}
                currentLang={this.props.currentLang}
              />
            </View>
          );
        })}
        {this.state.isLoginAlert ? (
          <LoginAlert
            isLoginAlert={this.state.isLoginAlert}
            navigation={this.props.navigation}
            doNoClick={value => {
              this.setState({isLoginAlert: false});
            }}
          />
        ) : (
          undefined
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  viewTabItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.colorTabBackground,
  },
  viewTabRow: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 5,
  },
});

function mapStateToProps(state) {
  return {
    currentUser: state.app.currentUser,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({}, dispatch),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(HomeTab);
