import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Animated,
  Dimensions,
  StyleSheet,
} from 'react-native';
import colors from '../../resources/colors';
import { RFPercentage } from 'react-native-responsive-fontsize';
import fonts from '../../resources/fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import OfferTabButtonGradient from '../buttons/OfferTabButtonGradient';
const { width } = Dimensions.get('window');

class WalletTab extends PureComponent {
  state = {
    activeTabOne: true,
    activeTabTwo: false,
    active: 0,
    xTabOne: 0,
    xTabTwo: 0,
    translateX: new Animated.Value(0),
    translateXTabOne: new Animated.Value(0),
    translateXTabTwo: new Animated.Value(width),
    translateY: -1000,
  };

  handleSlide = (type, title) => {
    this.props.getTransacationList(title);
    let {
      active,
      xTabOne,
      xTabTwo,
      translateX,
      translateXTabOne,
      translateXTabTwo,
    } = this.state;
    Animated.spring(translateX, {
      toValue: type,
      duration: 100,
    }).start();
    if (active === 0) {
      Animated.parallel([
        Animated.spring(translateXTabOne, {
          toValue: 0,
          duration: 100,
        }).start(),
        Animated.spring(translateXTabTwo, {
          toValue: width,
          duration: 100,
        }).start(),
      ]);
    } else {
      Animated.parallel([
        Animated.spring(translateXTabOne, {
          toValue: -width,
          duration: 100,
        }).start(),
        Animated.spring(translateXTabTwo, {
          toValue: 0,
          duration: 100,
        }).start(),
      ]);
    }
  };
  renderActiveTab(title) {
    return (
      <View>
        <OfferTabButtonGradient
          title={title}
          backgroundColor={colors.orange}
          fontFamily={fonts.Dosis_Bold}
          fontSize={RFPercentage(3)}
          width={wp(44)}
          height={hp(5)}
          currentLang={this.state.currentLang}
          borderRadius={30}
          isFlex={true}
        />
      </View>
    );
  }
  renderInActiveTab(title) {
    return (
      <View style={LoginSelectTabStyle.viewTabText}>
        <Text style={LoginSelectTabStyle.textTab}>{title}</Text>
      </View>
    );
  }
  render() {
    let {
      xTabOne,
      xTabTwo,
      translateX,
      active,
      activeTabOne,
      activeTabTwo,
      translateXTabOne,
      translateXTabTwo,
      translateY,
    } = this.state;
    const { tabOneTitle, tabTwoTitle, renderTabOne, renderTabTwo } = this.props;
    return (
      <View style={LoginSelectTabStyle.container}>
        <View style={LoginSelectTabStyle.viewTabOne}>
          <View
            style={LoginSelectTabStyle.animatedViewTabOne}
          />
          <TouchableOpacity
            style={LoginSelectTabStyle.tabOneTouch}

            onPress={() =>
              this.setState(
                { activeTabOne: true, activeTabTwo: false, active: 0 },
                () => this.handleSlide(xTabOne, tabOneTitle),
              )
            }>
            {activeTabOne
              ? this.renderActiveTab(tabOneTitle)
              : this.renderInActiveTab(tabOneTitle)}
          </TouchableOpacity>
          <TouchableOpacity
            style={LoginSelectTabStyle.tabTwoTouch}

            onPress={() =>
              this.setState(
                { activeTabOne: false, activeTabTwo: true, active: 1 },
                () => this.handleSlide(xTabTwo, tabTwoTitle),
              )
            }>
            {activeTabTwo
              ? this.renderActiveTab(tabTwoTitle)
              : this.renderInActiveTab(tabTwoTitle)}
          </TouchableOpacity>
        </View>

        {activeTabOne ? renderTabOne() : null}
        {activeTabTwo ? renderTabTwo() : null}
      </View>
    );
  }
}

const LoginSelectTabStyle = StyleSheet.create({
  container: {
    marginStart: 10,
    marginEnd: 10,
    flex: 1,
  },
  viewTabOne: {
    flexDirection: 'row',
    position: 'relative',
    borderRadius: 30,
    borderWidth: 1,
    backgroundColor: colors.colorTabBackground,
    borderColor: 'rgba(255,255,255,0.4)',
    padding: 1.5,
  },
  animatedViewTabOne: {
    position: 'absolute',
    width: '50%',
    top: 0,
    left: 0,
  },
  tabOneTouch: {
    flex: 1,
    backgroundColor: colors.colorTabBackground,
    borderRadius: 30,
    margin: 5,
  },
  tabTwoTouch: {
    flex: 1,
    backgroundColor: colors.colorTabBackground,
    borderRadius: 30,
    margin: 5,
  },
  viewTabText: { height: hp(5), alignItems: 'center', justifyContent: 'center' },
  textTab: {
    color: 'rgba(255,255,255,0.5)',
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Bold,
    textAlign: 'center',
  },
});
export default WalletTab;
