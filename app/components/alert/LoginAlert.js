import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {
  Text,
  View,
  Modal,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
import {getLangValue} from '../../resources/languages/language';
import strings from '../../resources/strings';
import colors from '../../resources/colors';
import fonts from '../../resources/fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
import ButtonGradient from '../buttons/ButtonGradient';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
class LoginAlert extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      title: 'Login-Register!!',
      message:
        'You need to Login/Register to access/use this. \nDo you want to Login/Register now?',
      buttonYes: getLangValue(strings.BTN_LOGIN_REGISTER, props.currentLang),
      buttonNo: getLangValue(strings.REMIND_ME_LATER, props.currentLang),
    };
  }

  doYesClick() {
    this.props.doNoClick(false);
    clearTimeout(this.alertTime);
    this.alertTime = setTimeout(() => {
      this.props.navigation.navigate('LoginPopUp');
    }, 200);
  }

  hideSellModal() {
    this.props.doNoClick(false);
  }
  componentDidUpdate(prevProps) {
    if (this.props.isLoginAlert !== prevProps.isLoginAlert) {
      this.setState({isLoginAlert: this.props.isLoginAlert});
    }
  }
  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          presentationStyle="overFullScreen"
          transparent={true}
          onRequestClose={() => this.hideSellModal()}
          visible={this.props.isLoginAlert}>
          <TouchableHighlight
            style={{
              flex: 1,
            }}
            underlayColor={colors.colorTransparent}
            onPress={() => this.hideSellModal()}>
            <View
              style={{
                backgroundColor: 'rgba(0,0,0,0.30)',
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  width: wp(90),
                  height: hp(35),
                  backgroundColor: colors.white,
                  borderRadius: 10,
                  padding: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: fonts.Dosis_Bold,
                    fontSize: RFPercentage(3),
                    color: colors.black,
                    textAlign: 'center',
                    marginBottom: hp(5),
                  }}>
                  {this.state.title}
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.Dosis_Regular,
                    fontSize: RFPercentage(2),
                    color: colors.black,
                    textAlign: 'center',
                    marginBottom: hp(5),
                  }}>
                  {this.state.message}
                </Text>
                <View style={{alignSelf: 'center', marginBottom: hp(2)}}>
                  <ButtonGradient
                    title={this.state.buttonYes}
                    backgroundColor={colors.orange}
                    fontFamily={fonts.Dosis_Bold}
                    fontSize={RFPercentage(2.5)}
                    onPress={() => this.doYesClick()}
                    width={wp(80)}
                    height={hp(6)}
                    currentLang={this.state.currentLang}
                    borderRadius={23}
                  />
                </View>
                <TouchableOpacity onPress={() => this.hideSellModal()}>
                  <Text
                    style={{
                      color: colors.colorBackground,
                      fontSize: RFPercentage(2),
                      fontFamily: fonts.Dosis_Bold,
                    }}>
                    {getLangValue(strings.REMIND_ME_LATER, strings.LAG_ENG)}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </TouchableHighlight>
        </Modal>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    currentLang: state.app.currentLang,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({}, dispatch),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginAlert);
