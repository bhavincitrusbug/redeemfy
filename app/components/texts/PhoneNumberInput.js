import React, {PureComponent} from 'react';
import {View, TextInput, Text, StyleSheet} from 'react-native';
import fonts from '../../resources/fonts';
import colors from '../../resources/colors';
import strings from '../../resources/strings';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {getErrorValue, getLangValue} from '../../resources/languages/language';
import errors from '../../resources/errors';
import {phoneNumberFormat} from '../../resources/validation';
import * as data from '../../resources/countries.json';
import RNPickerSelect from 'react-native-picker-select';
import PhoneNumberInputStyle from '../../resources/styles/PhoneNumberInputStyle';
class PhoneNumberInput extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentLang: props.currentLang,
      isPhoneNumberFocus: false,
      countryCode: '+65',
      phone_number: '',
      err_Phone_Number: undefined,
      countries: [],
    };
  }
  componentDidMount() {
    // let result = data.countries.map(data => {
    //   return {
    //     value: data.value,
    //     label: data.value,
    //   };
    // });
    this.setState({countries: data.countries});
  }
  updateState(country_code, phoneNumber, errPhoneNumber) {
    if (
      country_code === undefined &&
      phoneNumber === '' &&
      errPhoneNumber === undefined
    ) {
      this.setState({
        err_Phone_Number: this.state.isPhoneNumberFocus
          ? getErrorValue(errors.ENTER_PHONE_NUMBER, this.state.currentLang)
          : undefined,
      });
    } else {
      this.setState({
        countryCode: country_code,
        phone_number: phoneNumber,
        err_Phone_Number: errPhoneNumber,
      });
    }
  }
  onChangeText = text => {
    if (this.props.onNumberChange !== undefined) {
      this.props.onNumberChange(text);
    }

    this.setState(
      {
        phone_number: phoneNumberFormat(text.replace(/[^0-9]/g, '')),
      },
      () => {
        this.state.phone_number === ''
          ? this.setState({
              err_Phone_Number: getErrorValue(
                errors.ENTER_PHONE_NUMBER,
                this.state.currentLang,
              ),
            })
          : this.setState({err_Phone_Number: undefined});
      },
    );
  };
  onValueChange = (value, index) => {
    this.setState({countryCode: value});
  };

  render() {
    const {countryCode, phone_number, err_Phone_Number} = this.state;
    const placeholder = {
      label:
        countryCode !== undefined
          ? countryCode
          : getLangValue(strings.COUNTRY_CODE, this.state.currentLang),
      value: undefined,
      color: colors.colorBackground,
    };

    const {onComplete, label, labelColor, onNumberChange} = this.props; //
    onComplete(countryCode, phone_number, err_Phone_Number, this.phoneInput);

    return (
      <View>
        <View
          style={
            this.state.err_Phone_Number !== undefined
              ? PhoneNumberInputStyle.viewBorderError
              : PhoneNumberInputStyle.viewBorderNormal
          }>
          <View>
            <RNPickerSelect
              disabled={false}
              placeholder={placeholder}
              items={this.state.countries}
              onValueChange={(value, index) => this.onValueChange(value, index)}
              style={
                this.state.isPhoneNumberFocus
                  ? pickerSelectStyles
                  : pickerStyles
              }
              value={countryCode}
              useNativeAndroidPickerStyle={false}
              ref={el => {
                this.inputRefs = el;
              }}>
              <TextInput
                ref={input => (this.phoneInput = input)}
                fontFamily={fonts.Dosis_Regular}
                placeholder={placeholder.label}
                placeholderTextColor={placeholder.color}
                value={
                  this.state.countryCode !== null &&
                  this.state.countryCode !== undefined
                    ? this.state.countryCode
                    : placeholder.label
                }
                underlineColorAndroid={colors.colorTransparent}
                keyboardType="numeric"
                returnKeyType="done"
                editable={false}
                style={[
                  PhoneNumberInputStyle.inputNumber,
                  {
                    color:
                      this.state.countryCode !== null &&
                      this.state.countryCode !== undefined
                        ? colors.white
                        : colors.colorLabelInActive,
                  },
                ]}
              />
            </RNPickerSelect>
          </View>
          <View style={PhoneNumberInputStyle.viewInput}>
            <TextInput
              ref={input => (this.phoneInput = input)}
              placeholder={label}
              placeholderTextColor={labelColor}
              fontFamily={fonts.Dosis_Regular}
              value={this.state.phone_number}
              onChangeText={text => {
                this.onChangeText(text);
              }}
              underlineColorAndroid={colors.colorTransparent}
              keyboardType="numeric"
              returnKeyType="done"
              onSubmitEditing={() => {
                this.state.phoneNumber === ''
                  ? this.setState({
                      err_Phone_Number: getErrorValue(
                        errors.ENTER_PHONE_NUMBER,
                        this.state.currentLang,
                      ),
                    })
                  : this.setState({err_Phone_Number: undefined});
              }}
              onBlur={e => {
                this.setState({isPhoneNumberFocus: false});
              }}
              onFocus={e => {
                this.setState({isPhoneNumberFocus: true});
              }}
              style={PhoneNumberInputStyle.inputNumber}
              maxLength={12}
            />
          </View>
        </View>
        <Text style={PhoneNumberInputStyle.textError}>
          {this.state.err_Phone_Number}
        </Text>
      </View>
    );
  }
}

const pickerStyles = StyleSheet.create({
  inputIOS: {
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Regular,
    paddingVertical: 5,
    paddingHorizontal: 0,
    color: colors.colorLabelInActive,
    paddingRight: 0,
  },
  inputAndroid: {
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(3),
    paddingVertical: 5,
    paddingHorizontal: 0,
    color: colors.colorLabelInActive,
    paddingRight: 0,
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: RFPercentage(3),
    fontFamily: fonts.Dosis_Regular,
    paddingVertical: 5,
    paddingHorizontal: 0,
    color: colors.white,
    paddingRight: 0,
  },
  inputAndroid: {
    fontFamily: fonts.Dosis_Regular,
    fontSize: RFPercentage(3),
    paddingVertical: 5,
    paddingHorizontal: 0,
    color: colors.white,
    paddingRight: 0, // to ensure the text is never behind the icon
    borderBottomColor: colors.colorLine,
    borderBottomWidth: 2,
  },
});
export default PhoneNumberInput;
