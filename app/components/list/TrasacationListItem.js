import React, {PureComponent} from 'react';
import {View, Text, Image} from 'react-native';
import icons from '../../resources/icons';
import {getLangValue} from '../../resources/languages/language';
import strings from '../../resources/strings';
import TransactionListItemStyle from '../../resources/styles/TransactionListItemStyle';
import ReadMore from '../texts/ReadMore';
import colors from '../../resources/colors';
import fonts from '../../resources/fonts';
class TrasacationListItem extends PureComponent {
  renderPrice(payment_types, price) {
    if (payment_types === 'sale') {
      return (
        <View>
          <Text style={TransactionListItemStyle.textPrice}>+${price}</Text>
        </View>
      );
    } else if (payment_types === 'buy') {
      return (
        <View>
          <Text style={TransactionListItemStyle.textPriceBuy}>-${price}</Text>
        </View>
      );
    } else {
      return (
        <View>
          <Text style={TransactionListItemStyle.textPrice}>+${price}</Text>
        </View>
      );
    }
  }
  renderStatus(transacationType, price) {
    if (transacationType === '0') {
      return (
        <View style={TransactionListItemStyle.viewRejected}>
          <Text style={TransactionListItemStyle.textRejected}>
            {getLangValue(strings.REJECTED, this.props.currentLang)}
          </Text>
        </View>
      );
    } else if (transacationType === '1') {
      return (
        <View>
          <Text style={TransactionListItemStyle.textPrice}>+${price}</Text>
        </View>
      );
      // return (
      //   <View style={TransactionListItemStyle.viewCompleted}>
      //     <Text style={TransactionListItemStyle.textCompleted}>
      //       {getLangValue(strings.COMPLETED, this.props.currentLang)}
      //     </Text>
      //   </View>
      // );
    } else if (transacationType === '2') {
      return (
        <View style={TransactionListItemStyle.viewProcessing}>
          <Text style={TransactionListItemStyle.textProcessing}>
            {getLangValue(strings.PROCESSING, this.props.currentLang)}
          </Text>
        </View>
      );
    } else {
      return (
        <View>
          <Text style={TransactionListItemStyle.textPrice}>+${price}</Text>
        </View>
      );
    }
  }
  renderTransaction() {
    const {title, date, price, image, payment_types} = this.props;
    return (
      <View style={TransactionListItemStyle.viewListItem}>
        <View>
          <Image
            source={{uri: image}}
            style={TransactionListItemStyle.imgCoupon}
            resizeMode="cover"
          />
        </View>
        <View style={TransactionListItemStyle.viewTextItem}>
          <View style={TransactionListItemStyle.viewTitle}>
            <View style={{flex: 1}}>
              <ReadMore
                numberOfLines={2}
                ellipsizeMode="head"
                button={{
                  color: colors.white,
                  fontFamily: fonts.Dosis_Regular,
                }}>
                <Text style={TransactionListItemStyle.textTitle}>{title}</Text>
              </ReadMore>
            </View>
            <Image
              source={icons.RIGHT}
              style={TransactionListItemStyle.imgRight}
            />
          </View>
          <View style={TransactionListItemStyle.viewDate}>
            <Text style={TransactionListItemStyle.textDate}>{date}</Text>
            {this.renderPrice(payment_types, price)}
          </View>
        </View>
      </View>
    );
  }
  renderCashout() {
    const {title, date, price, transacationType} = this.props;
    return (
      <View style={TransactionListItemStyle.viewListItem}>
        <View style={TransactionListItemStyle.viewTextItem}>
          <View style={TransactionListItemStyle.viewTitle}>
            <Text style={TransactionListItemStyle.textTitle}>{title}</Text>
            <Text style={TransactionListItemStyle.textPrice}>${price}</Text>
            <Image
              source={icons.RIGHT}
              style={TransactionListItemStyle.imgRight}
            />
          </View>
          <View style={TransactionListItemStyle.viewDate}>
            <Text style={TransactionListItemStyle.textDate}>{date}</Text>
            {this.renderStatus(transacationType, price)}
          </View>
        </View>
      </View>
    );
  }
  render() {
    const {type} = this.props;

    return (
      <View>
        {type === '' ? this.renderTransaction() : this.renderCashout()}
      </View>
    );
  }
}
export default TrasacationListItem;
