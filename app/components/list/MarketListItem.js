import React, {PureComponent} from 'react';
import {View, Text, Image, Platform} from 'react-native';
import MarketListItemStyle from '../../resources/styles/MarketListItemStyle';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../resources/colors';
class MarketListItem extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      textHeight: Platform.OS === 'ios' ? hp(8) : hp(9),
    };
  }
  render() {
    const {image_url, title, description, left, discount, is_redeem} = this.props;

    return (
      <View style={MarketListItemStyle.viewListItem}>
        <View style={MarketListItemStyle.viewImgItem}>
          <View style={MarketListItemStyle.viewImg}>
            <Image
              source={{uri: image_url}}
              resizeMode="cover"
              style={MarketListItemStyle.imgMarket}
            />
          </View>
          {discount === 0 ? (
            undefined
          ) : (
            <View style={MarketListItemStyle.viewDiscount}>
              <Text style={MarketListItemStyle.textDiscount}>-{discount}%</Text>
            </View>
          )}

          <View style={MarketListItemStyle.viewLeftItem}>
            <Text style={MarketListItemStyle.textLeft}>{left}</Text>
          </View>
        </View>
        <View style={MarketListItemStyle.viewTitleItem}>
          {/* <ReadMore
            numberOfLines={1}
            ellipsizeMode="head"
            button={{color: colors.white, fontFamily: fonts.Dosis_Regular}}>
            
          </ReadMore> */}
          <View>
            <Text style={MarketListItemStyle.textTitle} numberOfLines={2}>
              {title}
            </Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text style={MarketListItemStyle.textDescription}>
              {description}
            </Text>
            {is_redeem &&
            <Text style={[MarketListItemStyle.textDescription, {flex: 1, textAlign: 'right', opacity: 1}]} numberOfLines={2}>
              REDEEMED
            </Text>
            }
          </View>
        </View>
      </View>
    );
  }
}
export default MarketListItem;
