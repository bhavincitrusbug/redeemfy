import React, {PureComponent} from 'react';
import {View, Text, Image} from 'react-native';
import ProgressBar from '../progress/ProgressBar';
import {getPercentage, add3Dots} from '../../resources/validation';
import RewardListItemStyle from '../../resources/styles/RewardListItemStyle';
import ReadMore from '../texts/ReadMore';
import colors from '../../resources/colors';
import fonts from '../../resources/fonts';
class RewardListItem extends PureComponent {
  render() {
    const {image_url, title, description, total, used, leftItem, is_redeem} = this.props;
    return (
      <View style={RewardListItemStyle.viewListItem}>
        <View>
          <Image
            source={{uri: image_url}}
            style={RewardListItemStyle.imgReward}
            resizeMode="cover"
          />
        </View>
        <View style={RewardListItemStyle.viewTextItem}>
          {/* <ReadMore
            numberOfLines={2}
            ellipsizeMode="head"
            button={{color: colors.white, fontFamily: fonts.Dosis_Regular}}>
            
          </ReadMore> */}
          <Text numberOfLines={2} style={RewardListItemStyle.textTitle}>
            {title}
          </Text>
          <View style={{flexDirection: 'row'}}>
          <Text
            style={RewardListItemStyle.textDescription}
            ellipsizeMode="head">
            {description}
          </Text>
          {is_redeem && <Text numberOfLines={2} style={[RewardListItemStyle.textTitle, {textAlign: 'right', flex:1}]}>
            REDEEMED
          </Text>}
          </View>
          

          <View style={RewardListItemStyle.viewRowCenter}>
            <View style={{flex: 1}}>
              <ProgressBar
                reachedBarHeight={4}
                borderRadius={8}
                reachedBarColor={'rgba(255,255,255,0.10)'}
                unreachedBarColor="#4A90E2"
                unreachedBarHeight={4}
                value={getPercentage(used, total)}
              />
            </View>
            <Text style={RewardListItemStyle.textLeft}>{leftItem}</Text>
          </View>
        </View>
      </View>
    );
  }
}
export default RewardListItem;
