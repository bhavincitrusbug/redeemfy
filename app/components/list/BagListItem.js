import React, {PureComponent} from 'react';
import {View, Text, Image} from 'react-native';
import BagListItemStyle from '../../resources/styles/BagListItemStyle';
import ReadMore from '../texts/ReadMore';
import colors from '../../resources/colors';
import fonts from '../../resources/fonts';
class BagListItem extends PureComponent {
  render() {
    const {image_url, title, description, quantity} = this.props;
    return (
      <View style={BagListItemStyle.viewListItem}>
        <View>
          <Image
            source={{uri: image_url}}
            style={BagListItemStyle.imgReward}
            resizeMode="cover"
          />
        </View>
        <View style={BagListItemStyle.viewTextItem}>
          <ReadMore
            numberOfLines={2}
            ellipsizeMode="head"
            button={{color: colors.white, fontFamily: fonts.Dosis_Regular}}>
            <Text style={BagListItemStyle.textTitle} ellipsizeMode="head">
              {title}
            </Text>
          </ReadMore>

          <View style={BagListItemStyle.viewRowCenter}>
            <Text style={BagListItemStyle.textDescription}>{description}</Text>
            <View style={BagListItemStyle.viewTextQuantity}>
              <Text style={BagListItemStyle.textQuantity}>{quantity}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
export default BagListItem;
