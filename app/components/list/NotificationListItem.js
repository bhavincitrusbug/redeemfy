import React, {PureComponent} from 'react';
import {View, Text} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../resources/colors';
import fonts from '../../resources/fonts';
import {RFPercentage} from 'react-native-responsive-fontsize';
class NotificationListItem extends PureComponent {
  render() {
    const {title, body, date} = this.props;
    return (
      <View
        style={{
          marginBottom: hp(2),
          marginStart: wp(2),
          marginEnd: wp(2),
        }}>
        <Text
          style={{
            color: colors.white,
            fontFamily: fonts.Dosis_Regular,
            fontSize: RFPercentage(3),
          }}>
          {title}
        </Text>
        <Text
          style={{
            color: colors.white,
            fontFamily: fonts.Dosis_Regular,
            fontSize: RFPercentage(2.8),
            marginTop: 5,
            marginBottom: 5,
          }}>
          {body}
        </Text>
        <Text
          style={{
            color: colors.colorTextNotification,
            fontFamily: fonts.Dosis_Regular,
            fontSize: RFPercentage(2),
          }}>
          {date}
        </Text>
      </View>
    );
  }
}
export default NotificationListItem;
