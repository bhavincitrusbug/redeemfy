import React from 'react';
import {View} from 'react-native';

import {BallIndicator} from 'react-native-indicators';
import colors from '../../resources/colors';
const Loader = props => (
  <View
    style={{
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0,0,0,0.5)',
    }}>
    <BallIndicator color={colors.white} count={10} />
  </View>
);

export default Loader;
