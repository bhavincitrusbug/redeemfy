import React from 'react';
import {View, StyleSheet, Animated} from 'react-native';

class ProgressBar extends React.PureComponent {
  static defaultProps = {
    value: 0,
    borderRadius: 0,
    reachedBarColor: '#5E8CAD',
    reachedBarHeight: 2,
    unreachedBarColor: '#CFCFCF',
    unreachedBarHeight: 1,
    showValue: true,
  };

  constructor(props) {
    super(props);
    this.onLayout = this.onLayout.bind(this);
    this.setValue = this.setValue.bind(this);

    this.reachedWidth = new Animated.Value(0);

    this.state = {
      value: 0,
    };
  }

  setValue(_value) {
    if (_value < 0) _value = 0;
    if (_value > 100) _value = 100;

    this.setState({
      value: _value,
    });

    const _reachedWidth = (this.width * _value) / 100;

    const _self = this;
    Animated.timing(_self.reachedWidth, {
      toValue: _reachedWidth,
      duration: 300,
    }).start();
  }

  componentDidMount() {
    this.reachedWidth.addListener(({value}) => {
      const w = this.reachedWidth.__getValue();
      this.refReachedBarView.setNativeProps({style: {width: w}});
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.value !== prevProps.value) {
      this.setValue(this.props.value);
    }
  }

  onLayout(event) {
    this.width = event.nativeEvent.layout.width;
    this.setValue(this.props.value);
  }

  render() {
    const {
      reachedBarHeight,
      reachedBarColor,
      borderRadius,
      unreachedBarColor,
      unreachedBarHeight,
    } = this.props;
    return (
      <View
        onLayout={this.onLayout}
        style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.unreached,
            {
              backgroundColor: unreachedBarColor,
              height: unreachedBarHeight,
              borderTopRightRadius: borderRadius,
              borderBottomRightRadius: borderRadius,
            },
          ]}
        />
        <View
          ref={component => (this.refReachedBarView = component)}
          style={{
            height: reachedBarHeight,
            backgroundColor: reachedBarColor,
            borderTopLeftRadius: borderRadius,
            borderBottomLeftRadius: borderRadius,
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  unreached: {
    flex: 1,
  },
  value: {
    fontSize: 11,
    textAlign: 'center',
  },
});
export default ProgressBar;
