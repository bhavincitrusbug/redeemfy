import React, {PureComponent} from 'react';
import {View, Text} from 'react-native';
import colors from '../../resources/colors';
import LinearGradient from 'react-native-linear-gradient';
class OfferTabButtonGradient extends PureComponent {
  render() {
    const {
      title,
      fontFamily,
      fontSize,
      height,
      width,
      borderRadius,
      isFlex,
    } = this.props;
    return (
      <View>
        <LinearGradient
          colors={[colors.colorBtnGradientOne, colors.colorBtnGradientTwo]}
          useAngle={true}
          start={{x: 1, y: 0}}
          end={{x: 1, y: 0}}
          angle={90}
          style={
            isFlex
              ? {
                  height: height,
                  flex: 1,
                  borderRadius: borderRadius === undefined ? 23 : borderRadius,
                  alignItems: 'center',
                  justifyContent: 'center',
                }
              : {
                  height: height,
                  width: width,
                  borderRadius: borderRadius === undefined ? 23 : borderRadius,
                  alignItems: 'center',
                  justifyContent: 'center',
                }
          }>
          <View>
            <Text
              style={{
                color: colors.white,
                fontFamily: fontFamily,
                fontSize: fontSize,
                alignSelf: 'center',
              }}>
              {title}
            </Text>
          </View>
        </LinearGradient>
      </View>
    );
  }
}
export default OfferTabButtonGradient;
