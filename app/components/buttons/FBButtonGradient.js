import React, { PureComponent } from "react";
import { View, Text, TouchableOpacity, Image, Platform } from "react-native";

import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import colors from "../../resources/colors";
import icons from "../../resources/icons";
import english from "../../resources/languages/english";

class FBButtonGradient extends PureComponent {
  render() {
    const {
      title,
      onPress,
      backgroundColor,
      fontFamily,
      height,
      width,
      currentLang
    } = this.props;
    return (
      <View>
        <TouchableOpacity onPress={onPress}>
          <View
            style={{
              backgroundColor: backgroundColor,
              shadowColor: colors.black,
              shadowOffset: {
                height: 1,
                width: 0.45
              },
              elevation: 2,
              borderRadius: 23.29,
              height: height,
              width: width,
              flexDirection: "row",
              alignItems: "center"
            }}
          >
            <Image
              source={icons.fb}
              style={{
                width: wp(5),
                height: hp(5),
                tintColor: colors.white,
                marginStart: wp(10),
                marginEnd: "5%"
              }}
              resizeMode="contain"
            />
            <Text
              style={{
                color: colors.white,
                textAlign: "center",
                fontFamily: fontFamily,
                fontSize: RFPercentage(3),
                marginTop:
                  currentLang === english.LAG_ENG
                    ? 0
                    : Platform.OS === "ios"
                    ? 5
                    : 0
              }}
            >
              {title}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
export default FBButtonGradient;
