import React, {PureComponent} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import colors from '../../resources/colors';
import LinearGradient from 'react-native-linear-gradient';
import icons from '../../resources/icons';
import {widthPercentageToDP} from 'react-native-responsive-screen';
class ButtonGradientIcon extends PureComponent {
  render() {
    const {
      title,
      onPress,
      fontFamily,
      fontSize,
      height,
      width,
      borderRadius,
      icon,
      iconStyle,
    } = this.props;
    return (
      <View>
        <TouchableOpacity
          onPress={onPress}
          style={{
            shadowColor: colors.black,
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.3,
            shadowRadius: 3,
            elevation: 2,
          }}>
          <LinearGradient
            colors={[colors.colorBtnGradientOne, colors.colorBtnGradientTwo]}
            useAngle={true}
            start={{x: 1, y: 0}}
            end={{x: 1, y: 0}}
            angle={45}
            style={{
              height: height,
              width: width,
              borderRadius: borderRadius,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                style={{
                  color: colors.white,
                  fontFamily: fontFamily,
                  fontSize: fontSize,
                }}>
                {title}
              </Text>
              <Image
                source={icon}
                style={iconStyle}
                resizeMode="contain"
              />
            </View>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    );
  }
}
export default ButtonGradientIcon;
