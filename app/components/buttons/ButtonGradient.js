import React, {PureComponent} from 'react';
import {View, Text, TouchableOpacity, Platform} from 'react-native';
import colors from '../../resources/colors';
import LinearGradient from 'react-native-linear-gradient';
class ButtonGradient extends PureComponent {
  render() {
    const {
      title,
      onPress,
      fontFamily,
      fontSize,
      height,
      width,
      borderRadius,
      disable,
    } = this.props;
    return (
      <View>
        <TouchableOpacity
          onPress={onPress}
          disabled={disable}
          style={{
            shadowColor: 'rgba(0,0,0, 0.3)',
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowRadius: 3,
            elevation: 2,
          }}>
          <LinearGradient
            colors={[colors.colorBtnGradientOne, colors.colorBtnGradientTwo]}
            useAngle={true}
            start={{x: 1, y: 0}}
            end={{x: 1, y: 0}}
            angle={45}
            style={{
              height: height,
              width: width,
              borderRadius: borderRadius,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View>
              <Text
                style={{
                  color: colors.white,
                  fontFamily: fontFamily,
                  fontSize: fontSize,
                  alignSelf: 'center',
                }}>
                {title}
              </Text>
            </View>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    );
  }
}
export default ButtonGradient;
