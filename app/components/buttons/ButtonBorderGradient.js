import React, { PureComponent } from "react";
import { View, Text, TouchableOpacity, Platform } from "react-native";
import english from "../../resources/languages/english";
class ButtonBorderGradient extends PureComponent {
  render() {
    const {
      title,
      onPress,
      textColor,
      borderColor,
      fontFamily,
      width,
      height,
      backgroundColor,
      fontSize,
      currentLang
    } = this.props;
    return (
      <View>
        <TouchableOpacity onPress={onPress}>
          <View
            style={{
              backgroundColor: backgroundColor,
              borderColor: borderColor,
              borderWidth: 1,
              borderRadius: 23.29,
              height: height,
              width: width,
              alignItems: "center",
              justifyContent: "center",
              shadowColor: "rgba(0,0,0,0.2)",
              shadowOffset: {
                width: 3,
                height: 0
              },
              shadowRadius: 5
            }}
          >
            <View
              style={{
                marginTop:
                  currentLang === english.LAG_ENG
                    ? 0
                    : Platform.OS === "ios"
                    ? 10
                    : 0
              }}
            >
              <Text
                style={{
                  color: textColor,
                  textAlign: "center",
                  fontFamily: fontFamily,
                  fontSize: fontSize
                }}
              >
                {title}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
export default ButtonBorderGradient;
