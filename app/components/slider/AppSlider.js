import React, {PureComponent} from 'react';
import {View} from 'react-native';
import Swiper from 'react-native-swiper';

import AppSliderStyle from '../../resources/styles/AppSliderStyle';
class AppSlider extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0,
    };
  }

  render() {
    const {sliders, renderSliders, onIndexChanged} = this.props;
    return (
      <View style={AppSliderStyle.container}>
        <Swiper
          onIndexChanged={index => {
            onIndexChanged(index);
          }}
          loop={false}
          dot={<View style={AppSliderStyle.inactiveDot} />}
          activeDot={<View style={AppSliderStyle.activeDot} />}
          paginationStyle={AppSliderStyle.pagination}>
          {sliders.map((value, index) => {
            return renderSliders(value, index);
          })}
        </Swiper>
      </View>
    );
  }
}
export default AppSlider;
