/* eslint-disable no-unused-vars */
const BASE_URL_LIVE = 'https://rdfy-master.stablecompute.com/api/v1/';
const BASE_URL_LIVE_API = 'http://rdfy-master.stablecompute.com/api/v1/';

const BASE_URL_STAGING = 'http://3.6.98.213/api/v1/';
const BASE_URL_STAGING_API = '';

const BASE_URL_LOCAL = 'http://192.168.1.116:8000/api/v1/';
const BASE_URL_LOCAL_API = '';

const BASE_URL = BASE_URL_LIVE;
const BASE_URL_API = BASE_URL_LIVE_API;
const ApiUrls = {
  BASE_URL: BASE_URL,
  POST_LOGIN: BASE_URL + 'login',
  POST_LOGOUT: BASE_URL + 'logout',
  POST_REGISTER: BASE_URL + 'register',
  POST_VERIFY: BASE_URL + 'verify',
  POST_RESEND: BASE_URL + 'resend_sms',
  POST_FORGOT_PASSWORD: BASE_URL + 'forget_password',
  POST_CHANGE_PASSWORD: BASE_URL + 'change_password',
  POST_RESET_PASSWORD: BASE_URL + 'forget_password_otp_check',
  GET_REWARDS: BASE_URL + 'coupons-list',
  GET_MARKET_LIST: BASE_URL + 'market-list',
  GET_BAG_LIST: BASE_URL + 'bag-list',
  GET_BAG_DETAIL: BASE_URL + 'bag-details',
  GET_NOTIFICATION_LIST: BASE_URL + 'notifications-list',
  GET_REWARD_DETAIL: BASE_URL + 'coupon-details',
  GET_MARKET_DETAIL: BASE_URL + 'market-details',
  GET_BANK_DETAIL: BASE_URL + 'bank-details',
  POST_BANK_DETAIL: BASE_URL + 'bank-details',
  POST_BANK_DETAIL_UPDATE: BASE_URL + 'bank-details-update',
  POST_SELL_COUPAN: BASE_URL + 'bag-details',
  POST_REDEEM_COUPAN: BASE_URL + 'bag-details',
  POST_EDIT_PROFILE: BASE_URL + 'profile-update',
  GET_POST_NOTIFICATION_SWITCH: BASE_URL + 'notification-switch',
  POST_NOTIFICATION_READ: BASE_URL + 'notification-read-all',
  POST_CASHOUT_REQUEST: BASE_URL + 'cashout-request',
  POST_EDIT_OTP_SEND: BASE_URL + 'profile-update',
  POST_EDIT_OTP_VERIFY: BASE_URL + 'verify-profile-update',
  POST_EDIT_OTP_RESEND: BASE_URL + 'resend-profile-update',
  GET_TRANSACATIONS: BASE_URL + 'transaction-history',
  GET_TRANSACATION_DETAIL: BASE_URL + 'transaction-history',
  GET_CASHOUT_REQUESTS: BASE_URL + 'cashout-request-list',
  POST_BUY_COUPON: BASE_URL + 'stripe-payment-api',
  GET_SEARCH_HISTORY: BASE_URL + 'transaction-history-search',
};
export default ApiUrls;
