// let base64 = require('base-64');
// let username = 'admin';
// let password = 'sg54321chiso';
// export const getBasic = () => {
//   return base64.encode(username + ':' + password);
// };
import * as Sentry from '@sentry/react-native';

export const postRequest = async (apiUrl, body) => {
  try {
    console.log('postRequest', apiUrl, body);

    const response = await fetch(apiUrl, {
      credentials: 'include',
      method: 'POST',
      headers: {
        Authorization: body.apiToken,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    });
    // console.log('text reds',await response.text())
    const responseJson = await response.json();
    console.log('responseJson', responseJson);

    return responseJson;
  } catch (error) {
    console.error(error);
    throw error;
  }
};

export const getRequest = async (apiUrl, body) => {
  try {
    console.log('getRequest', apiUrl, body);
    const response = await fetch(apiUrl, {
      credentials: 'include',
      method: 'GET',
      headers: {
        Authorization: body.apiToken,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
    const responseJson = await response.json();
    console.log('responseJson', responseJson);

    return responseJson;
  } catch (error) {
    console.error(error);
    throw error;
  }
};
export const getTimeRequest = async (apiUrl, body) => {
  try {
    console.log('getRequest', apiUrl);
    const response = await fetch(apiUrl, {
      credentials: 'include',
      method: 'GET',
      headers: {
        Authorization: body.apiToken,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
    const responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.error(error);
    throw error;
  }
};

export const postImageRequest = async (apiUrl, field, imageUrl, body) => {
  try {
    const response = await fetch(apiUrl, {
      credentials: 'include',
      method: 'POST',
      headers: {
        Authorization: body.apiToken,
        'Content-Type': 'multipart/form-data',
      },
      body: createFormData(field, imageUrl, body),
    });
    const responseJson = await response.json();
    console.log('responseJson', responseJson);

    return responseJson;
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const createFormDataField = (field, photo, body) => {
  const data = new FormData();

  data.append(field, {
    name: 'image.jpg',
    type: 'image/*',
    uri: photo,
  });

  Object.keys(body).forEach(key => {
    data.append(key, body[key]);
  });

  return data;
};
const createFormData = body => {
  const data = new FormData();

  Object.keys(body).forEach(key => {
    data.append(key, body[key]);
  });

  return data;
};

export const putRequest = async (apiUrl, body) => {
  try {
    console.log('putRequest', apiUrl, body);
    const response = await fetch(apiUrl, {
      credentials: 'include',
      method: 'PUT',
      headers: {
        Authorization: body.apiToken,
        Accept: 'application/json',
        'Content-Type': 'application/json',
        body: createFormData(body),
      },
    });
    const responseJson = await response.json();
    console.log('responseJson', responseJson);

    return responseJson;
  } catch (error) {
    console.error(error);
    throw error;
  }
};
